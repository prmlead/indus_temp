﻿using PRMServices.SQLHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;

namespace SAPDBIntegration
{
    class Program
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
        {
            string query = $@"select * from apps.XXSAI_EBS_TO_PRM360_OPEN_PR_V";
            OracleBizClass bizClass = new OracleBizClass();
            DataTable table = bizClass.SelectQuery(query);
            logger.Debug("TOTAL COUNT OF PR's: " + table.Columns);
            logger.Debug("TOTAL COUNT OF PR's: " + table.Rows.Count);
            if (table != null && table.Rows.Count > 0)
            {
                //PostData(table, "ERP_PR_STAGE");//.AsEnumerable().ToList()
                InsertDataIntoDB(table);
            }

        }

        private static void InsertDataIntoDB(DataTable table) {


        }

        private static void PostData(DataTable dt, string tableName)
        {
            string filePath = @"json\" + tableName + ".json";
            try
            {
                if (!System.IO.File.Exists(filePath))
                {
                    Console.WriteLine("not found: " + filePath);
                    return;
                }

                string rowJson = System.IO.File.ReadAllText(filePath);
                var drList = dt.AsEnumerable().ToList();
                var bathes = drList.ChunkBy(Convert.ToInt32(ConfigurationManager.AppSettings["BATCH_SIZE"]));

                foreach (var batch in bathes)
                {
                    string bodyText = string.Empty;
                    foreach (DataRow dr in batch)
                    {
                        string rowJsonModified = rowJson;
                        foreach (DataColumn column in dr.Table.Columns)
                        {
                            if (column.DataType == typeof(DateTime))
                            {
                                var value = GetValue(column, dr).Replace(@"""", @"\""");
                                var temp = value.Split('/').ToList();
                                if (temp[0].Length == 1 && !temp[0].StartsWith("0"))
                                {
                                    temp[0] = "0" + temp[0];
                                }

                                if (temp[1].Length == 1 && !temp[1].StartsWith("0"))
                                {
                                    temp[1] = "0" + temp[1];
                                }
                                value = string.Join("/", temp);

                                rowJsonModified = rowJsonModified.Replace($@"${column.ColumnName}$", value);
                            }
                            else
                            {
                                rowJsonModified = rowJsonModified.Replace($@"${column.ColumnName}$", GetValue(column, dr).Replace(@"""", @"\"""));
                            }
                        }

                        bodyText += rowJsonModified + ",";
                    }

                    bodyText = bodyText.Trim(',');
                    string json = "{   \"data\":[ BODYTEXT ] }  ".Replace("BODYTEXT", bodyText);
                    logger.Debug("tableName JSON:" + json);
                    string url = ConfigurationManager.AppSettings["PRM_JP_API"];
                    var httpWebRequest = (System.Net.HttpWebRequest)WebRequest.Create(url + "/" + tableName);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Method = "POST";
                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        streamWriter.Write(json);
                    }
                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();
                        logger.Debug("tableName Response:" + result);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Debug("TableName:" + tableName + "--ERROR POSTING:" + ex.Message);
                logger.Error(ex);
                throw new Exception("Error sending data to PRM", ex);
            }
        }


        private static string GetValue(DataColumn column, DataRow dr)
        {
            string value = string.Empty;
            if (column.DataType == typeof(int) || column.DataType == typeof(decimal) || column.DataType == typeof(double))
            {
                if (dr[column.ColumnName] == null || dr[column.ColumnName].ToString().Trim() == string.Empty)
                {
                    if (column.ColumnName.Equals("PR_NUMBER") || column.ColumnName.Equals("MATERIAL_CODE"))
                    {
                        throw new Exception("Required fields are not present");
                    }
                    value = "0";
                }
                else
                {
                    value = dr[column.ColumnName].ToString().Trim();
                }
            }
            else
            {
                value = dr[column.ColumnName].ToString().Trim();
            }

            return value;
        }

    }

    public static class ListExtensions
    {
        public static List<List<T>> ChunkBy<T>(this List<T> source, int chunkSize)
        {
            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / chunkSize)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }
    }

}
