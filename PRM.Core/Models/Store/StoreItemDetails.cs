﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class StoreItemDetails : ResponseAudit
    {

        [DataMember(Name = "itemDetailID")]
        public int ItemDetailID { get; set; }

        [DataMember(Name = "itemID")]
        public int ItemID { get; set; }

        string serialNo = string.Empty;
        [DataMember(Name = "serialNo")]
        public string SerialNo
        {
            get
            {
                if (!string.IsNullOrEmpty(serialNo))
                {
                    return serialNo;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    serialNo = value;
                }
            }
        }

        string warrantyNo = string.Empty;
        [DataMember(Name = "warrantyNo")]
        public string WarrantyNo
        {
            get
            {
                if (!string.IsNullOrEmpty(warrantyNo))
                {
                    return warrantyNo;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    warrantyNo = value;
                }
            }
        }
        
        DateTime warrantyDate = DateTime.MaxValue;
        [DataMember(Name = "warrantyDate")]
        public DateTime WarrantyDate
        {
            get
            {
                return this.warrantyDate;
            }
            set
            {
                this.warrantyDate = value;
            }
        }

        [DataMember(Name = "ginID")]
        public int GinID { get; set; }

        [DataMember(Name = "grnID")]
        public int GrnID { get; set; }

        [DataMember(Name = "poID")]
        public int PoID { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

    }
}