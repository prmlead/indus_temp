﻿using System;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class DeliveryTimeLine : ResponseAudit
    {
        [DataMember(Name = "vendor")]
        public User Vendor { get; set; }

        [DataMember(Name = "vendorCompany")]
        public Company VendorCompany { get; set; }

        [DataMember(Name = "vendorInitPrice")]
        public decimal VendorInitPrice { get; set; }

        [DataMember(Name = "vendorRunPrice")]
        public decimal VendorRunPrice { get; set; }

        [DataMember(Name = "reductionPrice")]
        public decimal ReductionPrice { get; set; }

        //DateTime expectedDelivery = DateTime.MinValue;
        //[DataMember(Name = "expectedDelivery")]
        //public DateTime? ExpectedDelivery {
        //    get
        //    {
        //        return expectedDelivery;
        //    }
        //    set
        //    {
        //        if(value.HasValue)
        //        {
        //            expectedDelivery = value.Value;
        //        }                
        //    }
        //}

        string expectedDelivery = string.Empty;
        [DataMember(Name = "expectedDelivery")]
        public string ExpectedDelivery
        {
            get
            {
                return expectedDelivery;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    expectedDelivery = value;
                }
            }
        }

        string paymentScheduleDate = string.Empty;
        [DataMember(Name = "paymentScheduleDate")]
        public string PaymentScheduleDate {
            get
            {
                return paymentScheduleDate;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    paymentScheduleDate = value;
                }
            }
        }
    }
}