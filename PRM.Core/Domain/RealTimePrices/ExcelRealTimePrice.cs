﻿using Dapper;
using PRM.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Core.Domain.RealTimePrices
{
    public  class ExcelRealTimePrice : IExcelEntity
    {
        public DateTime Date { get; set; }
        public string Product { get; set; }
        public string Category { get; set; }

        [DataRowKey("Trading Mode")]
        public string TradingMode { get; set; }

        [DataRowKey("Market/Port")]
        public string Market { get; set; }

        public decimal Price { get; set; }

        public string Unit { get; set; }

        public decimal Change { get; set; }


    }
}
