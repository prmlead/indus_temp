﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Core.Domain.RealTimePrices
{
  public  class RealtimePriceDrop
    {

        public RealTimePrice Today { get; set; }

        public RealTimePrice Yesterday { get; set; }
    }

}
