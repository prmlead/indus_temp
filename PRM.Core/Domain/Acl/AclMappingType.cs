﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Core.Domain.Acl
{
   public enum AclMappingType
    {
        User = 1 ,

        Project = 10,

        Department = 20,

    }
}
