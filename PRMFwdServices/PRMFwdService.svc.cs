﻿using System;
using System.ServiceModel.Activation;
using MySql.Data.MySqlClient;
using System.Data;
using System.Configuration;
using System.Web;
using PRM.Core.Models;
using PRM.Core.Common;

namespace PRMFwdServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMFwdService : IPRMFwdService
    {
        public Response RequirementSave(Requirement requirement)
        {
            Response response = new Response();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }
                        int isValidSession = Utilities.ValidateSession(requirement.SessionID, cmd);
                        //if (requirement.RequirementID > 0)
                        //{
                        //    Requirement oldReq = GetRequirementDataOffline(requirement.RequirementID, requirement.CustomerID, requirement.SessionID);

                        //    int[] vendorsToRemove = GetVendorsToRemove(requirement, oldReq);
                        //    foreach (int id in vendorsToRemove)
                        //    {
                        //        Response res = RemoveVendorFromAuction(id, requirement.RequirementID, requirement.SessionID, "VendoremailForRemoval");
                        //    }

                        //    int[] itemsToRemove = GetItemsToRemove(requirement, oldReq);
                        //    foreach (int id in itemsToRemove)
                        //    {
                        //        Response res = RemoveItemFromAuction(id, requirement.RequirementID, requirement.SessionID, "");
                        //    }
                        //}

                        if (requirement.Attachment != null)
                        {


                            long tick = DateTime.Now.Ticks;
                            requirement.AttachmentName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + tick + "_user" + requirement.CustomerID + "_" + requirement.AttachmentName);
                            PRMUtility.SaveFile(requirement.AttachmentName, requirement.Attachment);

                            requirement.AttachmentName = "req" + tick + "_user" + requirement.CustomerID + "_" + requirement.AttachmentName;
                            Response res = PRMUtility.SaveAttachment(requirement.AttachmentName, cmd);
                            if (res.ErrorMessage != "")
                            {
                                response.ErrorMessage = res.ErrorMessage;
                            }
                            requirement.AttachmentName = res.ObjectID.ToString();

                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {

                    }
                }
            }

            return response;
        }

        public Requirement GetRequirementData(int reqID, int userID, string sessionID)
        {
            Requirement req = new Requirement();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }

                        int isValidSession = PRMUtility.ValidateSession(sessionID, cmd);
                        cmd.CommandText = "po_GetPoList";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new MySqlParameter("P_REQ_ID", reqID));
                        cmd.Parameters.Add(new MySqlParameter("P_U_ID", userID));
                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();
                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            DataRow row = ds.Tables[0].Rows[0];
                            req = PRMUtility.GetFullReqEntity(row, cmd);
                            string Status = req.Status;
                            if (req.MinReduceAmount > 0)
                            {
                                req.MinBidAmount = req.MinReduceAmount;
                            }
                            else
                            {
                                req.MinBidAmount = Math.Round(Convert.ToDouble(req.Budget) / 100, 2);
                            }
                            DateTime now = DateTime.Now;
                            if (req.EndTime != DateTime.MaxValue && req.EndTime > now && req.StartTime < now)
                            {
                                DateTime date = Convert.ToDateTime(req.EndTime);
                                long diff = Convert.ToInt64((date - now).TotalSeconds);
                                req.TimeLeft = diff;
                                req.Status = PRMUtility.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());
                            }
                            else if (req.StartTime != DateTime.MaxValue && req.StartTime > now)
                            {
                                DateTime date = Convert.ToDateTime(req.StartTime);
                                long diff = Convert.ToInt64((date - now).TotalSeconds);
                                req.TimeLeft = diff;
                                req.Status = PRMUtility.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                            }
                            else if (req.EndTime < now)
                            {
                                req.TimeLeft = -1;
                                if ((Status == PRMUtility.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString()) || Status == PRMUtility.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString())) && req.EndTime < now)
                                {
                                    req.Status = PRMUtility.GetEnumDesc<PRMStatus>(PRMStatus.NegotiationEnded.ToString());
                                    
                                    //EndNegotiation(req.reqID, req.CustomerID, sessionID);
                                }
                                else
                                {
                                    req.Status = Status;
                                }
                                //if (isPOSent == 0 && req.Status != GetEnumDesc<PRMStatus>(PRMStatus.POGenerated.ToString())) 
                                //{
                                //    req.Status = "CLOSED";
                                //}
                            }
                            else if (req.StartTime == DateTime.MaxValue)
                            {
                                req.TimeLeft = -1;
                                req.Status = PRMUtility.GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                            }
                            if (Status == PRMUtility.GetEnumDesc<PRMStatus>(PRMStatus.DELETED.ToString()))
                            {
                                req.TimeLeft = -1;
                                req.Status = Status;
                            }
                        }
                        

                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[1].Rows)
                            {
                                VendorDetails vendor = PRMUtility.GetVendorDetailsEntity(row, cmd);
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        req.ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return req;
        }

        private string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MySQLConnectionString"].ConnectionString;
        }
        

    }
}
