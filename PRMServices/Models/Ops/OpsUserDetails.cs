﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models

{
    [DataContract]
    public class OpsUserDetails : Entity
    {


        [DataMember(Name = "u_ID")]
        public int U_ID { get; set; }

        [DataMember(Name = "userName")]
        public string UserName { get; set; }

        [DataMember(Name = "u_PHONE")]
        public string U_PHONE { get; set; }

        [DataMember(Name = "u_EMAIL")]
        public string U_EMAIL { get; set; }


        [DataMember(Name = "dateCreated")]
        public DateTime? DateCreated { get; set; }

        [DataMember(Name = "createdByName")]
        public string CreatedByName { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

    }
}