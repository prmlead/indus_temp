﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRMServices.Models.RealTimePrice
{
    public class RealTimeReportPostModel
    {
        public string Product { get; set; }
        public string Market { get; set; }

        public object FromDate { get; set; }

        public object ToDate { get; set; }

        public int TypeId { get; set; }
    }
}