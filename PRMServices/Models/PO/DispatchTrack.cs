﻿
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using PRMServices.Models;

namespace PRMServices
{
    [DataContract]
    public class DispatchTrack : ResponseAudit
    {


        [DataMember(Name = "dTID")]
        public int DTID { get; set; }

        [DataMember(Name = "poItemsEntity")]
        public List<POItems> POItemsEntity { get; set; }

        [DataMember(Name = "vendorPOObject")]
        public VendorPO VendorPOObject { get; set; }

        [DataMember(Name = "dispatchQuantity")]
        public decimal DispatchQuantity { get; set; }

        [DataMember(Name = "vendorPOQuantity")]
        public decimal VendorPOQuantity { get; set; }

        DateTime dispatchDate = DateTime.MaxValue;
        [DataMember(Name = "dispatchDate")]
        public DateTime? DispatchDate
        {
            get
            {
                return dispatchDate;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    dispatchDate = value.Value;
                }
            }
        }

        string dispatchType = string.Empty;
        [DataMember(Name = "dispatchType")]
        public string DispatchType
        {
            get
            {
                if (!string.IsNullOrEmpty(dispatchType))
                { return dispatchType; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { dispatchType = value; }
            }
        }

        string dispatchComments = string.Empty;
        [DataMember(Name = "dispatchComments")]
        public string DispatchComments
        {
            get
            {
                if (!string.IsNullOrEmpty(dispatchComments))
                { return dispatchComments; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { dispatchComments = value; }
            }
        }

        string dispatchCode = string.Empty;
        [DataMember(Name = "dispatchCode")]
        public string DispatchCode
        {
            get
            {
                if (!string.IsNullOrEmpty(dispatchCode))
                { return dispatchCode; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { dispatchCode = value; }
            }
        }

        string dispatchMode = string.Empty;
        [DataMember(Name = "dispatchMode")]
        public string DispatchMode
        {
            get
            {
                if (!string.IsNullOrEmpty(dispatchMode))
                { return dispatchMode; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { dispatchMode = value; }
            }
        }

        string deliveryTrackID = string.Empty;
        [DataMember(Name = "deliveryTrackID")]
        public string DeliveryTrackID
        {
            get
            {
                if (!string.IsNullOrEmpty(deliveryTrackID))
                { return deliveryTrackID; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { deliveryTrackID = value; }
            }
        }

        string dispatchLink = string.Empty;
        [DataMember(Name = "dispatchLink")]
        public string DispatchLink
        {
            get
            {
                if (!string.IsNullOrEmpty(dispatchLink))
                { return dispatchLink; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { dispatchLink = value; }
            }
        }


        [DataMember(Name = "recivedQuantity")]
        public decimal RecivedQuantity { get; set; }

        DateTime recivedDate = DateTime.MaxValue;
        [DataMember(Name = "recivedDate")]
        public DateTime? RecivedDate
        {
            get
            {
                return recivedDate;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    recivedDate = value.Value;
                }
            }
        }

        string recivedComments = string.Empty;
        [DataMember(Name = "recivedComments")]
        public string RecivedComments
        {
            get
            {
                if (!string.IsNullOrEmpty(recivedComments))
                { return recivedComments; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { recivedComments = value; }
            }
        }

        string recivedCode = string.Empty;
        [DataMember(Name = "recivedCode")]
        public string RecivedCode
        {
            get
            {
                if (!string.IsNullOrEmpty(recivedCode))
                { return recivedCode; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { recivedCode = value; }
            }
        }

        [DataMember(Name = "recivedLink")]
        public int RecivedLink { get; set; }

        string recivedBy = string.Empty;
        [DataMember(Name = "recivedBy")]
        public string RecivedBy
        {
            get
            {
                if (!string.IsNullOrEmpty(recivedBy))
                { return recivedBy; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { recivedBy = value; }
            }
        }

        string status = string.Empty;
        [DataMember(Name = "status")]
        public string Status
        {
            get
            {
                if (!string.IsNullOrEmpty(status))
                { return status; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { status = value; }
            }
        }

        [DataMember(Name = "poFile")]
        public FileUpload POFile { get; set; }

        [DataMember(Name = "poID")]
        public int POID { get; set; }


        [DataMember(Name = "poOrderId")]
        public string POOrderId { get; set; }

        string indentID = string.Empty;
        [DataMember(Name = "indentID")]
        public string IndentID
        {
            get
            {
                if (!string.IsNullOrEmpty(indentID))
                { return indentID; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { indentID = value; }
            }
        }
        

        string purchaseID = string.Empty;
        [DataMember(Name = "purchaseID")]
        public string PurchaseID
        {
            get
            {
                if (!string.IsNullOrEmpty(purchaseID))
                { return purchaseID; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { purchaseID = value; }
            }
        }

        [DataMember(Name = "returnQuantity")]
        public decimal ReturnQuantity { get; set; }


        [DataMember(Name = "receivedQtyPrice")]
        public decimal ReceivedQtyPrice { get; set; }

        [DataMember(Name = "paymentPrice")]
        public decimal PaymentPrice { get; set; }

        [DataMember(Name = "common")]
        public bool Common { get; set; }

        DateTime shipmentDate = DateTime.MaxValue;
        [DataMember(Name = "shipmentDate")]
        public DateTime? ShipmentDate
        {
            get
            {
                return shipmentDate;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    shipmentDate = value.Value;
                }
            }
        }

        DateTime documentDate = DateTime.MaxValue;
        [DataMember(Name = "documentDate")]
        public DateTime? DocumentDate
        {
            get
            {
                return documentDate;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    documentDate = value.Value;
                }
            }
        }

        string customerBatch = string.Empty;
        [DataMember(Name = "customerBatch")]
        public string CustomerBatch
        {
            get
            {
                if (!string.IsNullOrEmpty(customerBatch))
                { return customerBatch; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { customerBatch = value; }
            }
        }

        string shipmentFromLocation = string.Empty;
        [DataMember(Name = "shipmentFromLocation")]
        public string ShipmentFromLocation
        {
            get
            {
                if (!string.IsNullOrEmpty(shipmentFromLocation))
                { return shipmentFromLocation; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { shipmentFromLocation = value; }
            }
        }

        string customerLocation = string.Empty;
        [DataMember(Name = "customerLocation")]
        public string CustomerLocation
        {
            get
            {
                if (!string.IsNullOrEmpty(customerLocation))
                { return customerLocation; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { customerLocation = value; }
            }
        }

        string shipToLocation = string.Empty;
        [DataMember(Name = "shipToLocation")]
        public string ShipToLocation
        {
            get
            {
                if (!string.IsNullOrEmpty(shipToLocation))
                { return shipToLocation; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { shipToLocation = value; }
            }
        }

        string unloadingPoint = string.Empty;
        [DataMember(Name = "unloadingPoint")]
        public string UnloadingPoint
        {
            get
            {
                if (!string.IsNullOrEmpty(unloadingPoint))
                { return unloadingPoint; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { unloadingPoint = value; }
            }
        }


        DateTime manufacDate = DateTime.MaxValue;
        [DataMember(Name = "manufacDate")]
        public DateTime? ManufacDate
        {
            get
            {
                return manufacDate;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    manufacDate = value.Value;
                }
            }
        }

        DateTime bestBeforeDate = DateTime.MaxValue;
        [DataMember(Name = "bestBeforeDate")]
        public DateTime? BestBeforeDate
        {
            get
            {
                return bestBeforeDate;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    bestBeforeDate = value.Value;
                }
            }
        }

        string totalWeight = string.Empty;
        [DataMember(Name = "totalWeight")]
        public string TotalWeight
        {
            get
            {
                if (!string.IsNullOrEmpty(totalWeight))
                { return totalWeight; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { totalWeight = value; }
            }
        }

        string totalVolume = string.Empty;
        [DataMember(Name = "totalVolume")]
        public string TotalVolume
        {
            get
            {
                if (!string.IsNullOrEmpty(totalVolume))
                { return totalVolume; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { totalVolume = value; }
            }
        }

        string freightInvoice = string.Empty;
        [DataMember(Name = "freightInvoice")]
        public string FreightInvoice
        {
            get
            {
                if (!string.IsNullOrEmpty(freightInvoice))
                { return freightInvoice; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { freightInvoice = value; }
            }
        }

        string freightTotalInvoice = string.Empty;
        [DataMember(Name = "freightTotalInvoice")]
        public string FreightTotalInvoice
        {
            get
            {
                if (!string.IsNullOrEmpty(freightTotalInvoice))
                { return freightTotalInvoice; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { freightTotalInvoice = value; }
            }
        }

        string freightTax = string.Empty;
        [DataMember(Name = "freightTax")]
        public string FreightTax
        {
            get
            {
                if (!string.IsNullOrEmpty(freightTax))
                { return freightTax; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { freightTax = value; }
            }
        }

        string invoiceNumber = string.Empty;
        [DataMember(Name = "invoiceNumber")]
        public string InvoiceNumber
        {
            get
            {
                if (!string.IsNullOrEmpty(invoiceNumber))
                { return invoiceNumber; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { invoiceNumber = value; }
            }
        }

        string invoiceAmount = string.Empty;
        [DataMember(Name = "invoiceAmount")]
        public string InvoiceAmount
        {
            get
            {
                if (!string.IsNullOrEmpty(invoiceAmount))
                { return invoiceAmount; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { invoiceAmount = value; }
            }
        }

        string shippedThrough = string.Empty;
        [DataMember(Name = "shippedThrough")]
        public string ShippedThrough
        {
            get
            {
                if (!string.IsNullOrEmpty(shippedThrough))
                { return shippedThrough; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { shippedThrough = value; }
            }
        }

        DateTime poCreatedDate = DateTime.MaxValue;
        [DataMember(Name = "poCreatedDate")]
        public DateTime? PoCreatedDate
        {
            get
            {
                return poCreatedDate;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    poCreatedDate = value.Value;
                }
            }
        }

        DateTime poDeliveryDate = DateTime.MaxValue;
        [DataMember(Name = "poDeliveryDate")]
        public DateTime? PoDeliveryDate
        {
            get
            {
                return poDeliveryDate;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    poDeliveryDate = value.Value;
                }
            }
        }

        DateTime expectedDeliveryDate = DateTime.MaxValue;
        [DataMember(Name = "expectedDeliveryDate")]
        public DateTime? ExpectedDeliveryDate
        {
            get
            {
                return expectedDeliveryDate;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    expectedDeliveryDate = value.Value;
                }
            }
        }

        string supplierName = string.Empty;
        [DataMember(Name = "supplierName")]
        public string SupplierName
        {
            get
            {
                if (!string.IsNullOrEmpty(supplierName))
                { return supplierName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { supplierName = value; }
            }
        }

        [DataMember(Name = "phoneNo")]
        public int PhoneNo { get; set; }

        string itemName = string.Empty;
        [DataMember(Name = "itemName")]
        public string ItemName
        {
            get
            {
                if (!string.IsNullOrEmpty(itemName))
                { return itemName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { itemName = value; }
            }
        }

        string department = string.Empty;
        [DataMember(Name = "department")]
        public string Department
        {
            get
            {
                if (!string.IsNullOrEmpty(department))
                { return department; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { department = value; }
            }
        }

        string category = string.Empty;
        [DataMember(Name = "category")]
        public string Category
        {
            get
            {
                if (!string.IsNullOrEmpty(category))
                { return category; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { category = value; }
            }
        }
       

    }
}