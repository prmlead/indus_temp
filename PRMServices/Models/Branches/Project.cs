﻿using PRM.Core.Common;
using System;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class Project : Entity
    {
        [DataMember] [DataNames("PROJECT_ID")] public int PROJECT_ID { get; set; }
        [DataMember] [DataNames("COMP_ID")] public int COMP_ID { get; set; }
        [DataMember] [DataNames("PROJECT_CODE")] public string PROJECT_CODE { get; set; }
        [DataMember] [DataNames("PROJECT_NAME")] public string PROJECT_NAME { get; set; }
        [DataMember] [DataNames("PROJECT_DETAILS")] public string PROJECT_DETAILS { get; set; }
        [DataMember] [DataNames("PROJECT_HEAD")] public int PROJECT_HEAD { get; set; }
        [DataMember] [DataNames("PROJECT_COMMENTS")] public string PROJECT_COMMENTS { get; set; }
        [DataMember] [DataNames("SEL_BRANCHES")] public string SEL_BRANCHES { get; set; }
        [DataMember] [DataNames("CREATED_BY")] public int CREATED_BY { get; set; }
        [DataMember] [DataNames("CREATED")] public DateTime CREATED { get; set; }
        [DataMember] [DataNames("MODIFIED_BY")] public int MODIFIED_BY { get; set; }
        [DataMember] [DataNames("MODIFIED")] public DateTime MODIFIED { get; set; }

        [DataMember] public Branch[] Branches { get; set; }

        [DataMember] public Requirement requirement { get; set; }

    }
}
