﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class BudgetApprovedBy : Entity
    {
        [DataMember(Name = "budgetName")]
        public int BudgetName { get; set; }

        [DataMember(Name = "budgetSign")]
        public int BudgetSign { get; set; }

        [DataMember(Name = "budgetDate")]
        public string BudgetDate { get; set; }

    }
}