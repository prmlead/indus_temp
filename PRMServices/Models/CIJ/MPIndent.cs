﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class MPIndent : Entity
    {
        [DataMember(Name = "indID")]
        public int IndID { get; set; }

        [DataMember(Name = "indNo")]
        public string IndNo { get; set; }

        [DataMember(Name = "department")]
        public string Department { get; set; }

        [DataMember(Name = "requestDate")]
        public string RequestDate { get; set; }

        [DataMember(Name = "requestedBy")]
        public string RequestedBy { get; set; }

        [DataMember(Name = "approvedBy")]
        public string ApprovedBy { get; set; }

        [DataMember(Name = "purchaseIncharge")]
        public string PurchaseIncharge { get; set; }


        string purchaseDirector = string.Empty;
        [DataMember(Name = "purchaseDirector")]
        public string PurchaseDirector
        {
            get
            {
                if (!string.IsNullOrEmpty(purchaseDirector))
                { return purchaseDirector; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { purchaseDirector = value; }
            }
        }


        [DataMember(Name = "attachments")]
        public string Attachments { get; set; }

        [DataMember(Name = "listIndentItems")]
        public List<IndentItems> ListIndentItems { get; set; }

        [DataMember(Name = "cij")]
        public stringCIJ CIJ { get; set; }

        [DataMember(Name = "listDeleteItems")]
        public List<int> ListDeleteItems { get; set; }

        [DataMember(Name = "approximateCostRange")]
        public decimal ApproximateCostRange { get; set; }

        [DataMember(Name = "reqID")]
        public int ReqID { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "isMedical")]
        public int IsMedical { get; set; }

        [DataMember(Name = "status")]
        public string Status { get; set; }

        [DataMember(Name = "userWFStatus")]
        public string UserWFStatus { get; set; }

        [DataMember(Name = "createdBy")]
        public int CreatedBy { get; set; }

        [DataMember(Name = "isValid")]
        public int IsValid { get; set; }

        [DataMember(Name = "deptID")]
        public int DeptID { get; set; }

        [DataMember(Name = "isCapex")]
        public int IsCapex { get; set; }

        [DataMember(Name = "pdfID")]
        public int PdfID { get; set; }

        string itemsString = string.Empty;
        [DataMember(Name = "itemsString")]
        public string ItemsString
        {
            get
            {
                if (!string.IsNullOrEmpty(itemsString))
                { return itemsString; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { itemsString = value; }
            }
        }



        [DataMember(Name = "cloneIndID")]
        public int CloneIndID { get; set; }

        [DataMember(Name = "isCloned")]
        public Boolean IsCloned { get; set; }

        [DataMember(Name = "isIndentAssigned")]
        public int IsIndentAssigned { get; set; }

        //[DataMember(Name = "userPriority")]
        //public string UserPriority { get; set; }

        //[DataMember(Name = "userStatus")]
        //public string UserStatus { get; set; }

        [DataMember(Name = "currentApproverName")]
        public string CurrentApproverName { get; set; }

        string userPriority = string.Empty;
        [DataMember(Name = "userPriority")]
        public string UserPriority
        {
            get
            {
                if (!string.IsNullOrEmpty(userPriority))
                { return userPriority; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { userPriority = value; }
            }
        }

        string userStatus = string.Empty;
        [DataMember(Name = "userStatus")]
        public string UserStatus
        {
            get
            {
                if (!string.IsNullOrEmpty(userStatus))
                { return userStatus; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { userStatus = value; }
            }
        }

        string expectedDelivery = string.Empty;
        [DataMember(Name = "expectedDelivery")]
        public string ExpectedDelivery
        {
            get
            {
                if (!string.IsNullOrEmpty(expectedDelivery))
                { return expectedDelivery; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { expectedDelivery = value; }
            }
        }

    }
}