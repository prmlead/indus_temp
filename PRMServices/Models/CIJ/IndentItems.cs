﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class IndentItems : Entity
    {
        [DataMember(Name = "slNo")]
        public int SlNo { get; set; }

        [DataMember(Name = "indentItemID")]
        public int IndentItemID { get; set; }

        [DataMember(Name = "materialDescription")]
        public string MaterialDescription { get; set; }

        [DataMember(Name = "existFunctional")]
        public int ExistFunctional { get; set; }

        [DataMember(Name = "existNonFunctional")]
        public int ExistNonFunctional { get; set; }

        [DataMember(Name = "requiredQuantity")]
        public int RequiredQuantity { get; set; }

        [DataMember(Name = "purposeOfMaterial")]
        public string PurposeOfMaterial { get; set; }

        [DataMember(Name = "makeModel")]
        public string MakeModel { get; set; }

        [DataMember(Name = "units")]
        public string Units { get; set; }

        [DataMember(Name = "fileName")]
        public FileUpload FileName { get; set; }

        [DataMember(Name = "isRejected")]
        public int IsRejected { get; set; }

        [DataMember(Name = "rejectedQuantity")]
        public int RejectedQuantity { get; set; }

        [DataMember(Name = "rejectedBy")]
        public int RejectedBy { get; set; }

        string rejectReason = string.Empty;
        [DataMember(Name = "rejectReason")]
        public string RejectReason
        {
            get
            {
                if (!string.IsNullOrEmpty(rejectReason))
                { return rejectReason; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { rejectReason = value; }
            }
        }

        [DataMember(Name = "initialQty")]
        public int InitialQty { get; set; }

        [DataMember(Name = "budgetID")]
        public int BudgetID { get; set; }

        [DataMember(Name = "bcInfo")]
        public BudgetCodeEntity BCInfo { get; set; }



    }
}