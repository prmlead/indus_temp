﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class ASNDetails : ResponseAudit
    {
        [DataMember] [DataNames("ASN_ID")] public int ASN_ID { get; set; }
        [DataMember] [DataNames("COMP_ID")] public int COMP_ID { get; set; }
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("GRN_CODE")] public string GRN_CODE { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string VENDOR_CODE { get; set; }
        [DataMember] [DataNames("VENDOR_ID")] public int VENDOR_ID { get; set; }
        [DataMember] [DataNames("ASN_CODE")] public string ASN_CODE { get; set; }
        [DataMember] [DataNames("ASN_TYPE")] public string ASN_TYPE { get; set; }
        [DataMember] [DataNames("DELIVERY_DATE")] public DateTime? DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("SHIPMENT_DATE")] public DateTime? SHIPMENT_DATE { get; set; }
        [DataMember] [DataNames("DOCUMENT_DATE")] public DateTime? DOCUMENT_DATE { get; set; }
        [DataMember] [DataNames("CUSTOMER_BATCH")] public string CUSTOMER_BATCH { get; set; }
        [DataMember] [DataNames("SHIP_FROM_LOCATION")] public string SHIP_FROM_LOCATION { get; set; }
        [DataMember] [DataNames("CUSTOMER_LOCATION")] public string CUSTOMER_LOCATION { get; set; }
        [DataMember] [DataNames("SHIP_TO_LOCATION")] public string SHIP_TO_LOCATION { get; set; }
        [DataMember] [DataNames("UNLOADING_POINT")] public string UNLOADING_POINT { get; set; }
        [DataMember] [DataNames("SHIPPED_THROUGH")] public string SHIPPED_THROUGH { get; set; }
        [DataMember] [DataNames("MANUFACTURED_DATE")] public DateTime? MANUFACTURED_DATE { get; set; }
        [DataMember] [DataNames("BEST_BEFORE_DATE")] public DateTime? BEST_BEFORE_DATE { get; set; }
        [DataMember] [DataNames("TOTAL_WEIGHT_DETAILS")] public string TOTAL_WEIGHT_DETAILS { get; set; }
        [DataMember] [DataNames("TOTAL_VOLUME_DETAILS")] public string TOTAL_VOLUME_DETAILS { get; set; }
        [DataMember] [DataNames("FREIGHT_INVOICE_NO")] public string FREIGHT_INVOICE_NO { get; set; }
        [DataMember] [DataNames("FREIGHT_TOTAL_INVOICE_AMOUNT")] public decimal FREIGHT_TOTAL_INVOICE_AMOUNT { get; set; }
        [DataMember] [DataNames("FREIGHT_TAX")] public decimal FREIGHT_TAX { get; set; }
        [DataMember] [DataNames("INVOICE_NUMBER")] public string INVOICE_NUMBER { get; set; }
        [DataMember] [DataNames("INVOICE_AMOUNT")] public decimal INVOICE_AMOUNT { get; set; }
        [DataMember] [DataNames("SERVICE_CODE")] public string SERVICE_CODE { get; set; }
        [DataMember] [DataNames("SERVICE_COMPLETION_DATE")] public DateTime? SERVICE_COMPLETION_DATE { get; set; }
        [DataMember] [DataNames("SERVICE_COMPLETED_DATE")] public DateTime? SERVICE_COMPLETED_DATE { get; set; }
        [DataMember] [DataNames("SERVICE_DOCUMENT_DATE")] public DateTime? SERVICE_DOCUMENT_DATE { get; set; }
        [DataMember] [DataNames("SERVICE_CUSTOMER_LOCATION")] public string SERVICE_CUSTOMER_LOCATION { get; set; }
        [DataMember] [DataNames("SERVICE_LOCATION")] public string SERVICE_LOCATION { get; set; }
        [DataMember] [DataNames("SERVICE_BY")] public string SERVICE_BY { get; set; }
        [DataMember] [DataNames("COMMENTS")] public string COMMENTS { get; set; }
        [DataMember] [DataNames("RECEIVED_CODE")] public string RECEIVED_CODE { get; set; }
        [DataMember] [DataNames("RECEIVED_BY")] public string RECEIVED_BY { get; set; }
        [DataMember] [DataNames("RECEIVED_COMMENTS")] public string RECEIVED_COMMENTS { get; set; }
        [DataMember] [DataNames("RECEIVED_DATE")] public DateTime? RECEIVED_DATE { get; set; }
        [DataMember] [DataNames("DATE_CREATED")] public DateTime? DATE_CREATED { get; set; }
        [DataMember] [DataNames("DATE_MODIFIED")] public DateTime? DATE_MODIFIED { get; set; }
        [DataMember] [DataNames("CREATED_BY")] public int CREATED_BY { get; set; }
        [DataMember] [DataNames("MODIFIED_BY")] public int MODIFIED_BY { get; set; }

    }
}