﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class APMCDashboard : Entity
    {
        [DataMember(Name = "apmcInput")]
        public APMCInput APMCInput { get; set; }

        [DataMember(Name = "apmcNegotiationList")]
        public List<APMCNegotiation> APMCNegotiationList { get; set; }
    }
}