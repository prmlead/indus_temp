﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class ProductConfig : Entity
    {
        [DataMember(Name = "PC_ID")]
        public int PC_ID { get; set; }

        [DataMember(Name = "ProductId")]
        public int ProductId { get; set; }

        [DataMember(Name = "C_CONFIG_ID")]
        public int C_CONFIG_ID { get; set; }

        [DataMember(Name = "IS_VENDOR_EDIT")]
        public int IS_VENDOR_EDIT { get; set; }

        [DataMember(Name = "IS_CUSTOMER_EDIT")]
        public int IS_CUSTOMER_EDIT { get; set; }

        //[DataMember(Name = "firstName")]
        //public string FirstName { get; set; }

        //[DataMember(Name = "bidAmount")]
        //public double BidAmount { get; set; }
    }
}