﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using PRM.Core.Models;

namespace PRMServices.Models.Catalog
{
    [DataContract]
    public class FilterObject : EntityExt
    {
        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "field")]
        public string Field { get; set; }

        [DataMember(Name = "condition")]
        public string Condition { get; set; }

        [DataMember(Name = "value")]
        public string Value { get; set; }
    }

    
}