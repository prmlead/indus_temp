﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models.Common
{
    [DataContract]
    public class BaseModel
    {

        public IDictionary<string,object> AdditionalAttributes { get; set; }
    }
}