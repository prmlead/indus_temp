﻿prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('lotListCtrl', ["$state", "$stateParams", "$scope", "auctionsService", "userService", "$http", "$window", "domain", "fileReader", "growlService", "$log", "$filter", "ngDialog", "techevalService", "catalogService", "PRMLotReqService", "SignalRFactory", "signalRHubName",
        function ($state, $stateParams, $scope, auctionsService, userService, $http, $window, domain, fileReader, growlService, $log, $filter, ngDialog, techevalService, catalogService, PRMLotReqService, SignalRFactory, signalRHubName) {
            $scope.lotItems = {};
            $scope.lotItemsOrg = {};
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage = 8;
            $scope.itemsPerPage2 = 8;
            $scope.maxSize = 8;
            $scope.currentUserCompID = userService.getUserCompanyId();
            $scope.currentUserId = +userService.getUserId();
            $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;

            PRMLotReqService.getCompanyLots($scope.currentUserCompID, ($scope.isCustomer ? 0 : $scope.currentUserId))
                .then(function (response) {
                    $scope.lotItems = response;
                    $scope.totalItems = $scope.lotItems.length;
                    $scope.lotItems.forEach(function (lot, index) {
                        lot.StartTime = new moment(lot.startTime).format("YYYY-MM-DD HH:mm");
                        lot.EndTime = new moment(lot.endTime).format("YYYY-MM-DD HH:mm");
                        lot.PostedOn = new moment(lot.PostedOn).format("YYYY-MM-DD HH:mm");
                    });

                    $scope.lotItemsOrg = $scope.lotItems;
                });

            $scope.searchTable = function (str) {

                $scope.category = 'ALL CATEGORIES';
                $scope.reqStatus = 'ALL';
                $scope.lotItems = $scope.lotItemsOrg.filter(function (lot) {
                    return (String(lot.LotId).includes(str) == true || String(lot.LotTitle).includes(str) == true ||
                        String(lot.PostedBy).includes(str) == true || String(lot.PostedOn).includes(str) == true);
                });

                $scope.totalItems = $scope.lotItems.length;
            };

            $scope.getStatusFilter = function (filterVal) {
                $scope.filterArray = [];
                if (filterVal === 'ALL') {
                    $scope.lotItems = $scope.lotItemsOrg;

                } else {
                    $scope.filterArray = $scope.lotItemsOrg.filter(function (item) {
                        return item.Status.toUpperCase() === filterVal;
                    });

                    $scope.lotItems = $scope.filterArray;
                }

                $scope.totalItems = $scope.lotItems.length;

            };
        }]);