public Response SaveSapMaterialMaster(List<SAPMaterialMaster> details)
        {
            Response response = new Response();
            try
            {
                checkSapIntegrationUser();

                if (details != null && details.Count > 0)
                {
                    logger.Debug("SAP MATERIAL MASTER:" + JsonConvert.SerializeObject(details));
                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        using (cmd.Connection = new MySqlConnection(GetConnectionString()))
                        {
                            try
                            {
                                string query = "INSERT INTO sap_material_master ";
                                if (cmd.Connection.State != ConnectionState.Open)
                                {
                                    cmd.Connection.Open();
                                }
                                query += @"SELECT @FIELD_0, @LOKZ, @EXTWG, @WRKST, @MAKTL, @MAKTX, @MATNR, @FIELD_1, @FIELD_2, @FIELD_3, @FIELD_4, @FIELD_5, @FIELD_6, @MFRNR, 
                                @FIELD_7, @PO_TEXT, @FIELD_8, @FIELD_9, @FIELD_10, @FIELD_10_1, @XCHPF, @FIELD_11, @FIELD_12, @FIELD_13, @FROM_REORDER_PLANNING, 
                                @FIELD_14, @FIELD_15, @FIELD_16, @FIELD_17, @FIELD_18, @FIELD_19, @FIELD_20, @FIELD_21, @FIELD_22, @FIELD_23, @FIELD_24, @SHELFLIFE_DATA_1, 
                                @SHELFLIFE_DATA_2, @FIELD_25, @FIELD_26, @BATCH_MANAGEMENT, @FIELD_27, @FIELD_28, @FIELD_29, @BSTME, @ORDER_UNIT_1, @ORDER_UNIT_2, @AUSME, @UNIT_OF_ISSUE, 
                                @FIELD_30, @BSTMI, @STEUC, @WERKS, @LGORT, @DATE_CREATED, @DATE_MODIFIED, @CREATED_BY, @MODIFIED_BY";
                                cmd.CommandText = query;
                                cmd.CommandType = CommandType.Text;
                                foreach (var detail in details)
                                {
                                    cmd.Parameters.Clear();
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_0", detail.FIELD_0));
                                    cmd.Parameters.Add(new MySqlParameter("LOKZ", detail.LOKZ));
                                    cmd.Parameters.Add(new MySqlParameter("EXTWG", detail.EXTWG));
                                    cmd.Parameters.Add(new MySqlParameter("WRKST", detail.WRKST));
                                    cmd.Parameters.Add(new MySqlParameter("MAKTL", detail.MAKTL));
                                    cmd.Parameters.Add(new MySqlParameter("MAKTX", detail.MAKTX));
                                    cmd.Parameters.Add(new MySqlParameter("MATNR", detail.MATNR));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_1", detail.FIELD_1));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_2", detail.FIELD_2));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_3", detail.FIELD_3));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_4", detail.FIELD_4));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_5", detail.FIELD_5));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_6", detail.FIELD_6));
                                    cmd.Parameters.Add(new MySqlParameter("MFRNR", detail.MFRNR));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_7", detail.FIELD_7));
                                    cmd.Parameters.Add(new MySqlParameter("PO_TEXT", detail.PO_TEXT));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_8", detail.FIELD_8));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_9", detail.FIELD_9));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_10", detail.FIELD_10));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_10_1", detail.FIELD_10_1));
                                    cmd.Parameters.Add(new MySqlParameter("XCHPF", detail.XCHPF));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_11", detail.FIELD_11));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_12", detail.FIELD_12));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_13", detail.FIELD_13));
                                    cmd.Parameters.Add(new MySqlParameter("FROM_REORDER_PLANNING", detail.FROM_REORDER_PLANNING));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_14", detail.FIELD_14));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_15", detail.FIELD_15));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_16", detail.FIELD_16));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_17", detail.FIELD_17));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_18", detail.FIELD_18));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_19", detail.FIELD_19));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_20", detail.FIELD_20));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_21", detail.FIELD_21));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_22", detail.FIELD_22));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_23", detail.FIELD_23));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_24", detail.FIELD_24));
                                    cmd.Parameters.Add(new MySqlParameter("SHELFLIFE_DATA_1", detail.SHELFLIFE_DATA_1));
                                    cmd.Parameters.Add(new MySqlParameter("SHELFLIFE_DATA_2", detail.SHELFLIFE_DATA_2));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_25", detail.FIELD_25));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_26", detail.FIELD_26));
                                    cmd.Parameters.Add(new MySqlParameter("BATCH_MANAGEMENT", detail.BATCH_MANAGEMENT));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_27", detail.FIELD_27));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_28", detail.FIELD_28));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_29", detail.FIELD_29));
                                    cmd.Parameters.Add(new MySqlParameter("BSTME", detail.BSTME));
                                    cmd.Parameters.Add(new MySqlParameter("ORDER_UNIT_1", detail.ORDER_UNIT_1));
                                    cmd.Parameters.Add(new MySqlParameter("ORDER_UNIT_2", detail.ORDER_UNIT_2));
                                    cmd.Parameters.Add(new MySqlParameter("AUSME", detail.AUSME));
                                    cmd.Parameters.Add(new MySqlParameter("UNIT_OF_ISSUE", detail.UNIT_OF_ISSUE));
                                    cmd.Parameters.Add(new MySqlParameter("FIELD_30", detail.FIELD_30));
                                    cmd.Parameters.Add(new MySqlParameter("BSTMI", detail.BSTMI));
                                    cmd.Parameters.Add(new MySqlParameter("STEUC", detail.STEUC));
                                    cmd.Parameters.Add(new MySqlParameter("WERKS", detail.WERKS));
                                    cmd.Parameters.Add(new MySqlParameter("LGORT", detail.LGORT));
                                    cmd.Parameters.Add(new MySqlParameter("DATE_CREATED", DateTime.Now));
                                    cmd.Parameters.Add(new MySqlParameter("DATE_MODIFIED", DateTime.Now));
                                    cmd.Parameters.Add(new MySqlParameter("CREATED_BY", 6888));
                                    cmd.Parameters.Add(new MySqlParameter("MODIFIED_BY", 6888));

                                    MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                                    DataSet ds = new DataSet();
                                    myDA.Fill(ds);
                                }
                            }
                            catch (Exception ex)
                            {
                                details.Clear();
                                SAPMaterialMaster error = new SAPMaterialMaster();
                                error.ErrorMessage = ex.Message;
                                details.Add(error);
                            }
                            finally
                            {
                                cmd.Connection.Close();
                            }
                        }
                    }
                }
                response.Message = "You Successfully connected to PRM.";
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                response.Message = ex.Message;
            }
            return response;
        }