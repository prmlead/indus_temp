


update auctiondetails SET U_ID = 'TARGET_UID' where U_ID = 'SOURCE_UID' and REQ_ID in (select REQ_ID from auctiondetails WHERE U_ID = 'SOURCE_UID');

update quotations SET U_ID = 'TARGET_UID' where U_ID = 'SOURCE_UID' and REQ_ID in (select REQ_ID from quotations WHERE U_ID = 'SOURCE_UID');

update surrogatebidding SET VENDOR_ID = 'TARGET_UID' where VENDOR_ID = 'SOURCE_UID' and REQ_ID in (select REQ_ID from surrogatebidding WHERE VENDOR_ID = 'SOURCE_UID');

update qcs_vendor_item_assignment SET VENDOR_ID = 'TARGET_UID' where VENDOR_ID = 'SOURCE_UID' and REQ_ID in (select REQ_ID from qcs_vendor_item_assignment WHERE VENDOR_ID = 'SOURCE_UID');

update POGenerateDetails SET VENDOR_ID = 'TARGET_UID' where VENDOR_ID = 'SOURCE_UID' and REQ_ID in (select REQ_ID from POGenerateDetails WHERE VENDOR_ID = 'SOURCE_UID');

update negotiationaudit SET U_ID = 'TARGET_UID' where U_ID = 'SOURCE_UID' and REQ_ID in (select REQ_ID from negotiationaudit WHERE U_ID = 'SOURCE_UID');

update vendors SET IS_VALID = 0 WHERE U_ID in ('SOURCE_UID');