﻿using System;
using System.Collections.Generic;
using System.Linq;
using PRMServices.Models;
using System.ServiceModel.Activation;
using PRMServices.Models.Settings.Registrations;
using PRMServices.Infrastructure.Mapper;
using PRM.Services.Configurations;
using PRM.Core.Domain.Configurations;
using PRM.Core;
using PRM.Core.Domain.Configurations.Requirements;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMSettingsService : IPRMSettingsService
    {
        public PRMSettingsService(IRegistrationFormService registrationSettingFactory, IWorkContext workContext, IRequirementFormService requirementFormFactory)
        {
            this._registrationSettingFactory = registrationSettingFactory;
            this._workContext = workContext;
            _requirementFormFactory = requirementFormFactory;
        }

        private readonly IRegistrationFormService _registrationSettingFactory;
        private readonly IRequirementFormService _requirementFormFactory;
        private readonly IWorkContext _workContext;

        public Response DeleteRegistraionField(int id)
        {
           var entity = _registrationSettingFactory.GetRegistrationFormField(id);

            entity.Deleted = true;
            entity.UpdatedOnUtc = DateTime.UtcNow;
            entity.UpdatedBy = _workContext.CurrentUser;
           _registrationSettingFactory.UpdateRegistrationFormField(entity);

            return new Response() { Message = "Successfully deleted" };

        }


        public List<RegistrationForm> GetRegistrationSettings()
        {
            var list = _registrationSettingFactory.GetRegistrationForm(Convert.ToInt32(_workContext.CurrentCompany)).OrderBy(e=>e.DisplayOrder);
            return list.ToList();

        }

        public Response InsertRegistraionField(RegistrationFormModel model)
        {

            var entity = model.ToEntity();
            entity.UpdatedOnUtc = DateTime.UtcNow;
            entity.CreatedOnUtc = DateTime.UtcNow;
            entity.CreatedBy = _workContext.CurrentUser;
            entity.UpdatedBy = _workContext.CurrentUser;
            entity.Company_Id = _workContext.CurrentCompany;
            _registrationSettingFactory.InsertRegistrationFormField(entity);

            return new Response() { Message = "Successfully created" };
        }

        public Response UpdateRegistraionField(RegistrationFormModel model )
        {
            var entity = _registrationSettingFactory.GetRegistrationFormField(model.Id);

            if(entity != null)
            {
                entity = model.ToEntity(entity);
                entity.UpdatedOnUtc = DateTime.UtcNow;
                entity.UpdatedBy = _workContext.CurrentUser;
                _registrationSettingFactory.UpdateRegistrationFormField(entity);

            }

            return new Response() { Message = "Successfully updated" };




        }

        public List<RequirementForm> GetRequirementsSettings()
        {
            var list = _requirementFormFactory.GetRequirementForm(Convert.ToInt32(_workContext.CurrentCompany)).OrderBy(e => e.DisplayOrder);
            return list.ToList();
        }

        public Response InsertRequirementField(RequirementFormModel model)
        {
            var entity = model.ToEntity();
            entity.UpdatedOnUtc = DateTime.UtcNow;
            entity.CreatedOnUtc = DateTime.UtcNow;
            entity.CreatedBy = _workContext.CurrentUser;
            entity.UpdatedBy = _workContext.CurrentUser;
            entity.Company_Id = _workContext.CurrentCompany;
            _requirementFormFactory.InsertRequirementFormField(entity);

            return new Response() { Message = "Successfully created" };
        }

        public Response UpdateRequirementField(RequirementFormModel model)
        {
            var entity = _requirementFormFactory.GetRequirementFormField(model.Id);

            if (entity != null)
            {
                entity = model.ToEntity(entity);
                entity.UpdatedOnUtc = DateTime.UtcNow;
                entity.UpdatedBy = _workContext.CurrentUser;
                _requirementFormFactory.UpdateRequirementFormField(entity);

            }

            return new Response() { Message = "Successfully updated" };
        }

        public Response DeleteRequirementField(int id)
        {
            var entity = _requirementFormFactory.GetRequirementFormField(id);

            entity.Deleted = true;
            entity.UpdatedOnUtc = DateTime.UtcNow;
            entity.UpdatedBy = _workContext.CurrentUser;
            _requirementFormFactory.UpdateRequirementFormField(entity);

            return new Response() { Message = "Successfully deleted" };
        }
    }
}
