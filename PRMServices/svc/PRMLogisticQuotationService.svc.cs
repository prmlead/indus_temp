﻿using System;
using System.Collections.Generic;
using System.Linq;
using PRM.Core.Domain.Logistics;
using System.ServiceModel.Activation;
using PRM.Services.Logistics;
using PRM.Core.Models;
using PRMServices.Models.Logistics;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PRMLogisticQuotationService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PRMLogisticQuotationService.svc or PRMLogisticQuotationService.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMLogisticQuotationService : IPRMLogisticQuotationService
    {

        public PRMLogisticQuotationService(ILogisticQuotationService logisticQuotationService)
        {
            _logisticQuotationService = logisticQuotationService;

        }

        private readonly ILogisticQuotationService _logisticQuotationService;

        /// <summary>
        ///
        /// </summary>
        /// <param name="rid">requriment id</param>
        /// <returns></returns>
        /// 

        public IList<LogisticQuotationReport> GetLogisticQuotation(string rid)
        {

          return  _logisticQuotationService.GetLogisticQuotationByRequrimentId(Convert.ToInt32(rid)).Where(e=>e.VendorCompanyName != "PRICE_CAP").ToList();
        }

        public Response UpdateQuotationCharges(LogisticQuotationUpdateModel model)
        {

            _logisticQuotationService.UpdateQuotationCharges(new LogisticQuotationReport() { QuotationId = model.QuotationId, CustomClearance = model.CustomClearance, ServiceCharges = model.ServiceCharges, TerminalHandling = model.TerminalHandling, InvoiceNumber = model.InvoiceNumber, InvoiceDate = model.InvoiceDate, ConsigneeName = model.ConsigneeName,
            netWeight = model.netWeight,natureOfGoods = model.natureOfGoods,finalDestination = model.finalDestination,productIDorName = model.productIDorName});
            //
            return new Response() { Message = "Charges are updated" };
        }

        public Response UpdateQuotationChargesList(IList<LogisticQuotationUpdateModel> model)
         {


            foreach (var item in model)
            {
                _logisticQuotationService.UpdateQuotationCharges(new LogisticQuotationReport() { QuotationId = item.QuotationId, CustomClearance = item.CustomClearance, ServiceCharges = item.ServiceCharges, TerminalHandling = item.TerminalHandling, InvoiceNumber = item.InvoiceNumber, InvoiceDate = item.InvoiceDate, ConsigneeName = item.ConsigneeName, netWeight = item.netWeight, natureOfGoods = item.natureOfGoods,
                finalDestination = item.finalDestination,productIDorName = item.productIDorName,palletizeNumber = item.palletizeNumber,AirwayBillNumber = item.AirwayBillNumber,
                ShippingBillNumber = item.ShippingBillNumber,DrawbackAmount = item.DrawbackAmount,LicenseNumber = item.LicenseNumber,LoadingUnloadingCharges = item.LoadingUnloadingCharges,
                WarehouseStorages = item.WarehouseStorages,AdditionalMiscExp = item.AdditionalMiscExp,NarcoticSubcharges = item.NarcoticSubcharges,CustomerName = item.CustomerName});
                
            }

            return new Response() { Message = "Charges are updated" };
        }
    }
}
