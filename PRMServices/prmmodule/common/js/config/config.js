angular.module('commonModule')
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/header');

    $stateProvider
        .state('header', {
            url: '/header',
            templateUrl: '/partials/headerfooter.html',
            controller: 'headerCtrl'
        });
}]);