angular = require('angular');

angular.module('storeModule')
    // =========================================================================
    // Auction Details Services
    // =========================================================================
    .service('storeService', ['$http', 'store', '$state', '$rootScope', 'storeDomain', 'version', 'loginService', function ($http, store, $state, $rootScope, storeDomain, version, loginService) {
        //var domain = 'http://182.18.169.32/services/';
        var storeService = this;

        storeService.savestore = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'savestore',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        storeService.deletestore = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'deletestore',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        storeService.getcompanystores = function (compID) {
            return $http({
                method: 'GET',
                url: storeDomain + 'companystores?compid=' + compID + '&sessionid=' + loginService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }


        storeService.getstores = function (storeID, compID) {
            return $http({
                method: 'GET',
                url: storeDomain + 'stores?storeid=' + storeID + '&compid' + compID + '&sessionid=' + loginService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }





        storeService.savestoreitem = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'savestoreitem',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }


        storeService.deletestoreitem = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'deletestoreitem',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }


        storeService.getcompanystoreitems = function (storeID, itemID) {
            return $http({
                method: 'GET',
                url: storeDomain + 'storeitems?storeid=' + storeID + '&itemid=' + itemID + '&sessionid=' + loginService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }



        storeService.importentity = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'importentity',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }



        storeService.storesitempropvalues = function (itemID) {
            return $http({
                method: 'GET',
                url: storeDomain + 'storesitempropvalues?itemid=' + itemID + '&sessionid=' + loginService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }


        storeService.savestoreitemprop = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'savestoreitemprop',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        storeService.savestoreitempropvalue = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'savestoreitempropvalue',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }
        
        return storeService;
    }]);






