angular = require('angular');

angular.module('profileModule')
.controller('profileMyVendorsCtrl', ["$scope", "growlService", "$http", "domain", "$log", "loginService", "profileService", function ($scope, growlService, $http, domain, $log, loginService, profileService) {

	$scope.newVendor = {};

	$scope.vendorsList = [];
	$scope.inactiveVendorsList = [];

	var loginUserData = loginService.getUserObj();
	$scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.currencyvalidation = $scope.knownSincevalidation = false;

	$scope.ImportEntity = {};


	$scope.isOTPVerified = loginUserData.isOTPVerified;
	$scope.isEmailOTPVerified = loginUserData.isEmailOTPVerified;

	$scope.addVendorValidationStatus = false;
	$scope.addVendorShow = false;
	$scope.addVendor = 0;

	$scope.userDetails = {
            achievements: "",
            assocWithOEM: false,
            clients: "",
            establishedDate: "01-01-1970",
            aboutUs: "",
            logoFile: "",
            logoURL: "",
            products: "",
            strengths: "",
            responseTime: "",
            oemCompanyName: "",
            oemKnownSince: "",
            workingHours: "",
            files: [],
            directors: "",
            address: "",
            dateshow: 0

    };

	$scope.currencies = [];
	$scope.timezones = [];

	$scope.userObj = {};
	
	profileService.getUserDataNoCache()
	.then(function(response){
		$scope.userObj = response;
	})

	$scope.GetCompanyVendors = function () {
			$scope.isOTPVerified = loginUserData.isOTPVerified;
			$scope.isEmailOTPVerified = loginUserData.isEmailOTPVerified;
			$scope.params = { "userID": loginService.getUserId(), "sessionID": loginService.getUserToken() }

			if ($scope.isOTPVerified && $scope.isEmailOTPVerified) {
				profileService.GetCompanyVendors($scope.params)
					.then(function (response) {
						
						$scope.vendorsList = _.filter(response, ['isValid', true]);
						$scope.inactiveVendorsList = _.filter(response, ['isValid', false]);
				});
			}
		}
	$scope.GetCompanyVendors();

	$scope.activatecompanyvendor = function (vendorID, isValid) {
		profileService.activatecompanyvendor({ "customerID": loginService.getUserId(), "vendorID": vendorID, "isValid": isValid })
			.then(function (response) {
				if (response.errorMessage != "") {
					growlService.growl(response.errorMessage, "inverse");
				}
				else if (response.errorMessage == "") {
					$scope.GetCompanyVendors();
					if (isValid == 0) {
						growlService.growl("Vendor deleted Successfully", "inverse");
					}
					else if (isValid == 1) {
						growlService.growl("Vendor Activated Successfully", "success");
					}
				}
			});
	}

	$scope.addVendorValidation = function () {
		$scope.emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		$scope.mobileRegx = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
		$scope.panregx = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;

		$scope.addVendorValidationStatus = false;
		$scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.currencyvalidation = $scope.knownSincevalidation = false;

			if ($scope.newVendor.firstName == "" || $scope.newVendor.firstName === undefined) {
				$scope.firstvalidation = true;
				$scope.addVendorValidationStatus = true;
				return;
			}
			if ($scope.newVendor.lastName == "" || $scope.newVendor.lastName === undefined) {
				$scope.lastvalidation = true;
				$scope.addVendorValidationStatus = true;
				return;
			}
			if ($scope.newVendor.contactNum == "" || $scope.newVendor.contactNum === undefined || isNaN($scope.newVendor.contactNum)) {
				$scope.contactvalidation = true;
				$scope.addVendorValidationStatus = true;
				return;
			} else if (!$scope.mobileRegx.test($scope.newVendor.contactNum)) {
				$scope.contactvalidationlength = true;
				$scope.addVendorValidationStatus = true;
				return;
			}
			if ($scope.newVendor.email == "" || $scope.newVendor.email === undefined) {
				$scope.emailvalidation = true;
				$scope.addVendorValidationStatus = true;
				return;
			} else if (!$scope.emailRegx.test($scope.newVendor.email)) {
				$scope.emailregxvalidation = true;
				$scope.addVendorValidationStatus = true;
				return;
			}
			if ($scope.checkVendorEmailUniqueResult || $scope.checkVendorEmailUniqueResult) {
				$scope.addVendorValidationStatus = true;
				return;
			}
			if ($scope.newVendor.knownSince == "" || $scope.newVendor.knownSince === undefined) {
				$scope.knownSincevalidation = true;
				$scope.addVendorValidationStatus = true;
				return;
			}
			if ($scope.newVendor.companyName == "" || $scope.newVendor.companyName === undefined) {
				$scope.companyvalidation = true;
				$scope.addVendorValidationStatus = true;
				return;
			}
			if ($scope.newVendor.category == "" || $scope.newVendor.category === undefined) {
				$scope.categoryvalidation = true;
				$scope.addVendorValidationStatus = true;
				return;
			}
			if ($scope.newVendor.currency == "" || $scope.newVendor.currency === undefined) {
				$scope.currencyvalidation = true;
				$scope.addVendorValidationStatus = true;
				return;
			}

	}

	$scope.addVendor = function () {
			$scope.addVendorValidation();
			if ($scope.addVendorValidationStatus) {
				return false;
			}
			var vendCAtegories = [];
			$scope.newVendor.category = $scope.newVendor.category;
			vendCAtegories.push($scope.newVendor.category);
			var params = {
				
				"register": {
					"firstName": $scope.newVendor.firstName,
					"lastName": $scope.newVendor.lastName,
					"email": $scope.newVendor.email,
					"phoneNum": $scope.newVendor.contactNum,
					"username": $scope.newVendor.contactNum,
					"password": $scope.newVendor.contactNum,
					"companyName": $scope.newVendor.companyName ? $scope.newVendor.companyName : "",
					"isOTPVerified": 0,
					"category": $scope.newVendor.category,
					"userType": "VENDOR",
					"panNumber": ("panno" in $scope.newVendor) ? $scope.newVendor.panno : "",
					"stnNumber": ("serviceTaxNo" in $scope.newVendor) ? $scope.newVendor.serviceTaxNo : "",
					"vatNumber": ("vatNum" in $scope.newVendor) ? $scope.newVendor.vatNum : "",
					"referringUserID": loginService.getUserId(),
					"knownSince": ("knownSince" in $scope.newVendor) ? $scope.newVendor.knownSince : "",
					"errorMessage": "",
					"sessionID": "",
					"userID": 0,
					"department": "",
					"currency": $scope.newVendor.currency.key
				}
			};
			$http({
				method: 'POST',
				url: domain + 'register',
				encodeURI: true,
				headers: { 'Content-Type': 'application/json' },
				data: params
			}).then(function (response) {
				if (response && response.data && response.data.errorMessage == "") {
					$scope.newVendor = null;
					$scope.newVendor = {};
					$scope.addVendorShow = false;
					growlService.growl("Vendor Added Successfully.", 'inverse');
					location.reload();
					$scope.addVendor = 0;
				} else if (response && response.data && response.data.errorMessage) {
					growlService.growl(response.data.errorMessage, 'inverse');
				} else {
					growlService.growl('Unexpected Error Occurred', 'inverse');
				}
			});
		}
				
		$scope.getFile1 = function (id, doctype, ext) {
			$scope.progress = 0;
			$scope.file = $("#" + id)[0].files[0];
			$scope.docType = doctype + "." + ext;
			fileReader.readAsDataUrl($scope.file, $scope)
				.then(function (result) {
					if (id == "vendorsAttachment") {
						if (ext != "xlsx") {
							swal("Error!", "File type should be XSLX. Please download the template and  fill values accordingly.", "error");
							return;
						}
						var bytearray = new Uint8Array(result);
						$scope.ImportEntity.attachment = $.makeArray(bytearray);
						$scope.ImportEntity.attachmentFileName = $scope.file.name;
						$scope.AddVendorsExcel();
					}
				});
		};
		
		$scope.AddVendorsExcel = function () { 
		var params = {
			"entity": {
				attachment: $scope.ImportEntity.attachment,
				userid: parseInt(loginService.getUserId()),
				entityName: 'AddVendor',
				sessionid: loginService.getUserToken()
			}
		}
		profileService.AddVendorsExcel(params)
		.then(function (response) {
			if (response.errorMessage == "") {
				swal({
					title: "Thanks!",
					text: "Vendors Added Successfully",
					type: "success",
					showCancelButton: false,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Ok",
					closeOnConfirm: true
				},
					function () {
						location.reload();
					});
			} else {
				swal({
					title: "Failed!",
					text: response.errorMessage,
					type: "error",
					showCancelButton: false,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Ok",
					closeOnConfirm: true
				},
					function () {
						location.reload();
					});
			}
		})
	}

	$scope.getKeyValuePairs = function (parameter) {

            loginService.getKeyValuePairs(parameter)
            .then(function (response) {
                if (parameter == "CURRENCY") {
                    $scope.currencies = response;

                } else if (parameter == "TIMEZONES") {
                    $scope.timezones = response;

                }
				$scope.selectedCurrency = _.filter($scope.currencies, ['value', response.currency]);                
                $scope.selectedCurrency = $scope.selectedCurrency[0];
				
            })
	}

	$scope.callGetUserDetails = function () {
            $log.info("IN GET USER DETAILS");
            profileService.getProfileDetails({ "userid": loginService.getUserId(), "sessionid": loginService.getUserToken() })
                .then(function (response) {
                    $scope.userStatus = "registered";
                    if (response != undefined) {
                        $scope.userDetails = response;                        

                        $http({
                            method: 'GET',
                            url: domain + 'getcategories?userid=' + loginService.getUserId() + '&sessionid=' + loginService.getUserToken(),
                            encodeURI: true,
                            headers: { 'Content-Type': 'application/json' }
                        }).then(function (response) {
                            if (response && response.data) {
                                if (response.data.length > 0) {
                                    
									$scope.totalSubcats = _.filter(response.data, ['category', $scope.userDetails.category]);
                                    $scope.selectedCurrency = _.filter($scope.currencies, ['value', response.currency]);
									
                                    $log.info("IN GET USER DETAILS " + $scope.selectedCurrency);
                                    $scope.selectedCurrency = $scope.selectedCurrency[0];
                                    if ($scope.userDetails.subcategories && $scope.userDetails.subcategories.length > 0) {
                                        for (i = 0; i < $scope.userDetails.subcategories.length; i++) {
                                            for (j = 0; j < $scope.totalSubcats.length; j++) {
                                                if ($scope.userDetails.subcategories[i].id == $scope.totalSubcats[j].id) {
                                                    $scope.totalSubcats[j].ticked = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                            }
                        }, function (result) {
                        });
                        if ($scope.userDetails.subcategories && $scope.userDetails.subcategories.length > 0) {
                            for (i = 0; i < $scope.userDetails.subcategories.length; i++) {
                                $scope.subcategories += $scope.userDetails.subcategories[i].subcategory + ";";
                            }
                        }
                        var data = response.establishedDate;
                        var date = new Date(parseInt(data.substr(6)));
                        $scope.userDetails.establishedDate = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
                        
                        var today = new Date();
                        var todayDate = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
                        $scope.userDetails.dateshow = 0;
                        if($scope.userDetails.establishedDate == todayDate){
                            $scope.userDetails.dateshow = 1;
                        } 
                    }

                    $log.info($scope.userDetails);
                });       
        }

        $scope.callGetUserDetails();

	}]);