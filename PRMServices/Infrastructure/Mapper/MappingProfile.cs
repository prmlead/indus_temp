﻿using AutoMapper;
using PRM.Core.Domain.Configurations;
using PRM.Core.Domain.Configurations.Requirements;
using PRM.Core.Domain.Masters.ContactDetails;
using PRM.Core.Domain.Masters.DeliveryLocations;
using PRM.Core.Domain.Masters.DeliveryTerms;
using PRM.Core.Domain.Masters.GeneralTerms;
using PRM.Core.Domain.RealTimePrices;
using PRM.Core.Domain.Requirments;
using PRMServices.Models.Masters;
using PRMServices.Models.Requirements;
using PRMServices.Models.Settings.Registrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRMServices.Infrastructure.Mapper
{
    public class MappingProfile : Profile

    {
        public MappingProfile()
        {
            CreateMap<RegistrationFormModel, RegistrationForm>();
            CreateMap<RequirementFormModel, RequirementForm>();

            CreateMap<ExcelRealTimePrice, RealTimePrice>().ForMember(dest => dest.PriceDate, src=> src.MapFrom(e=>e.Date));

            CreateMap<PaymentTermModel, PaymentTerm>();
            CreateMap<GeneralTermModel, GeneralTerm>();
            CreateMap<DeliveryLocationModel, DeliveryLocation>();
            CreateMap<DeliveryTermModel, DeliveryTerm>();
            CreateMap<ContactDetailModel, ContactDetail>();
            CreateMap<RequirementTemplateModel, RequirementTemplate>();

        }
    }
}
