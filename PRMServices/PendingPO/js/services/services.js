﻿prmApp.constant('PRMPOServiceDomain', 'svc/PRMPOService.svc/REST/');
prmApp.service('PRMPOService', ["PRMPOServiceDomain", "SAPIntegrationServicesDomain", "userService", "httpServices",
    function (PRMPOServiceDomain, SAPIntegrationServicesDomain, userService, httpServices) {

        var PRMPOService = this;

        PRMPOService.getPOScheduleList = function (params) {

            params.onlycontracts = params.onlycontracts ? params.onlycontracts : 0;
            params.excludecontracts = params.excludecontracts ? params.excludecontracts : 0;

            let url = PRMPOServiceDomain + 'getposchedulelist?compid=' + params.compid + '&uid=' + params.uid + '&search=' + params.search + '&categoryid=' + params.categoryid
                + '&productid=' + params.productid + '&supplier=' + params.supplier + '&postatus=' + params.postatus
                + '&deliverystatus=' + params.deliverystatus
                + '&plant=' + params.plant
                + '&fromdate=' + params.fromdate + '&todate=' + params.todate
                + '&onlycontracts=' + params.onlycontracts + '&excludecontracts=' + params.excludecontracts + '&ackStatus=' + params.ackStatus + '&buyer=' + params.buyer + '&purchaseGroup=' + params.purchaseGroup
                + '&page=' + params.page
                + '&pagesize=' + params.pagesize + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPOService.getPOScheduleItems = function (params) {
            let url = PRMPOServiceDomain + 'getposcheduleitems?ponumber=' + params.ponumber + '&moredetails=' + params.moredetails + '&sessionid=' + userService.getUserToken();

            return httpServices.get(url);
        };

        PRMPOService.getPOScheduleFilterValues = function (params) {
            let url = PRMPOServiceDomain + 'getposchedulefilters?compid=' + params.compid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };


        PRMPOService.getPOInvoiceDetails = function (params) {
            let url = PRMPOServiceDomain + 'getpoinvoicedetails?ponumber=' + params.ponumber + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPOService.getASNDetails = function (params) {
            let url = PRMPOServiceDomain + 'getasndetails?compid=' + params.compid + '&asnid=' + params.asnid + '&ponumber=' + params.ponumber + '&grncode=' + params.grncode + '&asncode=' + params.asncode + '&sessionid=' + userService.getUserToken() + '&vendorid=' + params.vendorid;
            return httpServices.get(url);
        };

        PRMPOService.saveASNdetails = function (params) {
            let url = PRMPOServiceDomain + 'saveasndetails';
            return httpServices.post(url, params);
        };

        PRMPOService.savePOInvoice = function (params) {
            let url = PRMPOServiceDomain + 'savepoinvoice';
            return httpServices.post(url, params);
        };

        PRMPOService.savepoattachments = function (params) {
            let url = PRMPOServiceDomain + 'savepoattachments';
            return httpServices.post(url, params);
        };


        PRMPOService.poApproval = function (params) {
            let url = PRMPOServiceDomain + 'poapproval';
            return httpServices.post(url, params);
        };

        PRMPOService.deletePOInvoice = function (params) {
            let url = PRMPOServiceDomain + 'deletepoinvoice';
            return httpServices.post(url, params);
        };

        return PRMPOService;

    }]);