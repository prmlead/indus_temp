﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace PRMServices
{
    public static class ServerConfig
    {
        private static bool? _cachingEnabled;

        public static bool CachingEnabled
        {
            get 
            {
                if (_cachingEnabled == null)
                {
                    string keyStrVal = ConfigurationManager.AppSettings["CACHING_ENABLED"] as string;
                    if (!string.IsNullOrEmpty(keyStrVal))
                    {
                        _cachingEnabled = keyStrVal.ToLower() == "true";
                        return _cachingEnabled.GetValueOrDefault();
                    }

                }
                else
                {
                    return _cachingEnabled.GetValueOrDefault();
                }

                //set to default value
                _cachingEnabled = true;
                return true;
            }
            set
            {
                _cachingEnabled = value;
            }
        }

        private static int _slidingExpirationTime = Convert.ToInt32(ConfigurationManager.AppSettings["EXPIRATION_TIME"]);

        /// <summary>
        /// Sliding expiration time in seconds
        /// </summary>
        public static int SlidingExpirationTime
        {
            get 
            {
                return _slidingExpirationTime;
            }
            set
            {
                _slidingExpirationTime = value;
            }
        }



    }
}
