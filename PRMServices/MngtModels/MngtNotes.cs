﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class MngtNotes : Entity
    {
        [DataMember(Name="noteId")]
        public int NoteId { get; set; }

        [DataMember(Name = "userId")]
        public int UserId { get; set; }

        [DataMember(Name = "noteToId")]
        public int NoteToId { get; set; }

        [DataMember(Name = "notes")]
        public string Notes { get; set; }

        [DataMember(Name = "createdDate")]
        public DateTime? CreatedDate { get; set; }
    }
}