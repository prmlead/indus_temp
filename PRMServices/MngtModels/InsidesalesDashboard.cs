﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class InsidesalesDashboard : Entity
    {
        [DataMember(Name = "todaySuccessRation")]
        public double TodaySuccessRation { get; set; }

        [DataMember(Name = "overallSuccessRation")]
        public double OverallSuccessRation { get; set; }
        
        
        [DataMember(Name = "followUp")]
        public int FollowUp { get; set; }

        [DataMember(Name = "meetingSet")]
        public int MeetingSet { get; set; }

        [DataMember(Name = "followUpMeeting")]
        public int FollowUpMeeting { get; set; }

        [DataMember(Name = "positive")]
        public int Positive { get; set; }

        [DataMember(Name = "rejectedMeeting")]
        public int RejectedMeeting { get; set; }

        [DataMember(Name = "closure")]
        public int Closure { get; set; }

        [DataMember(Name = "followUpClosure")]
        public int FollowUpClosure { get; set; }

        [DataMember(Name = "rejectedClosure")]
        public int RejectedClosure { get; set; }

        [DataMember(Name = "trail")]
        public int Trail { get; set; }

        [DataMember(Name = "finalStage")]
        public int FinalStage { get; set; }
    }
}