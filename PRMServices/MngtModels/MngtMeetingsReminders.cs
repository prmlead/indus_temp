﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class MngtMeetingsReminders : Entity
    {
        [DataMember(Name = "companyID")]
        public int CompanyID { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "totalTurnover")]
        public string TotalTurnover { get; set; }

        [DataMember(Name = "contactName")]
        public string ContactName { get; set; }

        [DataMember(Name = "contactDesignation")]
        public string ContactDesignation { get; set; }

        [DataMember(Name = "mobileAndEmail")]
        public string MobileAndEmail { get; set; }

        [DataMember(Name = "meetingLocation")]
        public string MeetingLocation { get; set; }

        [DataMember(Name = "meetingDate")]
        public DateTime MeetingDate { get; set; }

        [DataMember(Name = "marketingAssTo")]
        public string MarketingAssTo { get; set; }

        [DataMember(Name = "insidesalesAssTo")]
        public string InsidesalesAssTo { get; set; }

        [DataMember(Name = "job")]
        public string Job { get; set; }
    }
}
