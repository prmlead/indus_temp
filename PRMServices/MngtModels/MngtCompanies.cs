﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class MngtCompanies : Entity
    {        

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "compID")]
        public int CompID { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "personWorking")]
        public string PersonWorking { get; set; }

        [DataMember(Name = "website")]
        public string Website { get; set; }

        [DataMember(Name = "cType")]
        public string CType { get; set; }

        [DataMember(Name = "totalTurnover")]
        public string TotalTurnover { get; set; }
        
        [DataMember(Name = "companyPhone")]
        public string CompanyPhone { get; set; }

        [DataMember(Name = "industry1")]
        public string Industry1 { get; set; }

        [DataMember(Name = "subIndustry1")]
        public string SubIndustry1 { get; set; }

        [DataMember(Name = "industry2")]
        public string Industry2 { get; set; }

        [DataMember(Name = "subIndustry2")]
        public string SubIndustry2 { get; set; }

        [DataMember(Name = "industry3")]
        public string Industry3 { get; set; }

        [DataMember(Name = "subIndustry3")]
        public string SubIndustry3 { get; set; }

        [DataMember(Name = "sector")]
        public string Sector { get; set; }

        [DataMember(Name = "levelOfOffice")]
        public string LevelOfOffice { get; set; }

        [DataMember(Name = "companyEntity")]
        public string CompanyEntity { get; set; }

        [DataMember(Name = "companyType")]
        public string CompanyType { get; set; }

        [DataMember(Name = "noOfEmployees")]
        public string NoOfEmployees { get; set; }

        [DataMember(Name = "addLine1")]
        public string AddLine1 { get; set; }

        [DataMember(Name = "addLine2")]
        public string AddLine2 { get; set; }

        [DataMember(Name = "city")]
        public string City { get; set; }

        [DataMember(Name = "state")]
        public string State { get; set; }

        [DataMember(Name = "pinCode")]
        public string PinCode { get; set; }

        [DataMember(Name = "stdCode")]
        public string StdCode { get; set; }

        [DataMember(Name = "compAssTo")]
        public int CompAssTo { get; set; }

        [DataMember(Name = "compAssBy")]
        public int CompAssBy { get; set; }

        [DataMember(Name = "insideSalesComments")]
        public string InsideSalesComments { get; set; }

        [DataMember(Name = "researchTeamComments")]
        public string ResearchTeamComments { get; set; }

        [DataMember(Name = "marketingTeamComments")]
        public string MarketingTeamComments { get; set; }

        [DataMember(Name = "generalComments")]
        public string GeneralComments { get; set; }

        [DataMember(Name = "workingHours")]
        public string WorkingHours { get; set; }

        [DataMember(Name = "priority")]
        public string Priority { get; set; }





        [DataMember(Name = "touchpointOutCome")]
        public string TouchpointOutCome { get; set; }

        [DataMember(Name = "touchpointComments")]
        public string TouchpointComments { get; set; }

        [DataMember(Name = "touchpointDateModified")]
        public DateTime? TouchpointDateModified { get; set; }

        [DataMember(Name = "touchpointMeetingDate")]
        public DateTime? TouchpointMeetingDate { get; set; }

        [DataMember(Name = "touchpointContactName")]
        public string TouchpointContactName { get; set; }

        [DataMember(Name = "touchpointContactDesg")]
        public string TouchpointContactDesg { get; set; }

        [DataMember(Name = "touchpointContactInfo")]
        public string TouchpointContactInfo { get; set; }

        [DataMember(Name = "touchpointId")]
        public int TouchpointId { get; set; }


    }
}