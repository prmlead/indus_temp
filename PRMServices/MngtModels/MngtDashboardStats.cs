﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class MngtDashboardStats : Entity
    {
        [DataMember(Name="totalTurnover")]
        public double TotalTurnover { get; set; }

        [DataMember(Name = "totalSavings")]
        public double TotalSavings { get; set; }

        [DataMember(Name = "avgSavingsValue")]
        public double AvgSavingsValue { get; set; }

        [DataMember(Name = "avgSavingsPercentage")]
        public double AvgSavingsPercentage { get; set; }

        [DataMember(Name = "noOfNegotiationsRunning")]
        public int NoOfNegotiationsRunning { get; set; }

        [DataMember(Name = "noOfNegotiationsCompleted")]
        public int NoOfNegotiationsCompleted { get; set; }

        [DataMember(Name = "noOfNegotiationsNotstarted")]
        public int NoOfNegotiationsNotstarted { get; set; }

        [DataMember(Name = "noOfCustomerRegisterd")]
        public int NoOfCustomerRegisterd { get; set; }

        [DataMember(Name = "noOfVendorsRegisterd")]
        public int NoOfVendorsRegisterd { get; set; }

        [DataMember(Name = "noOfCustomerRegisterdToday")]
        public int NoOfCustomerRegisterdToday { get; set; }

        [DataMember(Name = "noOfVendorsRegisterdToday")]
        public int NoOfVendorsRegisterdToday { get; set; }

        [DataMember(Name = "noOfSubscribedCustomer")]
        public int NoOfSubscribedCustomer { get; set; }

        

        [DataMember(Name = "noOfCallsMadeToday")]
        public int NoOfCallsMadeToday { get; set; }

        [DataMember(Name = "noOfMeetingsSetForToday")]
        public int NoOfMeetingsSetForToday { get; set; }

        

        [DataMember(Name = "noOfProspectiveClousers")]
        public int NoOfProspectiveClousers { get; set; }

        [DataMember(Name = "noOfClousers")]
        public int NoOfClousers { get; set; }

        [DataMember(Name = "tomorowMeetingsCount")]
        public int TomorowMeetingsCount { get; set; }

        [DataMember(Name = "listMeetings")]
        public List<MngtTouchpoint> ListMeetings { get; set; }

        [DataMember(Name = "listUserStatus")]
        public List<MngtUserDetails> ListUserStatus { get; set; }





        [DataMember(Name = "noOfMeetingsSetForMonth")]
        public int NoOfMeetingsSetForMonth { get; set; }

        [DataMember(Name = "noOfPositives")]
        public int NoOfPositives { get; set; }

        [DataMember(Name = "noOfTrails")]
        public int NoOfTrails { get; set; }

        [DataMember(Name = "noOfFinalStage")]
        public int NoOfFinalStage { get; set; }

        [DataMember(Name = "noOfClousersMonth")]
        public int NoOfClousersMonth { get; set; }

        [DataMember(Name = "noOfTotalClousers")]
        public int NoOfTotalClousers { get; set; }

        [DataMember(Name = "noOfFollowUpClousers")]
        public int NoOfFollowUpClousers { get; set; }



        [DataMember(Name = "todaySuccessRation")]
        public double TodaySuccessRation { get; set; }

        [DataMember(Name = "overallSuccessRation")]
        public double OverallSuccessRation { get; set; }
    }
}