﻿prmApp
    .controller('prCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService",
        "storeService", "growlService", "PRMPRServices", "poService", "$rootScope", "catalogService",
        "fileReader","workflowService",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService,
            storeService, growlService, PRMPRServices, poService, $rootScope, catalogService,
            fileReader, workflowService) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.compID = userService.getUserCompanyId();
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.prID = $stateParams.Id;

            $scope.companyItemUnits = [];
            $scope.companyLocation = [];
            $scope.companyLocationTemp = [];
            $scope.companyConfigStr = 'ITEM_UNITS,USER_LOCATION'
            //auctionsService.GetCompanyConfiguration($scope.compID, $scope.companyConfigStr, userService.getUserToken())
            //    .then(function (unitResponse) {

            //        $scope.companyConfigList = unitResponse;
            //        var countriesTemp = [];
            //        $scope.companyConfigList.forEach(function (item, index) {
            //           if (item.configKey == 'ITEM_UNITS') {
            //                $scope.companyItemUnits.push(item);
            //            } 
                       
            //        });

            //    });
            $scope.deptIDs = [];
            $scope.desigIDs = [];


            $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
            if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
                $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                    $scope.deptIDs.push(item.deptID);
                    item.listDesignation.forEach(function (item1, index1) {
                        if (item1.isAssignedToUser && item1.isValid) {
                            $scope.desigIDs.push(item1.desigID);
                        }
                    });
                });
            };

            $scope.compID = userService.getUserCompanyId();
            auctionsService.GetCompanyConfiguration($scope.compID, "ITEM_UNITS,USER_LOCATION", userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.companyItemUnits = unitResponse;


                    $scope.companyConfigList = unitResponse;
                    var countriesTemp = [];
                    $scope.companyConfigList.forEach(function (item, index) {
                        if (item.configKey == 'ITEM_UNITS') {
                            $scope.companyItemUnits.push(item);
                        }
                        if (item.configKey == 'USER_LOCATION') {
                            $scope.companyLocationTemp.push(item);
                        }
                    });
                    if ($scope.isSuperUser) {
                        
                        $scope.PRDetails.companyLocation = $scope.companyLocationTemp;
                    } else {
                        $scope.getuserLocations();
                    }
                    
                });
            $scope.companyLocationSearch = [];
            $scope.getuserLocations = function () {
                $scope.companyLocation = [];
                $scope.companyLocation1 = [];
                $scope.ListUserDepartmentDesignations.forEach(function (deptLoc, deptIdx) {
                    deptLoc.userLocationTemp = deptLoc.userLocation.split(',');
                    deptLoc.userLocationTemp.forEach(function (compLoc, locIdx) {
                        $scope.companyLocation1 = $scope.companyLocationTemp.filter(function (udd) {
                             if (udd.configValue == compLoc) {
                                 return udd;
                            };
                        });
                        $scope.companyLocation.push($scope.companyLocation1[0]);
                    })
                })
                $scope.companyLocationSearch = angular.copy($scope.companyLocation);
                $scope.PRDetails.companyLocation = angular.copy($scope.companyLocation);
            }
            

            /*region start WORKFLOW*/
            $scope.workflowList = [];
            $scope.itemWorkflow = [];
            $scope.obj = {
                auctionVendors: []
            };
            $scope.objNew = {
                auctionVendors: []
            };
            $scope.workflowObj = {};
            $scope.workflowObj.workflowID = 0;
            $scope.currentStep = 0;
            $scope.orderInfo = 0;
            $scope.assignToShow = '';
            $scope.isWorkflowCompleted = false;
            $scope.WorkflowModule = 'PR';
            $scope.disableWFSelection = false;
            /*region end WORKFLOW*/

            $scope.typesOfExpenditure = [
                {
                    display: '------------- Please Select Expenditure Type --------------',
                    value: ''
                },
                {
                    display: 'Equipment',
                    value: '0'
                },
                {
                    display: 'Consumables',
                    value: '1'
                },
                {
                    display: 'Consumables/Services',
                    value: '2'
                }
                //,
                //{
                //    display: '4 Wheeler Petrol',
                //    value: '3'
                //}
                //,
                //{
                //    display: 'Bill Submission',
                //    value: '4'
                //}
            ];

            $scope.companyBranches =
                [
                    {
                        display: 'LOCAL',
                        value: 1
                    },
                    {
                        display: 'CENTRAL',
                        value: 2
                    }
                ];

            //$scope.companyLocation =
            //    [
            //        {
            //            display: 'BANGALORE',
            //            value: 'BANGALORE'
            //        },
            //        {
            //            display: 'CHENNAI',
            //            value: 'CHENNAI'
            //        },
            //        {
            //            display: 'MUMBAI',
            //            value: 'MUMBAI'
            //        },
            //        {
            //            display: 'KOLKATA',
            //            value: 'KOLKATA'
            //        }
            //    ];


            $scope.natureOfRequirement = [
                {
                    display: '------------- Please Select Nature Of Requirement --------------',
                    value: ''
                },
                {
                    display: 'Product',
                    value: '0'
                },
                {
                    display: 'Service',
                    value: '1'
                },
                {
                    display: 'Product & Service',
                    value: '2'
                }
                //,
                //{
                //    display: '4 Wheeler Petrol',
                //    value: '3'
                //}
                //,
                //{
                //    display: 'Bill Submission',
                //    value: '4'
                //}
            ];

            $scope.prStatus = [
                {
                    display: 'PENDING',
                    value: 'PENDING'
                },
                {
                    display: 'COMPLETED',
                    value: 'COMPLETED'
                },
                {
                    display: 'CANCELLED',
                    value: 'CANCELLED'
                },
                {
                    display: 'REJECTED',
                    value: 'REJECTED'
                }
            ];


            $scope.GetSeries = function (series, deptID) {
                $scope.PRDetails.PR_NUMBER = 'INDUS/PR/' + userService.getSelectedUserDepartmentDesignation().deptCode;
            }

            $scope.companyItemUnits = [];

            $scope.companyDepartments = [];

            $scope.PRDetails = {
                isTabular: true,
                PR_ID: 0,
                U_ID: userService.getUserId(),
                COMP_ID: 0,
                PR_NUMBER: '',
                PR_TYPE: 'REGULAR_PURCHASE',
                ASSET_TYPE: '',
                PRIORITY: '',
                PRIORITY_COMMENTS: '',
                COMMENTS: '',
                DEPARTMENT: 0,
               // REQUEST_DATE: '',
                //REQUIRED_DATE: '',
                ATTACHMENTS: '',
                TOTAL_BASE_PRICE: 0,
                TOTAL_GST_PRICE: 0,
                TOTAL_PRICE: 0,
                SUB_TOTAL: 0,
                GST_PRICE: 0,
                IS_PR_SUBMITTED: 0,
                WF_ID:0,
                STATUS: 'PENDING',
                CLASSIFICATION: '',
                COMPANY: '',
                WORK_ORDER_DURATION: '',
                PURPOSE_IN_BRIEF:'',
                USER_LOCATION: '',
                PRItemsList: [],
                PROJECT: '',
                URGENCY: '',
                PURCHASE:true,
                PRSHOW: true,
                companyLocation: [],
                SelectedLocationArray: [],
                selectedLocations: ''
            };

            $scope.PRItem = {

                ITEM_ID: 0,
                PR_ID: 0,
                ITEM_NAME: '',
                HSN_CODE: '',
                ITEM_CODE: '',
                ITEM_DESCRIPTION: '',
                BRAND: '',
                UNITS: '',
                EXIST_QUANTITY: 0,
                REQUIRED_QUANTITY: 0,
                UNIT_PRICE: 0,
                C_GST_PERCENTAGE: 0,
                S_GST_PERCENTAGE: 0,
                I_GST_PERCENTAGE: 0,
                TOTAL_PRICE: 0,
                COMMENTS: '',
                ATTACHMENTS: '',
                ATTACHMENTSARR: [],
                CREATED_BY: 0,
                //CREATED_DATE: '',
                MODIFIED_BY: 0,
                //MODIFIED_DATE: '',
                CATALOGUE_ID: 0,
                U_ID: $scope.userID,
                sessionID: $scope.sessionID,
                itemAttachment: [],
                attachmentName: '',
                ITEM_NUM:''
            };

            auctionsService.GetCompanyDepartments($scope.userID, $scope.sessionID)
                .then(function (response) {
                    $scope.companyDepartments = response;
                });

          

            $scope.PRDetails.PRItemsList.push($scope.PRItem);

            $scope.addPrItem = function () {
                var PRItem = {
                    ITEM_ID: 0,
                    PR_ID: 0,
                    ITEM_NAME: '',
                    HSN_CODE: '',
                    ITEM_CODE: '',
                    ITEM_DESCRIPTION: '',
                    BRAND: '',
                    UNITS: '',
                    EXIST_QUANTITY: 0,
                    REQUIRED_QUANTITY: 0,
                    UNIT_PRICE: 0,
                    C_GST_PERCENTAGE: 0,
                    S_GST_PERCENTAGE: 0,
                    I_GST_PERCENTAGE: 0,
                    TOTAL_PRICE: 0,
                    COMMENTS: '',
                    ATTACHMENTS: '',
                    ATTACHMENTSARR: [],
                    CREATED_BY: 0,
                    //CREATED_DATE: '',
                    MODIFIED_BY: 0,
                    //MODIFIED_DATE: '',
                    CATALOGUE_ID: 0,
                    U_ID: $scope.userID,
                    sessionID: $scope.sessionID,
                    ITEM_NUM: '',
                    UOM: '',
                    MATERIAL_DESCRIPTION: ''
                };

                $scope.PRDetails.PRItemsList.push(PRItem);

            };

            $scope.checkIsFormDisable = function () {
                $scope.isFormdisabled = true;
                //if ($scope.PRDetails.CREATED_BY == $scope.userID) {
                //    $scope.isFormdisabled = false;
                //}
            };
           
            $scope.savePr = function () {
                $scope.PRDetails.U_ID = userService.getUserId();
                var params = {
                    "prdetails": $scope.PRDetails,
                    "sessionid": userService.getUserToken()
                };
                PRMPRServices.savePrDetails(params)
                    .then(function (response) {

                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            growlService.growl("Saved Successfully.", "success");
                            $state.go('list-pr');

                        }

                    });
            };

            $scope.submitPr = function (value) {
                // console.log("in submit ");

                if ($scope.PRDetails.PRSHOW == true && value) {

                    $scope.isValidPRNO = $scope.isValidConsumables = $scope.isValidNatureOfRequirement = $scope.isValidIndentDept = $scope.isValidReqDate = $scope.urgencyValidation = $scope.requestorinfovalidation = false;
                    $scope.userlocationvalidation = $scope.workFlowValidation = $scope.prTypeValidation = false;
                    // $scope.userEmployeeIDValidation = $scope.userEmployeeNameValidation = $scope.userEmployeeStatusValidation = 
                    $scope.isValidProject = false;
                    $scope.tableValidation = false;
                    if ($scope.PRDetails.PR_NUMBER == "" || $scope.PRDetails.PR_NUMBER == null || $scope.PRDetails.PR_NUMBER == 'undefined') {
                        $scope.isValidPRNO = true;
                        return;
                    }

                    if ($scope.PRDetails.ASSET_TYPE == "" || $scope.PRDetails.ASSET_TYPE == null || $scope.PRDetails.ASSET_TYPE == 'undefined') {
                        $scope.isValidConsumables = true;
                        return;
                    }

                    //if ($scope.PRDetails.REQUEST_DATE == "" || $scope.PRDetails.REQUEST_DATE == null || $scope.PRDetails.REQUEST_DATE == 'undefined') {
                    //    $scope.isValidDate = true;
                    //    return;
                    //}

                    //if ($scope.PRDetails.PRIORITY == "" || $scope.PRDetails.PRIORITY == null || $scope.PRDetails.PRIORITY == 'undefined') {
                    //    $scope.isValidNatureOfRequirement = true;
                    //    return;
                    //}

                    //if ($scope.PRDetails.PROJECT == "" || $scope.PRDetails.PROJECT == null || $scope.PRDetails.PROJECT == 'undefined') {
                    //    $scope.isValidProject = true;
                    //    return;
                    //}

                    if ($scope.PRDetails.DEPARTMENT == 0) {
                        $scope.isValidIndentDept = true;
                        return;
                    }

                    if ($scope.PRDetails.URGENCY == "" || $scope.PRDetails.URGENCY == null || $scope.PRDetails.URGENCY == 'undefined') {
                        $scope.urgencyValidation = true;
                        return;
                    }

                    if ($scope.PRDetails.PR_TYPE == "" || $scope.PRDetails.PR_TYPE == null || $scope.PRDetails.PR_TYPE == 'undefined') {
                        $scope.prTypeValidation = true;
                        return;
                    }

                    if ($scope.PRDetails.USER_LOCATION == "" || $scope.PRDetails.USER_LOCATION == null || $scope.PRDetails.USER_LOCATION == 'undefined') {
                        $scope.userlocationvalidation = true;
                        return;
                    }

                    if ($scope.workflowObj.workflowID == 0) {
                        $scope.workFlowValidation = true;
                        return;
                    }
                    

                    //if ($scope.PRDetails.EMPLOYEE_ID == "" || $scope.PRDetails.EMPLOYEE_ID == null || $scope.PRDetails.EMPLOYEE_ID == 'undefined') {
                    //    $scope.userEmployeeIDValidation = true;
                    //    return;
                    //}

                    //if ($scope.PRDetails.EMPLOYEE_NAME == "" || $scope.PRDetails.EMPLOYEE_NAME == null || $scope.PRDetails.EMPLOYEE_NAME == 'undefined') {
                    //    $scope.userEmployeeNameValidation = true;
                    //    return;
                    //}

                    //if ($scope.PRDetails.EMPLOYEE_STATUS == "" || $scope.PRDetails.EMPLOYEE_STATUS == null || $scope.PRDetails.EMPLOYEE_STATUS == 'undefined') {
                    //    $scope.userEmployeeStatusValidation = true;
                    //    return;
                    //}

                    $scope.PRDetails.PRItemsList.forEach(function (item, index) {

                        if ($scope.tableValidation) {
                            return false;
                        }

                        var itemsno = parseInt(index) + 1;
                        if (item.MATERIAL_DESCRIPTION == null || item.MATERIAL_DESCRIPTION == '') {
                            $scope.tableValidation = true;
                            $scope.tableValidationMsg = 'Please enter NAME(From Catalogue) for item: ' + itemsno;
                            return;
                        }
                        if (!item.MATERIAL_DELIVERY_DATE) {
                            $scope.tableValidation = true;
                            $scope.tableValidationMsg = 'Please enter Item Delivery Date for item: ' + itemsno;
                            return;
                        }
                        //if (!item.JUSTIFICATION) {
                        //    $scope.tableValidation = true;
                        //    $scope.tableValidationMsg = 'Please enter JUSTIFICATION for item: ' + itemsno;
                        //    return;
                        //}
                        if (item.UOM == 0) {
                            $scope.tableValidation = true;
                            $scope.tableValidationMsg = 'Please select UOM: ' + itemsno;
                            return;
                        }
                        //if (item.C_GST_PERCENTAGE >= 100) {
                        //    $scope.tableValidation = true;
                        //    $scope.tableValidationMsg = 'Please enter valid TAX for item: ' + itemsno;
                        //    return;
                        //}

                        if (item.REQUIRED_QUANTITY == null || item.REQUIRED_QUANTITY == '' || item.REQUIRED_QUANTITY <= 0) {
                            $scope.tableValidation = true;
                            $scope.tableValidationMsg = 'Please enter QUANTITY for item: ' + itemsno;
                            return;
                        }

                    });

                    if ($scope.tableValidation) {
                        return;
                    }
                }


                $scope.PRDetails.U_ID = userService.getUserId();


                var params = {
                    "prdetails": $scope.PRDetails,
                    "sessionid": userService.getUserToken()
                };

                //var ts = userService.toUTCTicks($scope.PRDetails.REQUEST_DATE);
                //var m = moment(ts);
                //var reqdate = new Date(m);
                //var milliseconds = parseInt(reqdate.getTime() / 1000.0);
                //params.prdetails.REQUEST_DATE = "/Date(" + milliseconds + "000++530)/";

                params.prdetails.WF_ID = $scope.workflowObj.workflowID;
                params.prdetails.DESIG_ID = userService.getSelectedUserDesigID();
                params.prdetails.COMP_ID = userService.getUserCompanyId();
                params.prdetails.IS_PR_SUBMITTED = value;

                if (params.prdetails.PRItemsList && params.prdetails.PRItemsList.length > 0) {
                    params.prdetails.PRItemsList.forEach(function (item, index) {
                        if (item.MATERIAL_DELIVERY_DATE) {
                            //var ts = userService.toUTCTicks(item.MATERIAL_DELIVERY_DATE);
                            //var m = moment(ts);
                            //var reqrddate = new Date(m);
                            //var milliseconds = parseInt(reqrddate.getTime() / 1000.0);
                            //item.MATERIAL_DELIVERY_DATE = "/Date(" + milliseconds + "000++530)/";

                            var ts = userService.toUTCTicks(item.MATERIAL_DELIVERY_DATE);
                            var m = moment(ts);
                            var quotationDate = new Date(m);
                            var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                            item.MATERIAL_DELIVERY_DATE = "/Date(" + milliseconds + "000+0530)/";
                        }
                    });
                }

                PRMPRServices.savePrDetails(params)
                    .then(function (response) {

                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            growlService.growl("Saved Successfully.", "success");
                            $state.go('list-pr');
                        }

                    });
            };


            $scope.getprdetails = function () {
                var params = {
                    "prid": $stateParams.Id,
                    "sessionid": userService.getUserToken()
                };
                PRMPRServices.getprdetails(params)
                    .then(function (response) {

                        $scope.PRDetails = response;
                        if ($scope.PRDetails.TYPE == "PR") {
                            $scope.PRDetails.PURCHASE = true;
                            $scope.PRDetails.PRSHOW = true;
                            $scope.PRDetails.DEPARTMENT_CODE = $scope.PRDetails.DEPARTMENT_NAME;
                        } else {
                            $scope.PRDetails.PURCHASE = false;
                            $scope.PRDetails.PRSHOW = false;
                        }
                     //   $scope.PRDetails.REQUEST_DATE = userService.toLocalDate($scope.PRDetails.REQUEST_DATE);//new moment($scope.PRDetails.REQUEST_DATE).format("YYYY-MM-DD HH:mm");
                     //   $scope.PRDetails.REQUIRED_DATE = userService.toLocalDate($scope.PRDetails.REQUIRED_DATE);//new moment($scope.PRDetails.REQUIRED_DATE).format("YYYY-MM-DD HH:mm");

                        if ($scope.PRDetails.PRItemsList.length > 0) {
                            $scope.PRDetails.PRItemsList.forEach(function (item, Idx) {
                                item.MATERIAL_DESCRIPTION = item.ITEM_NAME;
                                item.MATERIAL_DELIVERY_DATE = userService.toLocalDate(item.MATERIAL_DELIVERY_DATE);
                            })
                        }



                        if ($stateParams.Id > 0) {
                            $scope.checkIsFormDisable();

                            $scope.getItemWorkflow($scope.PRDetails.WF_ID, $scope.PRDetails.PR_ID, 'PR', '');
                        }
                    });
            };


            if ($stateParams.Id > 0) {
                $scope.getprdetails();
            } else {
                $scope.GetSeries('', userService.getSelectedUserDeptID());
            }

           


            $scope.deletePrItem = function (index) {
                if ($scope.PRDetails.PRItemsList.length > 1) {
                    $scope.PRDetails.PRItemsList.splice(index, 1);
                    $scope.prUnitPriceCalculation();
                } else {
                    growlService.growl("Can't Delete this Item", "inverse");
                }
            };
            
            $scope.prUnitPriceCalculation = function () {
                $scope.PRDetails.TOTAL_PRICE = 0;
                $scope.PRDetails.TOTAL_BASE_PRICE = 0;
                $scope.PRDetails.TOTAL_GST_PRICE = 0;

                $scope.PRDetails.PRItemsList.forEach(function (item, index) {
                    
                    if (item.UNIT_PRICE == undefined || item.UNIT_PRICE <= 0) {
                        item.UNIT_PRICE = 0;
                    };

                    if ($scope.PRDetails.PRItemsList[index].PRODUCT_RATE_TEMP < item.UNIT_PRICE) {
                        swal("Error!", "Expected Unit Price should be less than or equal to Product Fixed Price" + " " + $scope.PRDetails.PRItemsList[index].PRODUCT_RATE_TEMP + " ", "warning");
                        item.UNIT_PRICE = $scope.PRDetails.PRItemsList[index].PRODUCT_RATE_TEMP;
                        return;
                    }


                    item.TOTAL_BASE_PRICE = item.UNIT_PRICE * item.REQUIRED_QUANTITY;
                    item.TOTAL_PRICE = item.TOTAL_BASE_PRICE + ((item.TOTAL_BASE_PRICE / 100) * (item.C_GST_PERCENTAGE));

                    $scope.PRDetails.TOTAL_GST_PRICE += (item.TOTAL_BASE_PRICE / 100) * (item.C_GST_PERCENTAGE);
                    $scope.PRDetails.TOTAL_BASE_PRICE += item.TOTAL_BASE_PRICE;
                    $scope.PRDetails.TOTAL_PRICE += item.TOTAL_PRICE;

                });
            };

            $scope.userDepartments = [];
            $scope.userDepartments.push(userService.getSelectedUserDepartmentDesignation());
            if ($scope.PRDetails.PR_ID == 0) {
                $scope.PRDetails.DEPARTMENT = $scope.userDepartments[0].deptID;
                $scope.PRDetails.DEPT_CODE = $scope.userDepartments[0].deptCode;
                $scope.PRDetails.DEPARTMENT_CODE = $scope.userDepartments[0].deptCode;
              //  $scope.PRDetails.USER_LOCATION = userService.getSelectedUserDepartmentDesignation().userLocation;
               // $scope.PRDetails.USER_LOCATION = "NA";
            }
            $scope.goToPrList = function (id) {
                var url = $state.href("list-pr");
                window.open(url, '_self');
            };

            $scope.deptChanged = function () {
                $scope.userDepartments.filter(function (udd) {
                    if (udd.deptID == $scope.PRDetails.DEPARTMENT) {
                        $scope.PRDetails.DEPT_CODE = udd.deptCode;
                    }
                });
            };



            //#region Catalog
            $scope.productsList = [];
            //$scope.getProducts = function () {
            //    catalogService.getProducts($scope.compID)
            //        .then(function (response) {
            //            $scope.productsList = response;
            //        });
            //}
            //$scope.getProducts();

            $scope.getProducts = function (recordsFetchFrom, pageSize, searchString, index) {
                catalogService.getProducts($scope.compID, recordsFetchFrom * pageSize, pageSize, searchString ? searchString : "")
                    .then(function (response) {
                        $scope.productsList = [];
                        $scope.productsListTemp = [];
                        $scope.productsList = response;
                        $scope.productsListTemp = response;
                        $scope.fillProduct($scope.productsList, index);
                    });
            };

            $scope.autofillProduct = function (prodName, index) {
                if ($scope.PRDetails.PR_TYPE === undefined) {
                    swal("Error!", "Please Select Medical/Non-Medical PR Type to search for Products", "warning");
                    $scope.PRDetails.PRItemsList[index].ITEM_NAME = "";
                    return;
                }
                $scope['filterProducts_' + index] = null;
                //$scope['ItemSelected_' + index] = false;
                $scope.getProducts(0, 10, prodName, index);
            };

            //$scope.autofillProduct = function (prodName, index) {
            //    $scope['ItemSelected_' + index] = false;
            //    var output = [];
            //    if (prodName && prodName.trim() != '') {
            //        angular.forEach($scope.productsList, function (prod) {
            //            if (prod.prodName.toLowerCase().indexOf(prodName.trim().toLowerCase()) >= 0) {
            //                output.push(prod);
            //            }
            //        });
            //    }
            //    $scope["filterProducts_" + index] = output;
            //}
            $scope.fillTextbox = function (selProd, index) {
                $scope['ItemSelected_' + index] = true;
                $scope.PRDetails.PRItemsList[index].MATERIAL_DESCRIPTION = selProd.prodName;
                $scope.PRDetails.PRItemsList[index].ITEM_CODE = selProd.prodCode;
                $scope.PRDetails.PRItemsList[index].ITEM_NUM = "000" + ((index + 1) * 10);
                //ITEM_NUM
               // $scope.PRDetails.PRItemsList[index].CATALOGUE_ID = selProd.prodId;
                $scope.PRDetails.PRItemsList[index].PRODUCT_ID = selProd.prodId;
                $scope.PRDetails.PRItemsList[index].ITEM_DESCRIPTION = selProd.prodDesc;
                $scope.PRDetails.PRItemsList[index].UOM = selProd.prodQty;
                $scope.PRDetails.PRItemsList[index].HSN_CODE = selProd.prodHSNCode;
                $scope.PRDetails.PRItemsList[index].CATEGORY_ID = selProd.categoryId;
              //  $scope.PRDetails.PRItemsList[index].UNIT_PRICE = selProd.productRate;
                $scope.PRDetails.PRItemsList[index].PRODUCT_RATE_TEMP = 0;
                $scope.PRDetails.PRItemsList[index].PRODUCT_RATE_TEMP = selProd.productRate;
                $scope.PRDetails.PRItemsList[index].PRODUCT_RATE = selProd.productRate;
                $scope['filterProducts_' + index] = null;

                $scope.loadUserDepartments(selProd.prodId, index);

            }

            $scope.onBlurProduct = function (index) {
                if ($scope['ItemSelected_' + index] == false) {
                    $scope.PRDetails.PRItemsList[index].ITEM_NAME = "";
                }
            }
            //#endregion Catalog

            $scope.storeDetailsEdit = [];

            $scope.loadUserDepartments = function (productid, index) {

                var params = {
                    storeid: 0,
                    productid: productid
                }

                var inStock = 0, storeCode = '';
                $scope.inStock = 0;
                $scope.storeName = '';
                storeService.getcompanystoreitems(params.storeid, params.productid)
                    .then(function (response) {
                        $scope.storeDetails = response;
                        $scope.storeDetails.forEach(function (item, index) {
                            $scope.inStock += item.inStock;

                        });

                        //inStock = response[0].inStock;
                        $scope.PRDetails.PRItemsList[index].EXIST_QUANTITY = $scope.inStock;
                    })
            };

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.file = $("#" + id)[0].files[0];

                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {

                        if (id != "itemsAttachment") {
                            var bytearray = new Uint8Array(result);
                            var arrayByte = $.makeArray(bytearray);
                            var ItemFileName = $scope.file.name;
                            var index = id;
                            var obj = $scope.PRDetails.PRItemsList[index];
                            obj.itemAttachment = arrayByte;
                            obj.attachmentName = ItemFileName;
                            $scope.PRDetails.PRItemsList.splice(index, 1, obj);
                            console.log(index);
                            console.log($scope.PRDetails.PRItemsList);

                        }
                    });
            };

            $scope.GetCompanyRFQCreators = function () {
                var params = {
                    pr_id: $stateParams.Id//,
                   // dept_id: userService.getSelectedUserDepartmentDesignation().deptID
                }
                PRMPRServices.GetCompanyRFQCreators(params)
                    .then(function (response) {
                        $scope.CompanyRFQCreators = response;

                        $scope.CompanyRFQCreators = $scope.CompanyRFQCreators.filter(function (item, index) {
                            if (item.DEPT_ID == userService.getSelectedUserDepartmentDesignation().deptID) {
                                return item;
                            } else {

                            }
                        })

                        if ($scope.CompanyRFQCreators && $scope.CompanyRFQCreators.length > 0)
                        {
                            $scope.CompanyRFQCreators.forEach(function (item, index) {
                                if (item.IS_ASSIGNED > 0) {
                                    item.IS_ASSIGNED = true;
                                }
                                else {
                                    item.IS_ASSIGNED = false;
                                }
                            })
                        }

                    })
            };

            $scope.GetCompanyRFQCreators();

            //if ($rootScope.isUserEntitled(591168))
            //{
            //    $scope.GetCompanyRFQCreators();
            //}

            
            $scope.SaveCompanyRFQCreators = function () {
                var params = {
                    listPRRFQCreator: $scope.CompanyRFQCreators,
                    sessionid: userService.getUserToken()
                }

                if (params.listPRRFQCreator && params.listPRRFQCreator.length > 0) {
                    params.listPRRFQCreator.forEach(function (item, index) {
                        item.PR_ID = $stateParams.Id;
                        if (item.IS_ASSIGNED) {
                            item.IS_ASSIGNED = 1;
                        }
                        else {
                            item.IS_ASSIGNED = 0;
                        }
                    })
                }

                PRMPRServices.SaveCompanyRFQCreators(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            growlService.growl("Saved Successfully.", "success");
                            $scope.GetCompanyRFQCreators();
                        } else {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                    })
            };



            $scope.display = function (type) {
                if (type == 'PR') {
                    $scope.PRDetails.PRSHOW = true;
                    $scope.PRDetails.PRIORITY = '';
                    $scope.PRDetails.COMPANY = '';
                  //  $scope.PRDetails.REQUEST_DATE = today;
                } else {
                    $scope.PRDetails.PRSHOW = false;
                    $scope.PRDetails.PR_NUMBER = '';
                    $scope.GetSeries('', userService.getSelectedUserDeptID());
                    $scope.PRDetails.PRItemsList = [];
                    //$scope.PRDetails = {
                    $scope.PRDetailsisTabular = true;
                    $scope.PRDetails.PR_ID = 0,
                        $scope.PRDetails.U_ID = userService.getUserId();
                    $scope.PRDetails.COMP_ID = 0;
                    $scope.PRDetails.PR_NUMBER = '';
                    $scope.PRDetails.PR_TYPE = 'REGULAR_PURCHASE';
                    $scope.PRDetails.ASSET_TYPE = '';
                    $scope.PRDetails.PRIORITY = 'Anantapur';
                    $scope.PRDetails.PRIORITY_COMMENTS = '';
                    $scope.PRDetails.COMMENTS = '';
                    $scope.PRDetails.DEPARTMENT = userService.getSelectedUserDeptID();
                 //   $scope.PRDetails.REQUEST_DATE = today;
                    //$scope.PRDetails.REQUIRED_DATE = '';
                    $scope.PRDetails.ATTACHMENTS = '';
                    $scope.PRDetails.PROJECT = '';
                    $scope.PRDetails.USER_LOCATION = '';
                    $scope.PRDetails.TOTAL_BASE_PRICE = 0;
                    $scope.PRDetails.TOTAL_GST_PRICE = 0;
                    $scope.PRDetails.TOTAL_PRICE = 0;
                    $scope.PRDetails.SUB_TOTAL = 0;
                    $scope.PRDetails.GST_PRICE = 0;
                    $scope.PRDetails.WF_ID = 0;
                    $scope.PRDetails.IS_PR_SUBMITTED = 0;
                    //CREATED_BY: 0,
                    //CREATED_DATE: '',
                    //MODIFIED_BY: 0,
                    //MODIFIED_DATE: '',
                    $scope.PRDetails.CLASSIFICATION = '';
                    $scope.PRDetails.COMPANY = 'Greenko';
                    $scope.PRDetails.WORK_ORDER_DURATION = '';
                    $scope.PRDetails.PURPOSE_IN_BRIEF = '';
                    $scope.PRDetails.URGENCY = '';

                    $scope.PRDetails.PRItemsList = [];

                    //};


                    var PRItem = {

                        ITEM_ID: 0,
                        PR_ID: 0,
                        ITEM_NAME: '',
                        HSN_CODE: '',
                        ITEM_CODE: '',
                        ITEM_DESCRIPTION: '',
                        BRAND: '',
                        UNITS: '',
                        EXIST_QUANTITY: 0,
                        REQUIRED_QUANTITY: 0,
                        UNIT_PRICE: 0,
                        C_GST_PERCENTAGE: 0,
                        S_GST_PERCENTAGE: 0,
                        I_GST_PERCENTAGE: 0,
                        TOTAL_PRICE: 0,
                        COMMENTS: '',
                        ATTACHMENTS: '',
                        ATTACHMENTSARR: [],
                        CREATED_BY: 0,
                        //CREATED_DATE: '',
                        MODIFIED_BY: 0,
                        //MODIFIED_DATE: '',
                        CATALOGUE_ID: 0,
                        U_ID: $scope.userID,
                        sessionID: $scope.sessionID,
                        itemAttachment: [],
                        attachmentName: '',
                        ITEM_NUM: '',
                        UOM: '',
                        MATERIAL_DESCRIPTION: ''

                    };
                    $scope.PRDetails.PRItemsList.push(PRItem);
                }
            }

          //  console.log("purchase value is>>>>>>" + $scope.PRDetails.PURCHASE);


            /*region start WORKFLOW*/

            $scope.getWorkflows = function () {
                //createDomestic
                workflowService.getWorkflowList()
                    .then(function (response) {
                        $scope.workflowList = [];
                        $scope.workflowListDeptWise = [];
                        $scope.workflowListTemp = response;
                        $scope.workflowListTemp.forEach(function (item, index) {
                            if (item.WorkflowModule == $scope.WorkflowModule) {
                                $scope.workflowList.push(item);
                                $scope.workflowListDeptWise.push(item);
                            }
                        });

                        if (userService.getUserObj().isSuperUser) {
                            $scope.workflowList = $scope.workflowList;
                        }
                        else {
                            $scope.workflowList = [];
                            $scope.workflowListDeptWise.forEach(function (wf, idx) {
                                $scope.deptIDs.forEach(function (dep) {
                                    if (dep == wf.deptID) {
                                        $scope.workflowList.push(wf);
                                    }
                                });
                            });

                            //$scope.workflowList = $scope.workflowList.filter(function (item) {
                            //    return item.deptID == userService.getSelectedUserDepartmentDesignation().deptID;

                            //});
                        }
                    });
            };

            $scope.getWorkflows();

            $scope.getItemWorkflow = function (wfID,prID,type,itemID) {
                workflowService.getItemWorkflow(wfID, prID, type)
                    .then(function (response) {
                        $scope.itemWorkflow = response;
                        $scope.checkIsFormDisable();
                        if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                            $scope.currentStep = 0;

                            var count = 0;

                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                                if (!track.multipleAttachments) {
                                    track.multipleAttachments = [];
                                }

                                if (track.status === 'APPROVED') { track.statusNew = 'Approved'; }
                                if (track.status === 'HOLD') { track.statusNew = 'Hold'; }
                                if (track.status === 'PENDING') { track.statusNew = 'Pending'; }
                                if (track.status === 'REJECTED') { track.statusNew = 'Rejected'; }

                                if (track.status === 'APPROVED' || track.status === 'HOLD') {
                                    $scope.isFormdisabled = true;
                                }

                                if (track.status === 'APPROVED') {
                                    $scope.isWorkflowCompleted = true;
                                    $scope.orderInfo = track.order;
                                    $scope.assignToShow = track.status;

                                }
                                else {
                                    $scope.isWorkflowCompleted = false;
                                }

                                if (track.status === 'REJECTED' && count == 0) {
                                    count = count + 1;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD') && count == 0) {
                                    count = count + 1;
                                    $scope.IsUserApproverForStage(track.approverID);
                                    $scope.currentAccess = track.order;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD' || track.status === 'REJECTED') && $scope.currentStep === 0) {
                                    $scope.currentStep = track.order;
                                    return false;
                                }
                            });
                        }
                    });
            };

            $scope.updateTrack = function (step, status) {
                $scope.disableAssignPR = true;
                $scope.commentsError = '';
                if (step.comments != null || step.comments != "" || step.comments != undefined) {
                    step.comments = validateStringWithoutSpecialCharacters(step.comments);
                }
                var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
                if (step.order == tempArray.order && status === 'APPROVED') {
                    $scope.disableAssignPR = false;
                } else {
                    $scope.disableAssignPR = true;
                }

                if ($scope.isReject) {
                    $scope.commentsError = 'Please Save Rejected Items/Qty';
                    return false;
                }

                if (status === 'REJECTED' && (step.comments == null || step.comments == "")) {
                    $scope.commentsError = 'Please enter comments';
                    return false;
                }

                step.status = status;
                step.sessionID = $scope.sessionID;
                step.modifiedBy = userService.getUserId();

                step.moduleName = $scope.WorkflowModule;

                //step.subModuleName = $scope.requirementDetails.title;
                //step.subModuleID = $scope.reqId;

                workflowService.SaveWorkflowTrack(step)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            $scope.getItemWorkflow();
                            location.reload();
                            //$state.go('approval-qcs-list');
                        }
                    });
            };

            $scope.assignWorkflow = function (moduleID) {
                workflowService.assignWorkflow(({ wID: $scope.workflowObj.workflowID, moduleID: moduleID, user: userService.getUserId(), sessionID: $scope.sessionID }))
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                            $scope.isSaveDisable = false;
                        }
                        else {
                            //  $state.go('list-pr');
                        }
                    });
            };

            $scope.IsUserApprover = false;

            $scope.functionResponse = false;

            $scope.IsUserApproverForStage = function (approverID) {
                workflowService.IsUserApproverForStage(approverID, userService.getUserId())
                    .then(function (response) {
                        $scope.IsUserApprover = response;
                    });
            };

            $scope.isApproverDisable = function (index) {

                var disable = true;

                var previousStep = {};

                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                    if (index == stepIndex) {
                        if (stepIndex == 0) {
                            if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                        else if (stepIndex > 0) {
                            if (previousStep.status === 'PENDING' || previousStep.status === 'HOLD' || previousStep.status === 'REJECTED') {
                                disable = true;
                            }
                            else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                    }
                    previousStep = step;
                });

                return disable;
            };

        /*region end WORKFLOW*/

            $scope.fillProduct = function (output, index) {
                //$scope.isMedical($scope.PRDetails.PR_TYPE);
                $scope['filterProducts_' + index] = output;
            };

            $scope.isMedical = function (isMedical) {
                if ($scope.productsListTemp && $scope.productsListTemp.length > 0) {
                    if (isMedical) {
                        $scope.PRDetails.PR_TYPE = 1;
                        $scope.productsList = $scope.productsListTemp.filter(function (item) {
                            if (item.fieldTypeMed && +item.fieldTypeMed === 1) {
                                return item;
                            }
                        });

                    } else {
                        $scope.PRDetails.PR_TYPE = 0;
                        $scope.productsList = $scope.productsListTemp.filter(function (item) {
                            if (item.fieldTypeMed && +item.fieldTypeMed === 0) {
                                return item;
                            }
                        });
                    }
                }
            };

            $scope.dateFormat = function (date) {
                return userService.toLocalDate(date);
            };


            $scope.isUserBelongsToDeptandDesig = function (deptID, desigID) {
                var isEligible = true;

                if ($scope.deptIDs.indexOf(deptID) != -1 && $scope.desigIDs.indexOf(desigID) != -1) {
                    isEligible = true;
                } else {
                    isEligible = false;
                }

                return isEligible;
            };

            function validateStringWithoutSpecialCharacters(string) {
                if (string) {
                    string = string.replace(/\'/gi, "");
                    string = string.replace(/\"/gi, "");
                    string = string.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                    string = string.replace(/(\r\n|\n|\r)/gm, "");
                    string = string.replace(/\t/g, '');
                    return string;
                }
            }

            $scope.searchTableLocation = function (str, locations) {
                str1 = str.toUpperCase();
                if ($scope.companyLocationSearch.length == 0) {
                    $scope.LocationError = "No Locations available";
                } else {
                    $scope.PRDetails.companyLocation = $scope.companyLocationSearch.filter(function (req) {
                        if (String(req.configText).toUpperCase().includes(str1) == true) {
                            $scope.LocationError = '';
                            return (String(req.configText).toUpperCase().includes(str1) == true);
                        } else {
                            $scope.LocationError = 'Please select locations from dropdown only';
                        }

                    });
                }


            }

            // (locationObj,location,isChecked)
            $scope.PRDetails.SelectedLocationArray = [];
            $scope.PRDetails.selectedLocations = '';
            $scope.fillValue = function (locationObj, location, isChecked) {
                $scope.locationArray = [];
                if (isChecked) {
                    $scope.PRDetails.SelectedLocationArray.push(location);

                    locationObj.forEach(function (obj, objIdx) {
                        if (obj.isChecked == true) {
                            $scope.locationArray.push(obj.configValue);
                        }
                    })
                    $scope.PRDetails.selectedLocations = $scope.locationArray.join(',');
                    if ($scope.PRDetails.selectedLocations.length > 0) {
                        $scope.PRDetails.selectedLocationsError = '';
                        $scope.searchKeyword = '';
                    }

                } else {
                    var tempindex = $scope.PRDetails.SelectedLocationArray.indexOf(location);
                    if (tempindex > -1) {
                        $scope.PRDetails.SelectedLocationArray.splice(tempindex, 1);
                        $scope.PRDetails.SelectedLocationArray.forEach(function (obj, objIdx) {
                            if (obj.isChecked == true) {
                                $scope.locationArray.push(obj.configValue);
                            }
                        })
                        $scope.PRDetails.selectedLocations = $scope.locationArray.join(',');
                    }
                }
            }

        }]);
