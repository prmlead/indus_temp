﻿using PRM.Core.Common;
using PRMServices.models;
using PRMServices.Models;
using PRMServices.SQLHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ServiceModel.Activation;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]

    public class PRMCustomFieldService : IPRMCustomFieldService
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        private PRMServices prmServices = new PRMServices();

        #region GetFunctions
        public List<PRMCustomField> GetCustomFields(int compid, int customfieldid, string fieldname, string fieldmodule, string sessionid)
        {
            List<PRMCustomField> details = new List<PRMCustomField>();
            try
            {
                fieldname = fieldname ?? string.Empty;
                fieldmodule = fieldmodule ?? string.Empty;

                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_CUST_FIELD_ID", customfieldid);
                sd.Add("P_FIELD_NAME", fieldname);
                sd.Add("P_FIELD_MODULE", fieldmodule);
                DataSet dataset = sqlHelper.SelectList("cf_GetCustomFields", sd);
                DataNamesMapper<PRMCustomField> mapper = new DataNamesMapper<PRMCustomField>();
                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                {
                    details = mapper.Map(dataset.Tables[0]).ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            return details;
        }

        public List<PRMCustomField> GetCustomFieldsByModuleId(int compid, string fieldmodule, int moduleid, string sessionid)
        {
            List<PRMCustomField> details = new List<PRMCustomField>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_FIELD_MODULE", fieldmodule);
                sd.Add("P_MODULE_ID", moduleid);
                DataSet dataset = sqlHelper.SelectList("cf_GetModuleFields", sd);
                DataNamesMapper<PRMCustomField> mapper = new DataNamesMapper<PRMCustomField>();
                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                {
                    details = mapper.Map(dataset.Tables[0]).ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            return details;
        }

        //public List<PRMFieldMappingDetails> GetPRMFieldMappingDetails(int compid, string templatename, string sessionid)
        //{
        //    List<PRMFieldMappingDetails> details = new List<PRMFieldMappingDetails>();
        //    try
        //    {
        //        Utilities.ValidateSession(sessionid);
        //        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
        //        sd.Add("P_COMP_ID", compid);
        //        sd.Add("P_TEMPLATE_NAME", templatename);
        //        DataSet dataset = sqlHelper.SelectList("cf_GetPRMFieldMappingDetails", sd);
        //        DataNamesMapper<PRMFieldMappingDetails> mapper = new DataNamesMapper<PRMFieldMappingDetails>();
        //        if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
        //        {
        //            details = mapper.Map(dataset.Tables[0]).ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex, "Error in retrieving alias column details");
        //    }

        //    return details;
        //}

        public string[] GetPRMFieldMappingTemplates(int compid, string sessionid)
        {
            List<string> templates = new List<string>();
            try
            {
                Utilities.ValidateSession(sessionid);
                string query = string.Format("select distinct TEMPLATE_NAME From PRMFieldMappingDetails WHERE COMP_ID = {0};", compid);
                DataTable dt = sqlHelper.SelectQuery(query);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        string template = row["TEMPLATE_NAME"] != DBNull.Value ? Convert.ToString(row["TEMPLATE_NAME"]) : string.Empty;
                        if (!string.IsNullOrEmpty(template))
                        {
                            templates.Add(template);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                logger.Error(ex, "Error in retrieving templates");
            }

            return templates.ToArray();
        }

        public PRMTemplate[] GetTemplates(int compid, string sessionid)
        {
            List<PRMTemplate> templates = new List<PRMTemplate>();
            try
            {
                Utilities.ValidateSession(sessionid);
                string query = $"SELECT * FROM PRMTemplates WHERE COMP_ID = {compid}";
                DataTable dt = sqlHelper.SelectQuery(query);
                if (dt != null && dt.Rows.Count > 0)
                {
                    DataNamesMapper<PRMTemplate> mapper = new DataNamesMapper<PRMTemplate>();
                    templates = mapper.Map(dt).ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving templates");
            }

            return templates.ToArray();
        }

        public PRMTemplateFields[] GetTemplateFields(int templateid, string templatename, string sessionid)
        {
            List<PRMTemplateFields> templates = GetTemplateFieldsTemp(templateid, templatename);

            return templates.ToArray();
        }

        #endregion

        #region SaveFunctions
        public Response SaveTemplate(PRMTemplate template)
        {
            Response response = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_TEMPLATE_ID", template.TEMPLATE_ID);
                sd.Add("P_COMP_ID", template.COMP_ID);
                sd.Add("P_TEMPLATE_NAME", template.TEMPLATE_NAME);
                sd.Add("P_TEMPLATE_DESC", template.TEMPLATE_DESC);
                sd.Add("P_USER", template.ModifiedBy);
                sd.Add("P_IS_DEFAULT", template.IS_DEFAULT == true ? 1 : 0); 
                DataSet ds = sqlHelper.SelectList("cf_SaveTemplate", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in saving.");
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveTemplateFields(PRMTemplateFields[] fields)
        {
            Response response = new Response();
            try
            {
                foreach (var field in fields)
                {
                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_TEMPLATE_FIELD_ID", field.TEMPLATE_FIELD_ID);
                    sd.Add("P_TEMPLATE_ID", field.TEMPLATE_ID);
                    sd.Add("P_FIELD_NAME", field.FIELD_NAME.ToUpper());
                    sd.Add("P_FIELD_LABEL", field.FIELD_LABEL);
                    sd.Add("P_FIELD_PLACEHOLDER", field.FIELD_PLACEHOLDER);
                    sd.Add("P_FIELD_DATA_TYPE", field.FIELD_DATA_TYPE);
                    sd.Add("P_FIELD_DEFAULT_VALUE", field.FIELD_DEFAULT_VALUE);
                    sd.Add("P_FIELD_OPTIONS", field.FIELD_OPTIONS);
                    sd.Add("P_FIELD_READ_ONLY", field.FIELD_READ_ONLY);
                    sd.Add("P_FIELD_IS_VENDOR", field.FIELD_IS_VENDOR);
                    sd.Add("P_FIELD_IS_CUSTOMER", field.FIELD_IS_CUSTOMER);
                    sd.Add("P_USER", field.ModifiedBy);
                    DataSet ds = sqlHelper.SelectList("cf_SaveTemplateField", sd);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                    {
                        response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    }
                }

                if (fields != null && fields.Length > 0)
                {
                    response.ObjectID = fields.Length;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in saving.");
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        //public Response SaveTableColumnAlias(PRMColumnAliasField details, string sessionid)
        //{
        //    Response response = new Response();

        //    try
        //    {
        //        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
        //        sd.Add("P_COLUMN_ALIAS_ID", details.COLUMN_ALIAS_ID);
        //        sd.Add("P_COMP_ID", details.COMP_ID);
        //        sd.Add("P_TABLE_NAME", details.TABLE_NAME);
        //        sd.Add("P_COLUMN_NAME", details.COLUMN_NAME);
        //        sd.Add("P_COLUMN_ALIAS_NAME", details.COLUMN_ALIAS_NAME);
        //        sd.Add("P_MODULE_NAME", details.MODULE_NAME);
        //        sd.Add("P_USER", details.ModifiedBy);
        //        DataSet ds = sqlHelper.SelectList("cf_SaveTableColumnAliasName", sd);
        //        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
        //        {
        //            response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex, "Error in saving custom field details");
        //        response.ErrorMessage = ex.Message;
        //    }

        //    return response;
        //}

        public Response SaveCustomField(PRMCustomField details, string sessionid)
        {
            Response response = new Response();

            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_CUST_FIELD_ID", details.CUST_FIELD_ID);
                sd.Add("P_COMP_ID", details.COMP_ID);
                sd.Add("P_FIELD_NAME", details.FIELD_NAME);
                sd.Add("P_FIELD_HELP_TEXT", details.FIELD_HELP_TEXT);    
                sd.Add("P_FIELD_TYPE", details.FIELD_TYPE);
                sd.Add("P_FIELD_MODULE", details.FIELD_MODULE);
                sd.Add("P_IS_REQUIRED", details.IS_REQUIRED);
                sd.Add("P_FIELD_VALUE", details.FIELD_VALUE);
                sd.Add("P_FIELD_DEFAULT_VALUE", details.FIELD_DEFAULT_VALUE);
                sd.Add("P_IS_READ_ONLY", details.IS_READ_ONLY);
                sd.Add("P_VISIBLE_TO_VENDOR", details.VISIBLE_TO_VENDOR);
                sd.Add("P_USER", details.ModifiedBy);
                DataSet ds = sqlHelper.SelectList("cf_SaveCustomField", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in saving custom field details");
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveCustomFieldValue(PRMCustomField[] details, string sessionid)
        {
            Response response = new Response();

            try
            {
                if(details!=null && details.Count() > 0)
                {
                    foreach(var detail in details)
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_CUST_FIELD_ID", detail.CUST_FIELD_ID);
                        sd.Add("P_FIELD_MODULE", detail.FIELD_MODULE);
                        sd.Add("P_MODULE_ID", detail.MODULE_ID);
                        sd.Add("P_FIELD_VALUE", detail.FIELD_VALUE);
                        sd.Add("P_USER", detail.ModifiedBy);
                        DataSet ds = sqlHelper.SelectList("cf_SaveCustomFieldValue", sd);
                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                        {
                            response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in saving custom field details");
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        #endregion

        public List<PRMTemplateFields> GetTemplateFieldsTemp(int templateid, string templatename)
        {
            List<PRMTemplateFields> templates = new List<PRMTemplateFields>();
            try
            {
                string query = string.Empty;
                if (templateid > 0)
                {
                    query = $"SELECT * FROM PRMTemplateFields WHERE TEMPLATE_ID = {templateid}";
                }
                else if (!string.IsNullOrEmpty(templatename))
                {
                    query = $"SELECT * FROM PRMTemplateFields WHERE TEMPLATE_ID IN (SELECT TEMPLATE_ID FROM PRMTemplates WHERE TEMPLATE_NAME = '{templatename}')";
                }

                if (!string.IsNullOrEmpty(query))
                {
                    DataTable dt = sqlHelper.SelectQuery(query);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        DataNamesMapper<PRMTemplateFields> mapper = new DataNamesMapper<PRMTemplateFields>();
                        templates = mapper.Map(dt).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving templates");
            }

            return templates;
        }

        public Response SaveTemplateSubItem(SubItemTemplate subitemtemplate, string sessionid)
        {
            Response details = new Response();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_SUB_ITEM_ID", subitemtemplate.SUB_ITEM_ID);
                sd.Add("P_TEMPLATE_ID", subitemtemplate.TEMPLATE_ID);
                sd.Add("P_NAME", subitemtemplate.NAME);
                sd.Add("P_DESCRIPTION", subitemtemplate.DESCRIPTION);
                sd.Add("P_HAS_SPECIFICATION", subitemtemplate.HAS_SPECIFICATION);
                sd.Add("P_HAS_PRICE", subitemtemplate.HAS_PRICE);
                sd.Add("P_HAS_QUANTITY", subitemtemplate.HAS_QUANTITY);
                sd.Add("P_CONSUMPTION", subitemtemplate.CONSUMPTION);
                sd.Add("P_UOM", subitemtemplate.UOM);
                sd.Add("P_HAS_TAX", subitemtemplate.HAS_TAX);
                sd.Add("P_IS_VALID", subitemtemplate.IS_VALID);
                sd.Add("P_U_ID", subitemtemplate.U_ID);
                DataNamesMapper<Response> mapper = new DataNamesMapper<Response>();
                var dataset = sqlHelper.SelectList("cm_SaveTemplateSubItem", sd);
                details = mapper.Map(dataset.Tables[0]).FirstOrDefault();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<SubItemTemplate> GetTemplateSubItem(int templateid, string sessionid)
        {
            List<SubItemTemplate> details = new List<SubItemTemplate>();
            try
            {
                Utilities.ValidateSession(sessionid);
                DataNamesMapper<SubItemTemplate> mapper = new DataNamesMapper<SubItemTemplate>();
                var dataset = sqlHelper.ExecuteQuery($"SELECT * FROM PRMTemplateSubItems WHERE IS_VALID = 1 AND TEMPLATE_ID = {templateid}");
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }
    }
}