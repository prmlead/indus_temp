﻿prmApp
    .controller('saveCustomFieldCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMCustomFieldService",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMCustomFieldService) {

            $scope.customFieldId = $stateParams.Id;
            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.sessionID = userService.getUserToken();
            $scope.customFieldObj = {};

            $scope.fielTypeList = [
                {
                    FIELD_NAME: 'TEXT',
                    FIELD_TYPE: 'TEXT'
                },
                {
                    FIELD_NAME: 'Date Time',
                    FIELD_TYPE: 'DATETIME'
                },
                {
                    FIELD_NAME: 'NUMBER',
                    FIELD_TYPE: 'NUMBER'
                },
                {
                    FIELD_NAME: 'Large Text',
                    FIELD_TYPE: 'LARGETEXT'
                }
            ];

            $scope.moduleList = [
                {
                    FIELD_NAME: 'Select Module',
                    FIELD_TYPE: ''
                },
                {
                    FIELD_NAME: 'REQUIREMENT',
                    FIELD_TYPE: 'REQUIREMENT'
                }
            ];

            $scope.getCustomFieldList = function () {
                if ($scope.customFieldId > 0) {
                    var params = {
                        "compid": userService.getUserCompanyId(),
                        "customfieldid": $scope.customFieldId,
                        "sessionid": userService.getUserToken()
                    };

                    PRMCustomFieldService.getCustomFieldList(params)
                        .then(function (response) {
                            if (response && response.length > 0) {
                                $scope.customFieldObj = response[0];
                            }
                        });
                }
            };

            $scope.getCustomFieldList();

            $scope.saveField = function () {
                $scope.customFieldObj.ModifiedBy = $scope.userID;
                var params = {
                    "details": $scope.customFieldObj,
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.saveCustomField(params)
                    .then(function (response) {
                        if (response.errorMessage !== '' || response.objectID <= 0) {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            $scope.customFieldId = response.objectID;
                            //$scope.resetFieldObj();
                            growlService.growl("Saved Successfully.", "success");
                        }
                    });
            };

            $scope.resetFieldObj = function () {
                $scope.customFieldObj = {
                    CUST_FIELD_ID: 0,
                    COMP_ID: userService.getUserCompanyId(),
                    FIELD_NAME: '',
                    FIELD_HELP_TEXT: '',
                    FIELD_TYPE: 'TEXT',
                    FIELD_MODULE: '',
                    IS_REQUIRED: 0,
                    FIELD_VALUE: '',
                    FIELD_DEFAULT_VALUE: '',
                    IS_READ_ONLY: 0,
                    VISIBLE_TO_VENDOR: 1
                };
            };

            $scope.resetFieldObj();
        }]);