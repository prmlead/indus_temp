﻿prmApp

    .controller('vendorCompaniesCtrl', ["$scope", "$http", "$state", "domain", "$filter", "$log", "$stateParams", "$timeout", "auctionsService", "fwdauctionsService", "userService", "SignalRFactory", "fileReader", "growlService",
        function ($scope, $http, $state, domain, $filter, $log, $stateParams, $timeout, auctionsService, fwdauctionsService, userService, SignalRFactory, fileReader, growlService) {

            $scope.vendorCompanies = [];
            if ($stateParams.companyNames) {
                $scope.vendorCompanies = $stateParams.companyNames.split(",");
            }
    }]);