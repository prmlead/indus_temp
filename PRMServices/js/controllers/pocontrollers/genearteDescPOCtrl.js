prmApp
    .controller('genearteDescPOCtrl', ["$scope", "$state", "$stateParams", "userService", "auctionsService", "fileReader", "poService",
        function ($scope, $state, $stateParams, userService, auctionsService, fileReader, poService) {
            $scope.reqID = $stateParams.reqID;

            $scope.descPo = {};
            $scope.vendors = [];

            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;

            $scope.getDesPoInfo = function () {
                poService.getdespoinfo($stateParams.reqID)
                    .then(function (response) {
                        $scope.descPo = response;

                        //$scope.descPo.expectedDeliveryDate = new moment($scope.descPo.expectedDeliveryDate).format("DD-MM-YYYY");
                    });
            }

            $scope.getDesPoInfo();



            $scope.GetVendors = function () {
                poService.GetVendors($stateParams.reqID)
                    .then(function (response) {
                        $scope.vendors = response;
                    });
            }

            $scope.GetVendors();





            $scope.saveDescPoInfo = function () {

                $scope.descPo.sessionID = userService.getUserToken();
                $scope.descPo.createdBy = userService.getUserId();
                $scope.descPo.status = 'PO_GENERATED';

                var ts = moment($scope.descPo.expectedDelivery, "DD-MM-yyyy HH:mm").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                $scope.descPo.expectedDelivery = "/Date(" + milliseconds + "000+0530)/";

                var params = {
                    'povendor': $scope.descPo
                }
                poService.saveDescPoInfo(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            swal({
                                title: "Done!",
                                text: "Purchase Orders Generated Successfully.",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    //location.reload();s
                                    $state.go('view-requirement', { Id: $stateParams.reqID })
                                });
                        }
                    });
            }


            $scope.getFile1 = function (id, doctype, ext) {
                $scope.progress = 0;
                $scope.file = $("#" + id)[0].files[0];
                $scope.docType = doctype + "." + ext;
                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        if (id == "poFile") {
                            $scope.descPo.poFile = { "fileName": '', 'fileStream': null };
                            var bytearray = new Uint8Array(result);
                            $scope.descPo.poFile.fileStream = $.makeArray(bytearray);
                            $scope.descPo.poFile.fileName = $scope.file.name;
                        }

                    });
            };




            $scope.UpdatePOStatus = function (status) {

                var params = {
                    'poID': $scope.descPo.poID,
                    'status': status,
                    'sessionID': userService.getUserToken()
                }

                swal({
                    title: "Are You Sure?",
                    text: "Send PO to Vendors.",
                    type: "success",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    closeOnConfirm: true
                },
                    function () {
                        poService.UpdatePOStatus(params)
                            .then(function (response) {
                                if (response.errorMessage != '') {
                                    swal({
                                        title: "Done!",
                                        text: "Purchase Orders Sent Successfully.",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },
                                        function () {
                                            //$state.go('view-requirement');
                                            $state.go('view-requirement', { 'Id': response.requirementID });
                                        });
                                }
                            });
                    });


            }

            $scope.goToDescDispatchChallan = function () {
                var url = $state.href("desc-dispatch-challan", { "reqID": $scope.descPo.reqID, "poID": $scope.descPo.poID });
                window.open(url, '_blank');
            }

        }]);