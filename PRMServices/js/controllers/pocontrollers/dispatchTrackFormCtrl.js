prmApp
    .controller('dispatchTrackFormCtrl', ["$scope", "$state", "$window", "$stateParams", "userService", "auctionsService", "fileReader", "poService","PRMPOService",
        function ($scope, $state, $window, $stateParams, userService, auctionsService, fileReader, poService, PRMPOService) {
            $scope.reqID = $stateParams.reqID;
            $scope.poOrderId = $stateParams.poOrderId;
            $scope.dCode = $stateParams.dCode;
            $scope.vendorCode = $stateParams.vendorCode;
            $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
            $scope.sessionID = userService.getUserToken();
            $scope.companyId = userService.getUserCompanyId();
            $scope.currentUserId = userService.getUserId();
            $scope.dispatchTrackObject = {};

            $scope.vendors = [];

            $scope.dtobject = [];

            $scope.dispatchFile = 0;
            $scope.recivedFile = 0;

            $scope.asnForm = false;
            $scope.serviceForm = false;
         //   $scope.customerBatch = 'ASN';
            //$scope.dispatchObject.CUSTOMER_BATCH = '';

            $scope.getCustomerBatch = function (type) {
                if (type == 'ASN') {
                    $scope.asnForm = true
                    $scope.serviceForm = false

                } else {
                    $scope.serviceForm = true
                    $scope.asnForm = false

                }
                if (!$scope.isCustomer) {
                    var params = {
                        "compid": $scope.isCustomer ? $scope.companyId : 0,
                        "uid": $scope.isCustomer ? 0 : $scope.currentUserId,
                        "search": $scope.poOrderId,
                        "categoryid": '',
                        "productid": '',
                        "supplier": '',
                        "postatus": '',
                        "deliverystatus": '',
                        "plant": '',
                        "fromdate": '1970-01-01',
                        "todate": '2100-01-01',
                        "page": 0,
                        "pagesize": 10,
                        "ackStatus": '',
                        "buyer": '',
                        "purchaseGroup": '',
                        "sessionid": userService.getUserToken()

                    };

                    PRMPOService.getPOScheduleList(params)
                        .then(function (response) {
                            $scope.dispatchObject.SHIP_TO_LOCATION = response[0].PLANT_LOCATION
                            $scope.dispatchObject.SHIP_FROM_LOCATION = response[0].ADDRESS

                        });


                    var params1 = {
                        "ponumber": $scope.poOrderId,
                        "moredetails": 0
                    };
                    PRMPOService.getPOScheduleItems(params1)
                        .then(function (response) {
                            $scope.pendingPOItems = response;
                            
                            $scope.dispatchObject.poItemsEntity = $scope.pendingPOItems;
                        });
                }
            }
           
            $scope.getDispatchTrack = function () {
                //poService.getDispatchTrack($scope.poOrderId, $scope.dCode)
                //    .then(function (response) {
                //        $scope.dtobject = response;
                //        $scope.dispatchTrackObject = response[0];
                //        $scope.dispatchTrackObject.shipToLocation = "Delhi";
                //        $scope.purchaseID = $scope.dispatchTrackObject.poItemsEntity[0].purchaseID;
                //        $scope.indentID = $scope.dispatchTrackObject.poItemsEntity[0].indentID;

                //        $scope.dtobject.forEach(function (item, index) {
                //            if (item.dispatchLink > 0) {
                //                $scope.dispatchFile = item.dispatchLink;
                //            }
                //            if (item.dispatchLink > 0) {
                //                $scope.recivedFile = item.recivedLink;
                //            }
                //        });

                //        $scope.dispatchTrackObject.dispatchDate = new moment($scope.dispatchTrackObject.dispatchDate).format("DD-MM-YYYY");
                //        if ($scope.dispatchTrackObject.dispatchDate.indexOf('-9999') > -1) {
                //            $scope.dispatchTrackObject.dispatchDate = "";
                //        }

                //        $scope.dispatchTrackObject.shipmentDate = new moment($scope.dispatchTrackObject.shipmentDate).format("DD-MM-YYYY");
                //        if ($scope.dispatchTrackObject.shipmentDate.indexOf('-9999') > -1) {
                //            $scope.dispatchTrackObject.shipmentDate = "";
                //        }

                //        $scope.dispatchTrackObject.documentDate = new moment($scope.dispatchTrackObject.documentDate).format("DD-MM-YYYY");
                //        if ($scope.dispatchTrackObject.documentDate.indexOf('-9999') > -1) {
                //            $scope.dispatchTrackObject.documentDate = "";
                //        }

                //        $scope.dispatchTrackObject.recivedDate = new moment($scope.dispatchTrackObject.recivedDate).format("DD-MM-YYYY");
                //        if ($scope.dispatchTrackObject.recivedDate.indexOf('-9999') > -1) {
                //            $scope.dispatchTrackObject.recivedDate = "";
                //        }

                //        $scope.dispatchTrackObject.manufacDate = new moment($scope.dispatchTrackObject.manufacDate).format("DD-MM-YYYY");
                //        if ($scope.dispatchTrackObject.manufacDate.indexOf('-9999') > -1) {
                //            $scope.dispatchTrackObject.manufacDate = "";
                //        }

                //        $scope.dispatchTrackObject.bestBeforeDate = new moment($scope.dispatchTrackObject.bestBeforeDate).format("DD-MM-YYYY");
                //        if ($scope.dispatchTrackObject.bestBeforeDate.indexOf('-9999') > -1) {
                //            $scope.dispatchTrackObject.bestBeforeDate = "";
                //        }


                //    });
                $scope.params = {
                    "compid": $scope.isCustomer ? $scope.companyId : 0,
                    "asnid": 0,
                    "ponumber": $scope.poOrderId,
                    "grncode": 0,
                    "asncode": $scope.dCode,
                    "vendorid": $scope.isCustomer ? 0 : $scope.currentUserId,
                    "senssionid": userService.getUserToken()
                };

                PRMPOService.getASNDetails($scope.params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.dispatchObject = response[0];
                            $scope.dispatchObject.DELIVERY_DATE = userService.toLocalDateOnly($scope.dispatchObject.DELIVERY_DATE);
                            $scope.dispatchObject.SHIPMENT_DATE = userService.toLocalDateOnly($scope.dispatchObject.SHIPMENT_DATE);
                            $scope.dispatchObject.DOCUMENT_DATE = userService.toLocalDateOnly($scope.dispatchObject.DOCUMENT_DATE);
                            $scope.dispatchObject.MANUFACTURED_DATE = userService.toLocalDateOnly($scope.dispatchObject.MANUFACTURED_DATE);
                            $scope.dispatchObject.BEST_BEFORE_DATE = userService.toLocalDateOnly($scope.dispatchObject.BEST_BEFORE_DATE);
                            $scope.dispatchObject.RECEIVED_DATE = userService.toLocalDateOnly($scope.dispatchObject.RECEIVED_DATE);

                            $scope.getCustomerBatch($scope.dispatchObject.CUSTOMER_BATCH);

                        }
                    });
            };

           

            $scope.getDispatchTrack();

            $scope.formatDate = function (date) {
                return moment(date).format('MM/DD/YYYY');
            };

            $scope.SaveDispatchTrack = function (type) {

                //$scope.dispatchTrackObject.dTID = $scope.dTID;
                //$scope.dispatchTrackObject.poOrderId = $scope.purchaseID;

                //$scope.dispatchTrackObject.sessionID = userService.getUserToken();

                //var ts = moment($scope.dispatchTrackObject.dispatchDate, "DD-MM-YYYY").valueOf();
                //var m = moment(ts);
                //var deliveryDate = new Date(m);
                //var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                //$scope.dispatchTrackObject.dispatchDate = "/Date(" + milliseconds + "000+0530)/";

                //var ts = moment($scope.dispatchTrackObject.shipmentDate, "DD-MM-YYYY").valueOf();
                //var m = moment(ts);
                //var deliveryDate = new Date(m);
                //var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                //$scope.dispatchTrackObject.shipmentDate = "/Date(" + milliseconds + "000+0530)/";

                //var ts = moment($scope.dispatchTrackObject.documentDate, "DD-MM-YYYY").valueOf();
                //var m = moment(ts);
                //var deliveryDate = new Date(m);
                //var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                //$scope.dispatchTrackObject.documentDate = "/Date(" + milliseconds + "000+0530)/";

                //var ts = moment($scope.dispatchTrackObject.manufacDate, "DD-MM-YYYY").valueOf();
                //var m = moment(ts);
                //var deliveryDate = new Date(m);
                //var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                //$scope.dispatchTrackObject.manufacDate = "/Date(" + milliseconds + "000+0530)/";

                //var ts = moment($scope.dispatchTrackObject.bestBeforeDate, "DD-MM-YYYY").valueOf();
                //var m = moment(ts);
                //var deliveryDate = new Date(m);
                //var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                //$scope.dispatchTrackObject.bestBeforeDate = "/Date(" + milliseconds + "000+0530)/";

                //var ts = moment($scope.dispatchTrackObject.recivedDate, "DD-MM-YYYY").valueOf();
                //var m = moment(ts);
                //var deliveryDate = new Date(m);
                //var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                //$scope.dispatchTrackObject.recivedDate = "/Date(" + milliseconds + "000+0530)/";

                //if (type == 'DISPATCH') {
                //    $scope.dispatchTrackObject.recivedDate = null;
                //}

                $scope.dispatchObject.sessionID = userService.getUserToken();
                $scope.dispatchObject.COMP_ID = $scope.companyId;
                $scope.dispatchObject.PO_NUMBER = $scope.poOrderId;
                $scope.dispatchObject.VENDOR_ID = $scope.currentUserId;

                var ts = moment($scope.dispatchObject.DELIVERY_DATE, "DD-MM-YYYY").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                $scope.dispatchObject.DELIVERY_DATE = "/Date(" + milliseconds + "000+0530)/";

                var ts = moment($scope.dispatchObject.SHIPMENT_DATE, "DD-MM-YYYY").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                $scope.dispatchObject.SHIPMENT_DATE = "/Date(" + milliseconds + "000+0530)/";

                if ($scope.isCustomer) {

                    var ts = moment($scope.dispatchObject.DOCUMENT_DATE, "DD-MM-YYYY").valueOf();
                    var m = moment(ts);
                    var deliveryDate = new Date(m);
                    var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                    $scope.dispatchObject.DOCUMENT_DATE = "/Date(" + milliseconds + "000+0530)/";

                    var ts = moment($scope.dispatchObject.MANUFACTURED_DATE, "DD-MM-YYYY").valueOf();
                    var m = moment(ts);
                    var deliveryDate = new Date(m);
                    var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                    $scope.dispatchObject.MANUFACTURED_DATE = "/Date(" + milliseconds + "000+0530)/";

                    var ts = moment($scope.dispatchObject.BEST_BEFORE_DATE, "DD-MM-YYYY").valueOf();
                    var m = moment(ts);
                    var deliveryDate = new Date(m);
                    var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                    $scope.dispatchObject.BEST_BEFORE_DATE = "/Date(" + milliseconds + "000+0530)/";


                    var ts = moment($scope.dispatchObject.RECEIVED_DATE, "DD-MM-YYYY").valueOf();
                    var m = moment(ts);
                    var deliveryDate = new Date(m);
                    var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                    $scope.dispatchObject.RECEIVED_DATE = "/Date(" + milliseconds + "000+0530)/";

                    var ts = moment($scope.dispatchObject.SERVICE_COMPLETED_DATE, "DD-MM-YYYY").valueOf();
                    var m = moment(ts);
                    var deliveryDate = new Date(m);
                    var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                    $scope.dispatchObject.SERVICE_COMPLETED_DATE = "/Date(" + milliseconds + "000+0530)/";

                    var ts = moment($scope.dispatchObject.SERVICE_COMPLETION_DATE, "DD-MM-YYYY").valueOf();
                    var m = moment(ts);
                    var deliveryDate = new Date(m);
                    var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                    $scope.dispatchObject.SERVICE_COMPLETION_DATE = "/Date(" + milliseconds + "000+0530)/";

                    var ts = moment($scope.dispatchObject.SERVICE_DOCUMENT_DATE, "DD-MM-YYYY").valueOf();
                    var m = moment(ts);
                    var deliveryDate = new Date(m);
                    var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                    $scope.dispatchObject.SERVICE_DOCUMENT_DATE = "/Date(" + milliseconds + "000+0530)/";
                   
                    $scope.dispatchObject.VENDOR_CODE = $scope.vendorCode;

                } else {
                    var ts = moment($scope.dispatchObject.REQUESTED_DELIVERY_DATE, "DD-MM-YYYY").valueOf();
                    var m = moment(ts);
                    var deliveryDate = new Date(m);
                    var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                    $scope.dispatchObject.REQUESTED_DELIVERY_DATE = "/Date(" + milliseconds + "000+0530)/";

                    var ts = moment($scope.dispatchObject.TRACKING_DATE, "DD-MM-YYYY").valueOf();
                    var m = moment(ts);
                    var deliveryDate = new Date(m);
                    var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                    $scope.dispatchObject.TRACKING_DATE = "/Date(" + milliseconds + "000+0530)/";
                }
                
                var params = {
                    details: $scope.dispatchObject
                };
                
                PRMPOService.saveASNdetails(params)
                         .then(function (response) {
                        if (response.errorMessage == '') {
                            swal({
                                title: "Done!",
                                text: "Saved Successfully.",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    location.reload();
                                });
                        }
                    });

                //poService.SaveDispatchTrack(params)
                //    .then(function (response) {
                //        if (response.errorMessage == '') {
                //            swal({
                //                title: "Done!",
                //                text: "Saved Successfully.",
                //                type: "success",
                //                showCancelButton: false,
                //                confirmButtonColor: "#DD6B55",
                //                confirmButtonText: "Ok",
                //                closeOnConfirm: true
                //            },
                //                function () {
                //                    //location.reload();
                //                    $state.go('dispatchtrack', { reqID: $scope.reqID, poOrderId: $scope.poOrderId, vendorID: 0 });
                //                });
                //        }
                //    });
            };


            //$scope.getFile1 = function (id, doctype, ext) {
            //    $scope.progress = 0;
            //    $scope.file = $("#" + id)[0].files[0];
            //    $scope.docType = doctype + "." + ext;
            //    fileReader.readAsDataUrl($scope.file, $scope)
            //        .then(function (result) {
            //            if (id == "poFile") {
            //                $scope.descPo.poFile = { "fileName": '', 'fileStream': null };
            //                var bytearray = new Uint8Array(result);
            //                $scope.descPo.poFile.fileStream = $.makeArray(bytearray);
            //                $scope.descPo.poFile.fileName = $scope.file.name;
            //            }

            //        });
            //};


            //$scope.dispatchQuantityValidation = function () {
            //    $scope.dispatchTrackObject.poItemsEntity.forEach(function (item, index) {

            //        item.dispatchErrormessage = '';
            //        $scope.dispatchValidation = false;

            //        if ((parseFloat(item.dispatchQuantity) + parseFloat(item.sumRecivedQuantity)) > parseFloat(item.vendorPOQuantity)) {
            //            item.dispatchErrormessage = 'Dispatch Quantity Should be Less than or Equal to Po Quantity';
            //            $scope.dispatchValidation = true;
            //        }
            //    });
            //}


            //$scope.receiveQuantityValidation = function () {
            //    $scope.dispatchTrackObject.poItemsEntity.forEach(function (item, index) {

            //        item.receiveErrormessage = '';
            //        $scope.receivedValidation = false;

            //        temprecivedQuantity = item.recivedQuantity;
            //        tempreturnQuantity = item.returnQuantity;

            //        if (isNaN(item.recivedQuantity) || item.recivedQuantity == undefined || item.recivedQuantity == "") {
            //            item.recivedQuantity = 0;
            //        }

            //        if (isNaN(item.returnQuantity) || item.returnQuantity == undefined || item.returnQuantity == "") {
            //            item.returnQuantity = 0;
            //        }

            //        if ((parseFloat(item.recivedQuantity) + parseFloat(item.returnQuantity)) > parseFloat(item.dispatchQuantity)) {
            //            item.receiveErrormessage = 'Received Quantity + Return Quantity Should be Equal to Dispatch Quantity';
            //            $scope.receivedValidation = true;
            //        }

            //        item.recivedQuantity = temprecivedQuantity;
            //        item.returnQuantity = tempreturnQuantity;
            //    });
            //}

            //$scope.isExists = false;

            //$scope.CheckUniqueIfExists = function (param, idtype) {

            //    $scope.isExists = false;

            //    $scope.params = {
            //        param: param,
            //        idtype: idtype,
            //        sessionID: userService.getUserToken()
            //    }

            //    poService.CheckUniqueIfExists($scope.params)
            //        .then(function (response) {
            //            $scope.isExists = response;
            //        });
            //}


            //$scope.cancelPO = function () {
            //    if ($scope.isCustomer) {
            //        $state.go("po-list", { "reqID": $scope.reqID, "vendorID": $scope.dispatchTrackObject.poItemsEntity[0].vendorID, "poID": 0 });
            //    } else if (!$scope.isCustomer) {
            //        $state.go("po-list", { "reqID": $scope.reqID, "vendorID": userService.getUserId(), "poID": 0 });
            //    }
            //}

          
        }]);