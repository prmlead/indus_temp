﻿prmApp
    .controller('meterialReceivedCtrl', ["$scope", "$state", "$stateParams", "fileReader", "userService", "auctionsService",
        function ($scope, $state, $stateParams, fileReader, userService, auctionsService) {
            $scope.id = $stateParams.Id;
            $scope.formRequest = {};

            //console.log("ABCD===========");

            $scope.getData = function () {
                auctionsService.getrequirementdata({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        $scope.auctionItem = response;
                        $scope.formRequest.selectedVendor = $scope.auctionItem.auctionVendors[0];

                        //console.log("ABCD===========");
                    })
            }

            $scope.getData();

            $scope.materialReceived = {};

            $scope.GetMaterialReceivedData = function () {
                auctionsService.GetMaterialReceivedData({ "reqid": $scope.id, "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.materialReceived = response;
                        $scope.materialReceived.materialReceivedDate = new moment($scope.materialReceived.materialReceivedDate).format("DD-MM-YYYY"); // HH:mm
                    })
            }
            $scope.GetMaterialReceivedData();

            $scope.postRequest = function () {
                var ts = moment($scope.materialReceived.materialReceivedDate, "DD-MM-yyyy HH:mm").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                $scope.materialReceived.materialReceivedDate = "/Date(" + milliseconds + "000+0530)/";


                $scope.materialReceived.sessionID = userService.getUserToken();



                auctionsService.materialReceived($scope.materialReceived)
                    .then(function (response) {
                        if (response.objectID != 0) {

                            $scope.updateStatus('Material Received');

                            swal({
                                title: "Done!",
                                text: "Data Saved Successfully.",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    //$state.go('view-requirement');
                                    $state.go('view-requirement', { 'Id': response.objectID });
                                });
                        }
                    });
            }


            $scope.getFile = function () {
                $scope.progress = 0;

                $scope.file = $("#attachement")[0].files[0];

                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        var bytearray = new Uint8Array(result);
                        $scope.materialreceived.attachment = $.makeArray(bytearray);
                        $scope.materialreceived.attachmentName = $scope.file.name;
                    });
            };

            $scope.updateStatus = function (status) {
                var params = {
                    reqid: $scope.id,
                    userid: userService.getUserId(),
                    status: status,
                    type: "ALLVENDORS",
                    sessionID: userService.getUserToken()
                };
                auctionsService.updateStatus(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    })
            };
        }]);