﻿prmApp
    .controller('storeginCtrl', ["$scope", "$state", "$stateParams", "$window", "$log", "userService", "storeService", "fileReader", "growlService",
        function ($scope, $state, $stateParams, $window, $log, userService, storeService, fileReader, growlService) {
            $scope.storeID = $stateParams.storeID;
            $scope.selectedStoreItems = [];
            $scope.sessionID = userService.getUserToken();
            $scope.companyStoreItems = [];
            $scope.companyStoreItemsTemp = [];
            $scope.storeItemSerialNos = [];
            $scope.storeItemSerialNosSelected = {};
            $scope.currentStoreItemID = 0;

            $scope.phoneError = false;
            $scope.generateGinError = false;
            $scope.issueDateError = false;
            $scope.IssueDateVal = ''

            var GenRandom = {

                Stored: [],

                Job: function () {
                    var newId = Date.now().toString().substr(6); // or use any method that you want to achieve this string

                    if (!this.Check(newId)) {
                        this.Stored.push(newId);
                        return newId;
                    }

                    return this.Job();
                },

                Check: function (id) {
                    for (var i = 0; i < this.Stored.length; i++) {
                        if (this.Stored[i] == id) return true;
                    }
                    return false;
                }

            };

            $scope.ginObject = {
                ginID: 0,
                ginCode: "GIN-" + GenRandom.Job(),
                shipState: "",
                shipCountry: "",
                shipZip: "",
                orderQty: 0,
                shipQty: 0,
                sessionID: $scope.sessionID,
                modifiedBy: userService.getUserId()
            };

            $scope.searchKey = '';

            if ($stateParams.itemsArr) {
                $scope.companyStoreItems = $stateParams.itemsArr;
                if ($scope.companyStoreItems.length > 0) {
                    $scope.ginObject.storeItemDetails = $scope.companyStoreItems;
                }
            }

            $scope.cancelClick = function () {
                $window.history.back();
            }

            $scope.generateGin = function () {

                $scope.generateGinValidations();

                if (!$scope.generateGinError) {

                    if ($scope.companyStoreItems.length > 0) {


                        var savedItems = 0;
                        $.each($scope.ginObject.storeItemDetails, function (key, item) {
                            item.inStock = item.shipQty;
                            item.totalStock = item.orderQty;
                        });

                        var ts = moment($scope.IssueDateVal, "DD-MM-YYYY HH:mm").valueOf();
                        var m = moment(ts);
                        var issueDate = new Date(m);
                        var milliseconds = parseInt(issueDate.getTime() / 1000.0);
                        $scope.ginObject.issueDate = "/Date(" + milliseconds + "000+0530)/";

                        var params = {
                            "item": $scope.ginObject
                        };

                        storeService.saveStoreItemGIN(params)
                            .then(function (response) {
                                if (response.errorMessage != '') {
                                    growlService.growl(response.errorMessage, "inverse");
                                }
                                else {
                                    growlService.growl("GIN details saved successfully.", "success");
                                    $window.history.back();
                                }
                            });
                    }
                }
            }

            $scope.itemDetailsSelect = function (itemDetails) {
                $scope.storeItemSerialNosSelected[itemDetails.itemID] = _.filter($scope.storeItemSerialNos[itemDetails.itemID], function (item) {
                    return item.isSelected == true;
                });

                $.each($scope.ginObject.storeItemDetails, function (key, item) {
                    if (item.itemID == itemDetails.itemID) {
                        item.itemDetails = $scope.storeItemSerialNosSelected[item.itemID];
                    }
                });
            };

            $scope.handleDate = function (date) {
                return new moment(date).format("DD-MMM-YYYY");
            }

            $scope.selectSerialNo = function (storeItemID) {
                $scope.currentStoreItemID = storeItemID;
                if ($scope.storeItemSerialNos[storeItemID] === undefined || $scope.storeItemSerialNos[storeItemID].length <= 0) {
                    storeService.getStoreItemDetails(storeItemID, 0)
                        .then(function (response) {
                            $scope.storeItemSerialNos[storeItemID] = response;
                        });
                }
            }

            $scope.search = function () {
                $scope.companyStoreItemsTemp = _.filter($scope.companyStoreItems, function (item) {
                    return item.itemCode.indexOf($scope.searchKey) > -1;
                });
            }

            $scope.companyStoreItem = {};

            $scope.getCompanyStoreItem = function (itemID) {
                $scope.companyStoreItem = _.filter($scope.companyStoreItems, function (x) { return x.itemID == itemID; });
                $scope.singleItemDetails = $scope.companyStoreItem[0];
                return $scope.singleItemDetails;
            }

            $scope.generateGinValidations = function () {

                $scope.phoneError = false;
                $scope.generateGinError = false;
                $scope.issueDateError = false;

                if ($scope.ginObject.phone == undefined) {
                    $scope.phoneError = true;
                    $scope.generateGinError = true;
                }
                if ($scope.IssueDateVal == undefined || $scope.IssueDateVal == '') {
                    $scope.issueDateError = true;
                    $scope.generateGinError = true;
                }

            };

        }]);