﻿prmApp
    .controller('techevaluationCtrl', ["$scope", "$state", "$stateParams", "$log", "userService", "growlService", "techevalService", "fileReader",
        function ($scope, $state, $stateParams, $log, userService, growlService, techevalService, fileReader) {
            $scope.evalID = $stateParams.evalID == "" ? 0 : $stateParams.evalID;
            $scope.reqID = $stateParams.reqID == "" ? 0 : $stateParams.reqID;
            $scope.userID = userService.getUserId();

            $scope.compID = userService.getUserCompanyId();
            $scope.sessionID = userService.getUserToken();

            $scope.techEvalutionList = [];

            techevalService.GetTechEvalution($scope.evalID, $scope.reqID, 0)
                .then(function (response) {
                    $scope.techEvalutionList = response;
                });

            $scope.goToAllTechEval = function () {
                $state.go("managetecheval");
            };

            $scope.saveRequestDetails = function (techEvalution, isApproved) {

                var techEvalObj = techEvalution;
                if (techEvalObj) {
                    techEvalObj.isApproved = isApproved;
                    techEvalObj.modifiedBy = $scope.userID;

                    var params = {
                        techeval: techEvalObj
                    };

                    techevalService.saveTechApproval(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                growlService.growl("Saved Successfully.", "success");
                                $window.history.back();
                            }
                        })
                }
            };

            $scope.goToVendorEvaluation = function (techEvaluation) {
                $state.go("techevalresponses", { "evalID": techEvaluation.evalID, "vendorID": techEvaluation.vendor.userID, "techEvalObj": techEvaluation });
            };
        }]);