﻿prmApp

    .controller('sapReportsOpenPOCtrl', ["$scope", "$http", "$state", "domain", "$filter", "$log", "$stateParams", "$timeout",
        "auctionsService", "reportingService", "userService", "SignalRFactory", "fileReader", "growlService",
        function ($scope, $http, $state, domain, $filter, $log, $stateParams, $timeout,
            auctionsService, reportingService, userService, SignalRFactory, fileReader, growlService) {

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 5;
            $scope.maxSize = 5; //Number of pager buttons to show

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/
        
            $scope.openPOArr = [];
            $scope.openChildPOArr = [];
            $scope.openPOArrInitial = [];
            $scope.openPOPivot = [];
            $scope.openPOPivotData = [];
            $scope.filterModelPlant = '0';
            $scope.filterModelPurchaseUser = '0';
            $scope.filterDateRange = 0;
            $scope.poitemcomments = [];
            $scope.selectedPOItem;
            $scope.poquantity = 0;
            $scope.pocomments = '';
            $scope.selectedGraphFilter;
            $scope.permission = 'VIEW';
            $scope.poitemcomments = [];

            $scope.getUserAccess = function () {
                reportingService.GetSapAccess(userService.getUserId())
                    .then(function (response) {
                        $log.info(response);
                        $scope.accessTypes = response;
                        if ($scope.accessTypes && $scope.accessTypes.length > 0) {
                            $scope.filterModelPurchaseUser = $scope.accessTypes[0].objectID;
                            $scope.permission = $scope.accessTypes[0].errorMessage;
                        }
                    });
            }

            $scope.getUserAccess();

            $scope.savePoComments = function (type) {
                var params = {
                    pono: $scope.selectedPOItem.PURCHASING_DOCUMENT,
                    itemno: $scope.selectedPOItem.ITEM,
                    quantity: $scope.poquantity,
                    comments: $scope.pocomments,
                    newdeliverdate: $scope.newdeliverdate,
                    type: type,
                    userid: parseInt(userService.getUserId()),
                    sessionid: userService.getUserToken()
                }

                var ts = moment(params.newdeliverdate, "DD-MM-YYYY").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                params.newdeliverdate = "/Date(" + milliseconds + "000+0530)/";

                //$scope.newdeliverdate

                reportingService.SavePoComments(params)
                    .then(function (response) {
                        $log.info(response);
                        $scope.newdeliverdate = '';
                        $scope.poquantity = 0;
                        $scope.pocomments = '';
                        //$scope.showComments($scope.selectedPOItem);
                    });
            }

            $scope.showComments = function (poitem, type) {
                $scope.selectedPOItem = poitem;
                $scope.poitemcomments = [];
                reportingService.GetPoComments(poitem.PURCHASING_DOCUMENT, poitem.ITEM, type)
                .then(function (response) {
                    $log.info(response);
                    $scope.poitemcomments = response;

                    $scope.poitemcomments.forEach(function (item, index) {
                        item.newdeliverdate = new moment(item.newdeliverdate).format("DD-MM-YYYY");
                        if (item.newdeliverdate.indexOf('-9999') > -1) {
                            item.newdeliverdate = "";
                        }
                    })

                });
            }

            $scope.getOpenPO = function (plant, purchase) {
                reportingService.GetOpenPO(0, '', plant, purchase)//userService.getUserCompanyId()
                    .then(function (response) {
                        $log.info(response);
                        $scope.openPOArr = response;
                        $scope.openPOArrInitial = response;

                        $scope.openPOArrInitial.forEach(function (item) {
                            item.PO_DATE1 = moment(item.PO_DATE).format('DD-MM-YYYY');
                            item.PR_CREATE_DATE1 = moment(item.PR_CREATE_DATE).format('DD-MM-YYYY');
                            item.PO_DELV_DATE1 = moment(item.PO_DELV_DATE).format('DD-MM-YYYY');
                            item.IDEAL_DELIVERY_DATE1 = moment(item.IDEAL_DELIVERY_DATE).format('DD-MM-YYYY');
                            //item.LEAD_TIME1 = moment(item.LEAD_TIME).format('DD-MM-YYYY');
                            //item.DELIV_DATE_FROM_TO1 = moment(item.DELIV_DATE_FROM_TO).format('DD-MM-YYYY');
                            //item.DELIVERY_DATE1 = moment(item.DELIVERY_DATE).format('DD-MM-YYYY');
                        });

                        /*PAGINATION CODE START*/
                        $scope.totalItems = $scope.openPOArr.length;
                        /*PAGINATION CODE END*/
                    });
            }

            $scope.getOpenPOPivot = function (plant, purchase) {
                reportingService.GetOpenPOPivot(0, plant, purchase)//userService.getUserCompanyId()
                .then(function (response) {
                    $log.info(response);
                    $scope.openPOPivot = response;
                    if ($scope.openPOPivot && $scope.openPOPivot.length > 0) {


                        $scope.openPOPivot.forEach(function (obj, key) {
                            var temp = {
                                "name": obj.key1,
                                "y": obj.value1
                            };

                            $scope.openPOPivotData.push(temp);
                        });

                        loadHighCharts();
                    }
                });
            }

            //$scope.getOpenPO(0, 0);
            //$scope.getOpenPOPivot(0, 0);

            function loadHighCharts() {
                Highcharts.chart('container2', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Open PO'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        type: 'category'
                    },
                    yAxis: {
                        title: {
                            text: 'Total No.of PO\'s'
                        }

                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true
                            },
                            point: {
                                events: {
                                    click: function () {
                                        //alert($scope.RequisitionDateArray[this.x].id);
                                        //$scope.filterDateRange = $scope.RequisitionDateArray[this.x + 1].id;
                                        $scope.selectedGraphFilter = $scope.RequisitionDateArray[this.x + 1]
                                        //$log.info($scope.RequisitionDateArray[this.x]);
                                        if (!$scope.$$phase) {
                                            $scope.$apply(function () {
                                                $scope.filter();
                                            })
                                        } else {
                                            $scope.filter();
                                        }
                                    }
                                }
                            }
                        }
                    },

                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b><br/>'
                    },

                    "series": [
                        {
                            "name": "PR COUNT",
                            "colorByPoint": true,
                            "data": $scope.openPOPivotData
                        }
                    ]
                });
            }




            $scope.RequisitionDateArray = [
                {
                    id: 0,
                    code: 'ALL',
                    from: 0,
                    to: 100000
                },
                {
                    id: 1,
                    code: 'DELIVERY MISSED',
                    from: 0,
                    to: 7
                },
                {
                    id: 7,
                    code: 'WITHIN 0 to 7 Days',
                    from: 0,
                    to: 7
                },
                {
                    id: 2,
                    code: 'WITHIN 7 to 14 Days',
                    from: 7,
                    to: 14
                },
                {
                    id: 3,
                    code: 'WITHIN 14 to 21 Days',
                    from: 14,
                    to: 21
                },
                {
                    id: 4,
                    code: 'WITHIN 21 to 35 Days',
                    from: 21,
                    to: 35
                },
                {
                    id: 5,
                    code: 'WITHIN 35 to 60 Days',
                    from: 35,
                    to: 60
                },
                {
                    id: 6,
                    code: '> 60',
                    from: 100,
                    to: 100000
                }

            ]

            $scope.filter = function () {
                $scope.openPOArr = [];
                if ($scope.selectedGraphFilter) {
                    var todayTemp = moment();
                    var today = moment();
                    var fromDate = new moment().add('days', +($scope.selectedGraphFilter.from));
                    var toDate = new moment().add('days', +($scope.selectedGraphFilter.to));
                    $scope.filterFromDate = fromDate.format('DD-MM-YYYY');
                    $scope.filterToDate = toDate.format('DD-MM-YYYY');
                    $scope.openPOArr = $scope.openPOArrInitial.filter(function (item) {
                        if (item && item.DELIVERY_RANGE) {
                            return item.DELIVERY_RANGE.toLowerCase().indexOf($scope.selectedGraphFilter.code.toLowerCase()) >= 0;
                        }
                    });
                }
                else {

                    $scope.openPOArr = $scope.openPOArrInitial;
                }

                $scope.totalItems = $scope.openPOArr.length;
            }

            $scope.filterGraph = function () {
                $scope.openPOPivot = [];
                $scope.openPOPivotData = [];
                $scope.openPOArrInitial = [];
                $scope.getOpenPOPivot($scope.filterModelPlant, $scope.filterModelPurchaseUser);
                $scope.getOpenPO($scope.filterModelPlant, $scope.filterModelPurchaseUser);
            }

            $scope.showPODetails = function (item) {
                reportingService.GetOpenPO(0, item.PO_NO, 0, 0)//userService.getUserCompanyId()
                    .then(function (response) {
                        $log.info(response);
                        $scope.openChildPOArr = response;
                    });
            }


            $scope.ImportEntity = {};

            $scope.getFile1 = function (id, doctype, ext) {
                $scope.progress = 0;
                $scope.file = $("#" + id)[0].files[0];
                $scope.docType = doctype + "." + ext;
                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {

                        if (id == "attachment") {
                            if (ext.toLowerCase() != "xlsx") {
                                swal("Error!", "File type should be XSLX.", "error");
                                return;
                            }
                            var bytearray = new Uint8Array(result);
                            $scope.ImportEntity.attachment = $.makeArray(bytearray);
                            $scope.ImportEntity.attachmentFileName = $scope.file.name;
                            $scope.importEntity('Open PR');
                        }

                        if (id == "attachment1") {
                            if (ext.toLowerCase() != "xlsx") {
                                swal("Error!", "File type should be XSLX.", "error");
                                return;
                            }
                            var bytearray = new Uint8Array(result);
                            $scope.ImportEntity.attachment = $.makeArray(bytearray);
                            $scope.ImportEntity.attachmentFileName = $scope.file.name;
                            $scope.importEntity('OPEN-PO');
                        }

                        if (id == "attachment2") {
                            if (ext.toLowerCase() != "xlsx") {
                                swal("Error!", "File type should be XSLX.", "error");
                                return;
                            }
                            var bytearray = new Uint8Array(result);
                            $scope.ImportEntity.attachment = $.makeArray(bytearray);
                            $scope.ImportEntity.attachmentFileName = $scope.file.name;
                            $scope.importEntity('PR-GRN');
                        }



                    });
            };           

            $scope.importEntity = function (entityName) {
                var params = {
                    "entity": {
                        entityID: userService.getUserCompanyId(),
                        attachment: $scope.ImportEntity.attachment,
                        userid: parseInt(userService.getUserId()),
                        entityName: entityName,
                        sessionid: userService.getUserToken()
                    }
                };

                auctionsService.importEntity(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            swal({
                                title: "Thanks!",
                                text: "Saved Successfully",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    location.reload();
                                });
                        } else {
                            swal({
                                title: "Failed!",
                                text: response.errorMessage,
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    location.reload();
                                });
                        }
                    })
            };

    }]);