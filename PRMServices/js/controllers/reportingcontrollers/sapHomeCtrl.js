﻿prmApp

    .controller('sapHomeCtrl', ["$scope", "$http", "$state", "domain", "$filter", "$log", "$stateParams", "$timeout",
        "auctionsService", "reportingService", "userService", "SignalRFactory", "fileReader", "growlService",
        function ($scope, $http, $state, domain, $filter, $log, $stateParams, $timeout,
            auctionsService, reportingService, userService, SignalRFactory, fileReader, growlService) {

            $scope.deliveryMissed = 0;
            $scope.deliveryAlert = 0;
            $scope.timeAvailable = 0;
            $scope.delivered = 0;

            $scope.minDateMoment = moment();
            //$scope.minDateMoment = moment().subtract(1, 'day');
            //$scope.minDateString = moment().subtract(1, 'day').format('YYYY-MM-DD');

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 8;
            $scope.maxSize = 5; //Number of pager buttons to show
            $scope.errorMessage = '';
            $scope.color = '';
            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/

            $scope.openPOArr = [];
            $scope.openPOArrForSortig = $scope.openPOArr;
            $scope.openChildPOArr = [];
            $scope.openPOArrInitial = [];
            $scope.openPOPivot = [];
            $scope.openPOPivotData = [];
            $scope.filterModelPurchaseUser = '0';
            $scope.filterDateRange = 0;
            $scope.poitemcomments = [];
            $scope.selectedPOItem;
            $scope.poquantity = 0;
            $scope.pocomments = '';
            $scope.selectedGraphFilter;
            $scope.monthWiseReport = {};
            $scope.tileFilter = '';
            $scope.filterModelPlant = '0';
            $scope.filterModelExclusion = '0';
            $scope.selectedMonth = '';
            $scope.displayDetails = false;
            $scope.tileFilterTextToDisplay = '';
            $scope.showDetailedView = false;
            $scope.accessTypes = [];
            $scope.permission = 'VIEW';
            $scope.lastUpdatedDate = "";
            $scope.commentType = "SHORTAGE";

            $scope.getLastUpdatedDate = function () {
                reportingService.GetLastUpdatedDate('sap_openpo')
                    .then(function (response) {
                        $log.info(response);
                        $scope.lastUpdatedDate = response.message;
                    });
            }

            $scope.getUserAccess = function () {
                reportingService.GetSapAccess(userService.getUserId())
                    .then(function (response) {
                        $log.info(response);
                        $scope.accessTypes = response;
                        if ($scope.accessTypes && $scope.accessTypes.length > 0) {
                            $scope.filterModelPurchaseUser = $scope.accessTypes[0].objectID;
                            $scope.permission = $scope.accessTypes[0].errorMessage;
                            $scope.getOpenPO(0, $scope.filterModelPurchaseUser,0);
                            //$scope.getOpenPRPivot(0, $scope.filterModelPurchaseUser);
                        }
                    });
            }

            $scope.getUserAccess();
            $scope.getLastUpdatedDate();

            $scope.savePoComments = function (type,category) {
                var isValid = true;
                $scope.errorMessage = '';

                if (($scope.category == '' || $scope.category == null || $scope.category == undefined)) {
                    $scope.categoryValidation = true;
                    return false;
                }

                //if (!$scope.newdeliverdate || $scope.newdeliverdate == '') {
                //    $scope.errorMessage = 'Date is mandatory';
                //}

                //if ($scope.newdeliverdate && $scope.newdeliverdate != '' && moment($scope.newdeliverdate, "DD-MM-YYYY").valueOf() < new moment().valueOf()) {
                //    $scope.errorMessage = 'Invalid date.';
                //}


                if (($scope.category == 'DELAY')) {
                    if ($scope.newdeliverdate == '' || $scope.newdeliverdate == null || moment($scope.newdeliverdate, "DD-MM-YYYY").valueOf() < new moment().valueOf()) {
                        $scope.newdeliverdateValidation = true;
                        //growlService.growl("Please enter valid New Delivery Date.", "inverse");
                        return false;
                    }

                } else {
                    $scope.newdeliverdateValidation = false;
                }

                //if (type == 'DELIVERY' || type == 'SHORTAGE' || type == 'SHORTAGE') {
                //    if (!$scope.newdeliverdate || $scope.newdeliverdate == '') {
                //        $scope.errorMessage = 'Date is mandatory';
                //    }

                //    if ($scope.newdeliverdate && $scope.newdeliverdate != '' && moment($scope.newdeliverdate, "DD-MM-YYYY").valueOf() < new moment().valueOf()) {
                //        $scope.errorMessage = 'Invalid date.';
                //    }
                //}
                //if (type == 'SHORTAGE'){
                //    if (!$scope.poquantity || $scope.poquantity == '' || $scope.poquantity <= 0) {
                //        $scope.errorMessage = 'Quantity is mandatory';
                //    }
                //}
                
                if ($scope.errorMessage != '') {
                    isValid = false;
                }

                if (isValid) {
                    var params = {
                        pono: $scope.selectedPOItem.PURCHASING_DOCUMENT,
                        itemno: $scope.selectedPOItem.ITEM,
                        quantity: $scope.poquantity,
                        comments: $scope.pocomments,
                        newdeliverdate: $scope.newdeliverdate,
                        materialdescription: $scope.selectedPOItem.SHORT_TEXT,
                        apiname: $scope.selectedPOItem.API_NAME,
                        type: type,
                        userid: parseInt(userService.getUserId()),
                        sessionid: userService.getUserToken(),
                        openpo: $scope.selectedPOItem,
                        category: category,
                        status: $scope.selectedPOItem.NWSTATUS
                    }

                    var ts = moment(params.newdeliverdate, "DD-MM-YYYY").valueOf();
                    var m = moment(ts);
                    var deliveryDate = new Date(m);
                    var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                    params.newdeliverdate = "/Date(" + milliseconds + "000+0530)/";

                    //$scope.newdeliverdate
                    // in po
                    reportingService.SavePoComments(params)
                        .then(function (response) {
                          //  $log.info("Comments response>>>>"+response);
                            $scope.newdeliverdate = '';
                            $scope.poquantity = 0;
                            $scope.pocomments = '';
                            $scope.errorMessage = '';
                            $scope.category = '';
                            growlService.growl("Saved successfully.", "success");
                            $scope.newdeliverdateValidation = false;
                            $scope.showComments($scope.selectedPOItem, type);
                        //    $scope.newdeliverdate = '';
                            //$scope.showComments($scope.selectedPOItem);
                        });

                    $scope.openPOArr.forEach(function (item, index) {
                        if (item.PURCHASING_DOCUMENT == $scope.selectedPOItem.PURCHASING_DOCUMENT && item.ITEM == $scope.selectedPOItem.ITEM) {
                            item.CATEGORY = $scope.category;
                            item.NWSTATUS = 'REVIEW PENDING';
                            item.color = 'Red';
                        }
                    });

                    $scope.openPOArrGlobalSearchResults.forEach(function (item, index) {
                        if (item.PURCHASING_DOCUMENT == $scope.selectedPOItem.PURCHASING_DOCUMENT && item.ITEM == $scope.selectedPOItem.ITEM) {
                            item.CATEGORY = $scope.category;
                            item.NWSTATUS = 'REVIEW PENDING';
                            item.color = 'Red';
                        }
                    });

                    //openPOArrGlobalSearchResults
                }
            }


           
          

            $scope.showComments = function (poitem, type) {
                $scope.selectedPOItem = poitem;
                $scope.poitemcomments = [];
                reportingService.GetPoComments(poitem.PURCHASING_DOCUMENT, poitem.ITEM, type)
                    .then(function (response) {
                        $log.info(response);
                        $scope.poitemcomments = response;

                        $scope.poitemcomments.forEach(function (item, index) {
                            item.newdeliverdate = new moment(item.currentTime).format("DD-MM-YYYY");
                            if (item.newdeliverdate.indexOf('-9999') > -1) {
                                item.newdeliverdate = "";
                            }
                            //if (item.remarksType == 'SHORTAGE') {
                            //    item.remarksType = "Shortage Comments";
                            //} else if (item.remarksType == 'DELAY') {
                            //    item.remarksType = "Delay Comments";
                            //} else if (item.remarksType == 'PURCHASE') {
                            //    item.remarksType = "Purchase Comments";
                            //}
                        })

                    });
            }

            $scope.updateStatus = function (poitem, status, index) {
                $scope.selectedPOItem = poitem;

                if ($scope.selectedPOItem.NWSTATUS == '' || $scope.selectedPOItem.NWSTATUS == null || $scope.selectedPOItem.NWSTATUS == undefined) {
                    return;
                }

                var params = {
                    pono: $scope.selectedPOItem.PURCHASING_DOCUMENT,
                    itemno: $scope.selectedPOItem.ITEM,
                    userid: parseInt(userService.getUserId()),
                    sessionid: userService.getUserToken(),
                    status: $scope.selectedPOItem.NWSTATUS

                }
                //if (($scope.category == '' || $scope.category == null || $scope.category == undefined)) {
                //    $scope.categoryValidation = true;
                //    return false;
                //}

                //if (($scope.newdeliverdate == '' || $scope.newdeliverdate == null || $scope.newdeliverdate <= CurrentDate)) {
                //    $scope.newdeliverdateValidation = true;
                //    return false;
                //}

                var ts = moment(params.newdeliverdate, "DD-MM-YYYY").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                params.newdeliverdate = "/Date(" + milliseconds + "000+0530)/";
                var CurrentDate = moment(new Date());
                //$scope.newdeliverdate


                reportingService.UpdatePoStatus(params)
                    .then(function (response) {
                        //     $log.info("resp>>>>>>"+response);
                        //      $scope.status = '';
                        growlService.growl("Status Saved successfully.", "success");
                        // $scope.showComments($scope.selectedPOItem, type);
                        //$scope.colorObj = {
                        //    color: ''
                        //};


                        $scope.openPOArr.forEach(function (item, index) {
                                if (item.PURCHASING_DOCUMENT == response.prNo && item.ITEM == response.prItemNo) {
                                    item.NWSTATUS = response.errorMessage;
                                    if (item.NWSTATUS == 'REVIEW PENDING') {
                                        item.color = 'Red';
                                    } else if (item.NWSTATUS == 'REVIEWED BUT ACTION PENDING') {
                                        item.color = 'Yellow';
                                    } else if (item.NWSTATUS == 'REVIEWED AND ACTION TAKEN') {
                                        item.color = 'GreenYellow';
                                    }
                                }
                        });


                        $scope.openPOArrGlobalSearchResults.forEach(function (item, index) {
                            if (item.PURCHASING_DOCUMENT == response.prNo && item.ITEM == response.prItemNo) {
                                item.NWSTATUS = response.errorMessage;
                                if (item.NWSTATUS == 'REVIEW PENDING') {
                                    item.color = 'Red';
                                } else if (item.NWSTATUS == 'REVIEWED BUT ACTION PENDING') {
                                    item.color = 'Yellow';
                                } else if (item.NWSTATUS == 'REVIEWED AND ACTION TAKEN') {
                                    item.color = 'GreenYellow';
                                }
                            }
                        });
                        //if (response.errorMessage == 'REVIEW PENDING') {
                        //    $scope.colorObj.color = 'Red';
                        //} else if (response.errorMessage == 'REVIEWED BUT ACTION PENDING') {
                        //    $scope.colorObj.color = 'Yellow';
                        //} else if (response.errorMessage == 'REVIEWED AND ACTION TAKEN') {
                        //    $scope.colorObj.color = 'GreenYellow';
                        //}
                    });
            };


            $scope.save = false;
            $scope.showSave = function (status) {//poitem, 
                //$scope.selectedPOItem = poitem;
                $scope.save = true;
            }

            $scope.commentTYpeChange = function (type) {
                $scope.poitemcomments = [];
                reportingService.GetPoComments($scope.selectedPOItem.PURCHASING_DOCUMENT, $scope.selectedPOItem.ITEM, type)
                    .then(function (response) {
                        $log.info(response);
                        $scope.poitemcomments = response;

                        $scope.poitemcomments.forEach(function (item, index) {
                            item.newdeliverdate = new moment(item.currentTime).format("DD-MM-YYYY");
                            if (item.newdeliverdate.indexOf('-9999') > -1) {
                                item.newdeliverdate = "";
                            }
                        })

                    });
            }
            // in po
            $scope.openPOArrInitialPageLoadData = [];
            $scope.getOpenPO = function (plant, purchase, exclusionList) {
                $scope.serchString = '';
                reportingService.GetOpenPO(0, '', plant, purchase, exclusionList)
                    .then(function (response) {
                        $log.info(response);

                      //  if ((plant == 0 || plant == '' || plant == null || plant == undefined) && (purchase == 0 || purchase == '' || purchase == null || purchase == undefined))
                      //  {
                       //     $scope.openPOArrInitialPageLoadData = response;
                    //    }
                        if ($scope.openPOArrInitialPageLoadData.length == 0) {
                            $scope.openPOArrInitialPageLoadData = response;
                        }
                        $scope.openPOArr = response;
                        $scope.openPOArrForSortig = $scope.openPOArr;
                        $scope.openPOArrInitial = response;

                        $scope.openPOArrInitial.forEach(function (item) {
                            item.PO_DATE1 = moment(item.PO_DATE).format('DD-MM-YYYY');
                            item.PR_CREATE_DATE1 = moment(item.PR_CREATE_DATE).format('DD-MM-YYYY');
                            item.PO_DELV_DATE1 = moment(item.PO_DELV_DATE).format('DD-MM-YYYY');
                            item.IDEAL_DELIVERY_DATE1 = moment(item.IDEAL_DELIVERY_DATE).format('DD-MM-YYYY');
                            item.PR_DELV_DATE1 = moment(item.PR_DELV_DATE).format('DD-MM-YYYY');
                            if (item.NWSTATUS == 'REVIEW PENDING') {
                                item.color = 'Red';
                            } else if (item.NWSTATUS == 'REVIEWED BUT ACTION PENDING') {
                                item.color = 'Yellow';
                            } else if (item.NWSTATUS == 'REVIEWED AND ACTION TAKEN') {
                                item.color = 'GreenYellow';
                            }
                            if (item.NEW_DELIVERY_DATE_COMM == null) {
                                item.NEW_DELIVERY_DATE_COMM = '-';
                            } else {
                                item.NEW_DELIVERY_DATE_COMM = moment(item.NEW_DELIVERY_DATE_COMM).format('DD-MM-YYYY');
                            }
                        });

                        $scope.openPOArr = $scope.openPOArrInitial.filter(function (item) {
                            return item.STATUS.toLowerCase().indexOf('DELIVERY MISSED'.toLowerCase()) >= 0;
                        });
                        $scope.openPOArrForSortig = $scope.openPOArr;

                        $scope.deliveryAlert = $scope.openPOArrInitial.length;
                        $scope.deliveryMissed = $scope.openPOArr.length;

                        var temp1 = $scope.openPOArrInitial.filter(function (item) {
                            return item.STATUS.toLowerCase().indexOf('TIME AVAILABLE'.toLowerCase()) >= 0;
                        });

                        if (temp1) {
                            $scope.timeAvailable = temp1.length;
                        }


                        var temp2 = $scope.openPOArrInitial.filter(function (item) {
                            return item.STATUS.toLowerCase().indexOf('DELIVERY ALERT'.toLowerCase()) >= 0;
                        });
                        
                        if (temp2) {
                            $scope.deliveryAlert = temp2.length;
                        }

                        var temp3 = $scope.openPOArrInitial.filter(function (item) {
                            return item.STATUS.toLowerCase().indexOf('DELIVERED'.toLowerCase()) >= 0;
                        });

                        if (temp3) {
                            $scope.delivered = temp3.length;
                        }


                        //$scope.delivered =
                        //$scope.deliveryAlert = $scope.deliveryAlert - ($scope.timeAvailable + $scope.deliveryMissed);
                        

                        $scope.totalItems = $scope.openPOArrInitial.length;


                        //$scope.setTileFilterValue('DELIVERY MISSED');



                        if (plant > 0) {
                            $scope.filterPlanData(plant);
                        }


                    });
            }
            
            $scope.RequisitionDateArray = [
                {
                    id: 0,
                    code: 'ALL',
                    from: 0,
                    to: 100000
                },
                {
                    id: 1,
                    code: 'DELIVERY MISSED',
                    from: 0,
                    to: 7
                },
                {
                    id: 7,
                    code: 'WITHIN 0 to 7 Days',
                    from: 0,
                    to: 7
                },
                {
                    id: 2,
                    code: 'WITHIN 7 to 14 Days',
                    from: 7,
                    to: 14
                },
                {
                    id: 3,
                    code: 'WITHIN 14 to 21 Days',
                    from: 14,
                    to: 21
                },
                {
                    id: 4,
                    code: 'WITHIN 21 to 35 Days',
                    from: 21,
                    to: 35
                },
                {
                    id: 5,
                    code: 'WITHIN 35 to 60 Days',
                    from: 35,
                    to: 60
                },
                {
                    id: 6,
                    code: '> 60',
                    from: 100,
                    to: 100000
                }

            ]
            
            
            //open po
            $scope.globalFilter = function () {
                $scope.serchString = '';
                $scope.openPOArrTemp = $scope.openPOArrInitial.filter(function (item) {
                    return item.STATUS.toLowerCase().indexOf($scope.tileFilter.toLowerCase()) >= 0;
                });

                if ($scope.selectedMonth && $scope.selectedMonth != '') {
                    $scope.openPOArrTemp = $scope.openPOArrTemp.filter(function (item) {
                        return item.IDEAL_DELIVERY_MONTH == $scope.selectedMonth;
                    });
                }

                if ($scope.filterModelPlant && $scope.filterModelPlant != '' && $scope.filterModelPlant != '0') {
                    $scope.openPOArrTemp = $scope.openPOArrTemp.filter(function (item) {
                        return item.PLANT.toLowerCase().indexOf($scope.filterModelPlant.toLowerCase()) >= 0;
                    });
                }

                $scope.openPOArr = $scope.openPOArrTemp;

                if (!$scope.$$phase) {
                    $scope.$apply(function () {
                        $scope.openPOArrForSortig = $scope.openPOArrTemp;
                        $scope.openPOArr = $scope.openPOArrTemp;
                        $scope.totalItems = $scope.openPOArrTemp.length;
                        $scope.showDetailedView = true;
                    })
                } else {
                    $scope.openPOArr = $scope.openPOArrTemp;
                    $scope.openPOArrForSortig = $scope.openPOArrTemp;
                    $scope.totalItems = $scope.openPOArrTemp.length;
                    $scope.showDetailedView = true;
                }
            }

            $scope.getFilteredOpenPoData = function (month) {
                $scope.selectedMonth = month;
                $scope.globalFilter();
            }
            
            $scope.filterPlanData = function (filterModelPlant) {
                $scope.filterModelPlant = filterModelPlant;
                $scope.globalFilter();
            }

            $scope.filterPurchaser = function () {
                $scope.filterModelPlant = '0';
                $scope.selectedMonth = '';
                $scope.openPOPivot = [];
                $scope.openPOPivotData = [];
                $scope.openPOArrInitial = [];
                $scope.showDetailedView = false;
                $scope.displayDetails = false;
                $scope.tileFilter = '';
                $scope.getOpenPO($scope.filterModelPlant, $scope.filterModelPurchaseUser, $scope.filterModelExclusion);
            }

            $scope.filterExclusionList = function () {
                $scope.getOpenPO($scope.filterModelPlant, $scope.filterModelPurchaseUser, $scope.filterModelExclusion);
            }

            $scope.showPODetails = function (item) {
                reportingService.GetOpenPO(0, item.PO_NO, 0, 0)//userService.getUserCompanyId()
                    .then(function (response) {
                        $log.info(response);
                        $scope.openChildPOArr = response;
                    });
            }

            $scope.ImportEntity = {};

            $scope.getFile1 = function (id, doctype, ext) {
                $scope.progress = 0;
                $scope.file = $("#" + id)[0].files[0];
                $scope.docType = doctype + "." + ext;
                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {


                        if (id == "attachment") {
                            if (ext.toLowerCase() != "xlsx") {
                                swal("Error!", "File type should be XSLX.", "error");
                                return;
                            }
                            var bytearray = new Uint8Array(result);
                            $scope.ImportEntity.attachment = $.makeArray(bytearray);
                            $scope.ImportEntity.attachmentFileName = $scope.file.name;
                            $scope.importEntity('OPEN-PO');
                        }

                        if (id == "attachment1") {
                            if (ext.toLowerCase() != "xlsx") {
                                swal("Error!", "File type should be XSLX.", "error");
                                return;
                            }
                            var bytearray = new Uint8Array(result);
                            $scope.ImportEntity.attachment = $.makeArray(bytearray);
                            $scope.ImportEntity.attachmentFileName = $scope.file.name;
                            $scope.importEntity('PR-GRN');
                        }



                    });
            };

            $scope.importEntity = function (entityName) {
                var params = {
                    "entity": {
                        entityID: userService.getUserCompanyId(),
                        attachment: $scope.ImportEntity.attachment,
                        userid: parseInt(userService.getUserId()),
                        entityName: entityName,
                        sessionid: userService.getUserToken()
                    }
                };

                auctionsService.importEntity(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            swal({
                                title: "Thanks!",
                                text: "Saved Successfully",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    location.reload();
                                });
                        } else {
                            swal({
                                title: "Failed!",
                                text: response.errorMessage,
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    location.reload();
                                });
                        }
                    })
            };

            function loadHighCharts() {
                Highcharts.chart('container', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Pending Po\'s'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        type: 'category'
                    },
                    yAxis: {
                        title: {
                            text: 'Total No.of PO\'s'
                        }

                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true
                            },
                            point: {
                                events: {
                                    click: function () {
                                        //alert($scope.RequisitionDateArray[this.x].id);
                                        //$scope.filterDateRange = $scope.RequisitionDateArray[this.x + 1].id;
                                        //$log.info($scope.RequisitionDateArray[this.x]);
                                        $scope.getFilteredOpenPoData(this.name);
                                        //if (!$scope.$$phase) {
                                        //    $scope.$apply(function () {
                                        //        $scope.getFilteredOpenPoData(this.name);
                                        //    })
                                        //} else {
                                        //    $scope.getFilteredOpenPoData(this.name)
                                        //}
                                    }
                                }
                            }
                        }
                    },

                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b><br/>'
                    },

                    "series": [
                        {
                            "name": "PR COUNT",
                            "colorByPoint": true,
                            "data": $scope.monthWiseReportPivotData
                        }
                    ],
                    "drilldown": {

                    }
                });
            }
            
            $scope.back = function()
            {
                //location.reload();
                $scope.showDetailedView = false;
                $scope.serchString = '';
            }
            
            $scope.filterPlanDataNew = function (filterModelPlant) {
                $scope.getOpenPO(filterModelPlant, $scope.filterModelPurchaseUser, $scope.filterModelExclusion);
            };
            

            $scope.filterPlanData = function (filterModelPlant) {
                $scope.filterModelPlant = filterModelPlant;

                $scope.openPOArr = $scope.openPOArrInitial.filter(function (item) {
                    return item.STATUS.toLowerCase().indexOf($scope.tileFilter.toLowerCase()) >= 0;
                });
                $scope.openPOArrForSortig = $scope.openPOArr;

                if ($scope.selectedMonth != '') {
                    $scope.openPOArr = $scope.openPOArrInitial.filter(function (item) {
                        return item.IDEAL_DELIVERY_MONTH == $scope.selectedMonth;
                    });
                    $scope.openPOArrForSortig = $scope.openPOArr;
                }

                if (!$scope.$$phase) {
                    $scope.$apply(function () {
                        $scope.openPOArr = $scope.openPOArr.filter(function (item) {
                            if ($scope.filterModelPlant && $scope.filterModelPlant != '' && $scope.filterModelPlant != '0') {
                                return item.PLANT == $scope.filterModelPlant;
                            }
                            else {
                                return true;
                            }
                        });
                        $scope.openPOArrForSortig = $scope.openPOArr;

                        $scope.totalItems = $scope.openPOArr.length;
                    })
                } else {
                    $scope.openPOArr = $scope.openPOArr.filter(function (item) {
                        if ($scope.filterModelPlant && $scope.filterModelPlant != '' && $scope.filterModelPlant != '0') {
                            return item.PLANT == $scope.filterModelPlant;
                        }
                        else {
                            return true;
                        }
                    });
                    $scope.openPOArrForSortig = $scope.openPOArr;
                    $scope.totalItems = $scope.openPOArr.length;
                }


            }


            $scope.setTileFilterValue = function (filterVal) {
                $scope.displayDetails = true;
                $scope.tileFilter = filterVal;
                $scope.monthWiseReport = {};
                $scope.selectedMonth = '';
                $scope.openPOArr = $scope.openPOArrInitial.filter(function (item) {
                    return item.STATUS.toLowerCase().indexOf(filterVal.toLowerCase()) >= 0;
                });

                $scope.openPOArrForSortig = $scope.openPOArr;
                $scope.totalItems = $scope.openPOArr.length;
                $scope.openPOArr.forEach(function (item, index) {
                    if (typeof $scope.monthWiseReport[item.IDEAL_DELIVERY_MONTH] !== 'undefined') {
                        $scope.monthWiseReport[item.IDEAL_DELIVERY_MONTH] = $scope.monthWiseReport[item.IDEAL_DELIVERY_MONTH] + 1;
                    } else {
                        $scope.monthWiseReport[item.IDEAL_DELIVERY_MONTH] = 1;
                    }
                });

                $scope.monthWiseReportPivotData = [];
                Object.keys($scope.monthWiseReport).forEach(function (key) {
                    var temp = {
                        "name": key,
                        "y": $scope.monthWiseReport[key]
                    };

                    $scope.monthWiseReportPivotData.push(temp);
                    console.log(key, $scope.monthWiseReport[key]);
                });

                loadHighCharts();

                if ($scope.filterModelPlant && $scope.filterModelPlant != '' && $scope.filterModelPlant != '0') {
                    $scope.openPOArr = $scope.openPOArrInitial.filter(function (item) {
                        return item.PLANT == $scope.filterModelPlant;
                    });
                    $scope.openPOArrForSortig = $scope.openPOArr;
                }

                //alert($scope.openPOArrInitial.length);
               // console.log("$scope.openPOArrForSortig>>>>>>>>>>>>>>" + $scope.openPOArrForSortig);
            }

            $scope.exportItemsToExcel = function () {
                var mystyle = {
                    sheetid: 'OpenPO',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };
                alasql.fn.decimalRound = function (x) { return Math.round(x); };   
                alasql('SELECT PURCHASER as [LP], PLANT_NAME as [Plant], MATERIAL as [MATERIAL], SHORT_TEXT as [DESCRIPTION], API_NAME as [API], NAME_OF_SUPPLIER as [SUPPLIER], '+
                    ' PR_CREATE_DATE1 as [PR DATE],PO_DATE1 as [PO DATE],PR_DELV_DATE1 as [PR Delivery Date],IDEAL_DELIVERY_DATE1 as [Ideal Delivery Date], ORDER_UNIT as [ORDER UNIT], ' +
                    ' decimalRound(PR_QUAN) as [PR Qty],decimalRound(ORDER_QUANTITY) as [PO Qty], decimalRound(STILL_TO_BE_DELIVERED_QTY) as [Open Qty], ' +
                    ' PR_NUMBER as [PR Number], PR_ITM_NO as [PR LN], PURCHASING_DOCUMENT as [PO Number], ITEM as [PO LINE ITEM], USER_ID as [PO Creator], MULTIPLE_PR_TEXT as [Multiple PR], ' +
                    ' IMPORT_LOCAL as [Supp. Country], LEAD_TIME as [Lead Time],' +
                    ' CATEGORY as [Category], NWSTATUS as [Status], NEW_DELIVERY_DATE_COMM as [New Delivery Date], COMMENTS as [Comments] ' +
                    ' INTO XLSX(?, { headers: true, sheetid: "OpenPO", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ', ["OpenPO.xlsx", $scope.openPOArr]);
            }



            $scope.exportItemsToExcelBasedOnFilterValue = function (filterVal) {
                var mystyle = {
                    sheetid: 'OpenPO',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };
                $scope.mainPOArrForSortig = $scope.openPOArrForSortig.filter(function (item) {
                    return item.STATUS.toLowerCase().indexOf(filterVal.toLowerCase()) >= 0;
                });
                console.log("export array value length>>>>>" + $scope.mainPOArrForSortig.length);
                alasql.fn.decimalRound = function (x) { return Math.round(x); };
                //alasql('SELECT PLANT_NAME as [Plant], MATERIAL as [Material], SHORT_TEXT as [Material Desc.], API_NAME as [API Name], PR_CREATE_DATE1 as [PR Date], PO_DATE1 as [PO Date], ' +
                //    ' ORDER_UNIT as [Order Unit], decimalRound(ORDER_QUANTITY) as [Order Quantity], decimalRound(STILL_TO_BE_DELIVERED_QTY) as [Still to be delivered (qty)], ' +
                //    ' PR_NUMBER as [PR Number], MULTIPLE_PR_TEXT as [Multiple PR], PURCHASING_DOCUMENT as [PO Number], USER_ID as [PO Creator], NAME_OF_SUPPLIER as [Name of Supplier], ' +
                //    ' PR_DELV_DATE1 as [PR Delivery Date], PO_DELV_DATE1 as [PO Delivery Date], IMPORT_LOCAL as [Import/ Local], LEAD_TIME as [Lead Time], IDEAL_DELIVERY_DATE1 as [Ideal Delivery Date], ' +
                //    ' CATEGORY as [Category], NWSTATUS as [Status], NEW_DELIVERY_DATE_COMM as [New Delivery Date], COMMENTS as [Comments] ' +
                //    ' INTO XLSX(?, { headers: true, sheetid: "OpenPO", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ', ["OpenPO -" + $scope.tileFilter+".xlsx", $scope.openPOArrForSortig]);
                alasql('SELECT PURCHASER as [LP], PLANT_NAME as [Plant], MATERIAL as [MATERIAL], SHORT_TEXT as [DESCRIPTION], API_NAME as [API], NAME_OF_SUPPLIER as [SUPPLIER], ' +
                    ' PR_CREATE_DATE1 as [PR DATE],PO_DATE1 as [PO DATE],PR_DELV_DATE1 as [PR Delivery Date],IDEAL_DELIVERY_DATE1 as [Ideal Delivery Date], ORDER_UNIT as [ORDER UNIT], ' +
                    ' decimalRound(PR_QUAN) as [PR Qty],decimalRound(ORDER_QUANTITY) as [PO Qty], decimalRound(STILL_TO_BE_DELIVERED_QTY) as [Open Qty], ' +
                    ' PR_NUMBER as [PR Number], PR_ITM_NO as [PR LN], PURCHASING_DOCUMENT as [PO Number], ITEM as [PO LINE ITEM], USER_ID as [PO Creator], MULTIPLE_PR_TEXT as [Multiple PR], ' +
                    ' IMPORT_LOCAL as [Supp. Country], LEAD_TIME as [Lead Time],' +
                    ' CATEGORY as [Category], NWSTATUS as [Status], NEW_DELIVERY_DATE_COMM as [New Delivery Date], COMMENTS as [Comments] ' +
                    ' INTO XLSX(?, { headers: true, sheetid: "OpenPO", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ', ["OpenPO -" + filterVal + ".xlsx", $scope.mainPOArrForSortig]);
            }

            $scope.retrieveShortageReport = function () {
                var mystyle = {
                    sheetid: 'OpenPO',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };

                reportingService.GetOpenPOReport(userService.getUserCompanyId())
                    .then(function (response) {
                        $log.info(response);
                        response.forEach(function (item) {
                            item.PO_DATE1 = moment(item.PO_DATE).format('DD-MM-YYYY');
                            item.PR_CREATE_DATE1 = moment(item.PR_CREATE_DATE).format('DD-MM-YYYY');
                            item.PO_DELV_DATE1 = moment(item.PO_DELV_DATE).format('DD-MM-YYYY');
                            item.IDEAL_DELIVERY_DATE1 = moment(item.IDEAL_DELIVERY_DATE).format('DD-MM-YYYY');
                            item.PR_DELV_DATE1 = moment(item.PR_DELV_DATE).format('DD-MM-YYYY');
                            item.NEW_DELIVERY_DATE1 = moment(item.NEW_DELIVERY_DATE).format('DD-MM-YYYY');
                            item.COMMENTS = item.COMMENTS == 'null' ? '' : item.COMMENTS;
                        });
                        alasql.fn.decimalRound = function (x) { return Math.round(x); };   
                        alasql('SELECT PLANT_NAME as [Plant], MATERIAL as [Material], SHORT_TEXT as [Material Desc.], API_NAME as [API Name], PR_NUMBER as [PR Number], ORDER_UNIT as [UOM], PURCHASING_DOCUMENT as [PO Number], decimalRound(ORDER_QUANTITY) as [Order Quantity], decimalRound(STILL_TO_BE_DELIVERED_QTY) as [Still to be delivered (qty)], PR_DELV_DATE1 as [PR Delivery Date], LEAD_TIME as [Lead Time], IDEAL_DELIVERY_DATE1 as [Ideal Delivery Date], COMMENTS as [Shortage Comments], PURCHASER as [Purchaser], NEW_DELIVERY_DATE1 as [New Delivery Date] INTO XLSX(?,{headers:true,sheetid: "OpenPO", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["OpenPOShortage.xlsx", response]);
                    });
            }

            $scope.sapReport = function () {
                var mystyle = {
                    sheetid: 'OpenPR',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };

                reportingService.GetOpenPRReport(userService.getUserCompanyId())
                    .then(function (GetOpenPRReportResponse) {
                        $log.info(GetOpenPRReportResponse);


                        reportingService.GetOpenPOReport(userService.getUserCompanyId())
                        .then(function (response) {
                            $log.info(response);

                            var excelArray = [];

                            GetOpenPRReportResponse.forEach(function (item) {
                                //PR
                                item.DELIVERY_DATE1 = moment(item.DELIVERY_DATE).format('DD-MM-YYYY');
                                item.PR_DELV_DATE1 = moment(item.PR_DELV_DATE).format('DD-MM-YYYY');
                                item.NEW_DELIVERY_DATE1 = moment(item.NEW_DELIVERY_DATE).format('DD-MM-YYYY');
                                item.REQUISITION_DATE1 = moment(item.REQUISITION_DATE).format('DD-MM-YYYY');
                                item.COMMENTS = item.COMMENTS == 'null' ? '' : item.COMMENTS;


                                item.IDEAL_DELIVERY_DATE1 = item.DELIVERY_DATE1;
                                item.STILL_TO_BE_DELIVERED_QTY = '';
                                item.CATEGORY = 'PR';

                                item.QUANTITY_REQUESTED = Math.round(item.QUANTITY_REQUESTED);

                                excelArray.push(item);

                            });

                            response.forEach(function (item) {
                                item.PO_DATE1 = moment(item.PO_DATE).format('DD-MM-YYYY');
                                item.PR_CREATE_DATE1 = moment(item.PR_CREATE_DATE).format('DD-MM-YYYY');
                                item.PO_DELV_DATE1 = moment(item.PO_DELV_DATE).format('DD-MM-YYYY');
                                item.IDEAL_DELIVERY_DATE1 = moment(item.IDEAL_DELIVERY_DATE).format('DD-MM-YYYY');
                                item.PR_DELV_DATE1 = moment(item.PR_DELV_DATE).format('DD-MM-YYYY');
                                item.NEW_DELIVERY_DATE1 = moment(item.NEW_DELIVERY_DATE).format('DD-MM-YYYY');
                                item.COMMENTS = item.COMMENTS == 'null' ? '' : item.COMMENTS;

                                item.UNIT_OF_MEASURE = item.ORDER_UNIT;
                                item.PURCHASE_REQUISITION = item.PURCHASING_DOCUMENT;
                                item.QUANTITY_ORDERED = item.ORDER_QUANTITY;
                                item.DELIVERY_DATE1 = item.PR_DELV_DATE1;
                                item.UNIT_OF_MEASURE = item.ORDER_UNIT;
                                item.QUANTITY_REQUESTED = '';
                                item.CATEGORY = 'PO';

                                item.STILL_TO_BE_DELIVERED_QTY = Math.round(item.STILL_TO_BE_DELIVERED_QTY);

                                excelArray.push(item);

                            });


                            alasql.fn.decimalRound = function (x) { return Math.round(x); };
                            alasql('SELECT ' +
                            ' PLANT_NAME as [Plant], ' +
                            ' MATERIAL as [Material Code],' +
                            ' SHORT_TEXT as [Material Description],' +
                            ' API_NAME as [API Name], ' +
                            ' PR_NUMBER as [PR Number],' +
                            ' UNIT_OF_MEASURE as [UOM],' +

                            ' QUANTITY_REQUESTED as [PR Qty],' +

                            ' PURCHASE_REQUISITION as [PO Number], ' +
                            ' decimalRound(QUANTITY_ORDERED) as [Order Quantity],' +

                            ' STILL_TO_BE_DELIVERED_QTY as [Still to be delivered (qty)], ' +

                            ' DELIVERY_DATE1 as [PR Delivery Date],' +
                            ' LEAD_TIME as [Lead Time], ' +
                            ' IDEAL_DELIVERY_DATE1 as [Ideal Delivery Date],' +

                            ' CATEGORY as [Category (Open PR/Open PO)], ' +

                            ' COMMENTS as [Shortage Comments], ' +
                            ' PURCHASER as [Purchaser],' +

                            ' NEW_DELIVERY_DATE1 as [New Delivery Date] ' +

                            ' INTO  XLSX(?,{headers:true,sheetid: "Shortage Report", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["Shortage Report.xlsx", excelArray]);

                            

                        });



                    });


            };

            $scope.scrollWin = function (id, scrollPosition) {
                var elmnt = document.getElementById(id);
                elmnt.scrollIntoView();
                //'+=1500'
                //$(".innerWrapper").animate({ scrollLeft: 100 + 200 }, 800);

                if (scrollPosition == 'BOTTOM')
                {
                    window.scroll({ bottom: elmnt.offsetBottom });
                }
                else {
                    window.scroll({ top: elmnt.offsetTop });
                }
                //window.scroll({ bottom: elmnt.offsetBottom });
                //window.scroll({ top: 10 });

                //window.scrollTo(500, 0);
                //document.body.scrollRight = 0; // For Chrome, Safari and Opera 
                //document.documentElement.scrollRight = 0; // For IE and Firefox

                //var el = $("#" + id);
                //var position = el.position();
                //console.log("left: " + position.left + ", top: " + position.top);
                //window.scroll({ top: position.top, left: position.left });
            };

            document.body.scrollTop = 0; // For Chrome, Safari and Opera 
            document.documentElement.scrollTop = 0; // For IE and Firefox


            $scope.serchString = '';
            $scope.SearchPOData = function (str) {
                $scope.openPOArrGlobalSearchResults = [];
                if (str && str != '' && str != null && str != undefined && str.length > 0) {
                    str = String(str).toUpperCase();
                    var openPOArr = $scope.openPOArrInitialPageLoadData.filter(function (item) {
                        return (String(item.NAME_OF_SUPPLIER).toUpperCase().includes(str) == true ||
                            String(item.API_NAME).toUpperCase().includes(str) == true ||
                            String(item.SHORT_TEXT).toUpperCase().includes(str) == true ||
                            String(item.MATERIAL).toUpperCase().includes(str) == true);
                    });

                    if (!$scope.$$phase) {
                        $scope.$apply(function () {
                            $scope.openPOArrGlobalSearchResults = openPOArr;
                            //$scope.openPOArrGlobalSearchResults.forEach(function (item, index) {
                            ////if (item.PURCHASING_DOCUMENT == response.prNo && item.ITEM == response.prItemNo) {
                            ////    item.NWSTATUS = response.errorMessage;
                            ////    if (item.NWSTATUS == 'REVIEW PENDING') {
                            ////        item.color = 'Red';
                            ////    } else if (item.NWSTATUS == 'REVIEWED BUT ACTION PENDING') {
                            ////        item.color = 'Yellow';
                            ////    } else if (item.NWSTATUS == 'REVIEWED AND ACTION TAKEN') {
                            ////        item.color = 'GreenYellow';
                            ////    }
                            ////}
                            //});


                            $scope.totalItems = $scope.openPOArrGlobalSearchResults.length;
                        })
                    } else {
                        $scope.openPOArrGlobalSearchResults = openPOArr;
                        //$scope.openPOArrGlobalSearchResults.forEach(function (item, index) {
                        //    //if (item.PURCHASING_DOCUMENT == response.prNo && item.ITEM == response.prItemNo) {
                        //    //    item.NWSTATUS = response.errorMessage;
                        //    //    if (item.NWSTATUS == 'REVIEW PENDING') {
                        //    //        item.color = 'Red';
                        //    //    } else if (item.NWSTATUS == 'REVIEWED BUT ACTION PENDING') {
                        //    //        item.color = 'Yellow';
                        //    //    } else if (item.NWSTATUS == 'REVIEWED AND ACTION TAKEN') {
                        //    //        item.color = 'GreenYellow';
                        //    //    }
                        //    //}
                        //});
                        $scope.totalItems = $scope.openPOArrGlobalSearchResults.length;
                    }

                    $scope.showDetailedView = true;
                    $scope.displayDetails = true;

                }
                else {
                    $scope.showDetailedView = false;
                    $scope.displayDetails = false;
                    $scope.serchString = '';
                    $scope.openPOArrGlobalSearchResults = [];
                }
            }

            $scope.clearForm = function () {
                $scope.category = '';
                $scope.newdeliverdate = '';
                $scope.pocomments = '';
            }

        }]);