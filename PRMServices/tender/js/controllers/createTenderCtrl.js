prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('createTenderCtrl', ["$state", "$stateParams", "$scope", "auctionsService", "userService", "$http", "$window", "domain",
        "fileReader", "growlService", "$log", "$filter", "ngDialog", "techevalService", "fwdauctionsService", "catalogService", "prmCompanyService",
        "workflowService", "PRMPRServices", "PRMCustomFieldService",
        function ($state, $stateParams, $scope, auctionsService, userService, $http, $window, domain,
            fileReader, growlService, $log, $filter, ngDialog, techevalService, fwdauctionsService, catalogService, prmCompanyService,
            workflowService, PRMPRServices, PRMCustomFieldService) {
            $scope.showCatalogQuotationTemplate = true;
            $scope.userID = userService.getUserId();
            $scope.cloneId = ($stateParams.reqObj || {}).cloneId;
            $scope.isClone = ($stateParams.reqObj && $stateParams.reqObj.cloneId);
            $scope.prmFieldMappingTemplates = [];
            $scope.selectedTemplate = 'DEFAULT';
            $scope.prmFieldMappingDetails = {};
            $scope.widgetStates = {
                MIN: 0,
                MAX: 1,
                PIN: -1
            };

            if ($stateParams.Id) {
                $scope.stateParamsReqID = $stateParams.Id;
                $scope.postRequestLoding = true;
                $scope.showCatalogQuotationTemplate = false;
                $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
                $scope.desigID = userService.getSelectedUserDesigID();
                $scope.deptTypeID = userService.getSelectedUserDepartmentDesignation().deptTypeID;

            } else {
                $scope.stateParamsReqID = 0;
                $scope.postRequestLoding = false;
                $scope.showCatalogQuotationTemplate = true;
                $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
                $scope.desigID = userService.getSelectedUserDesigID();
                $scope.deptTypeID = userService.getSelectedUserDepartmentDesignation().deptTypeID;

            };
            $scope.itemPreviousPrice = {};
            $scope.itemPreviousPrice.lastPrice = -1;
            $scope.itemLastPrice = {};
            $scope.itemLastPrice.lastPrice = -1;
            $scope.bestPriceEnable = 0;
            $scope.bestLastPriceEnable = 0;
            $scope.companyItemUnits = [];
            $scope.customFieldList = [];
            $scope.selectedcustomFieldList = [];
            $scope.otherRequirementItems = {
                INSTALLATION_CHARGES: true,
                PACKAGING: true,
                FREIGHT_CHARGES: true
            };
            
            $scope.SelectedVendorsCount = 0;

            
            $scope.isTechEval = false;
            $scope.isForwardBidding = false;
            $scope.allCompanyVendors = [];
            $scope.selectedProducts = [];

            var curDate = new Date();
            var today = moment();
            var tomorrow = today.add('days', 1);
            //var dateObj = $('.datetimepicker').datetimepicker({
            //    format: 'DD/MM/YYYY',
            //    useCurrent: false,
            //    minDate: tomorrow,
            //    keepOpen: false
            //});
            $scope.subcategories = [];
            $scope.sub = {
                selectedSubcategories: [],
            }
            $scope.selectedCurrency = {};
            $scope.currencies = [];
            $scope.questionnaireList = [];

            //$scope.postRequestLoding = false;
            $scope.selectedSubcategories = [];

            $scope.companyCatalogueList = [];

            $scope.selectVendorShow = true;
            $scope.isEdit = false;
            //Input Slider
            this.nouisliderValue = 4;
            this.nouisliderFrom = 25;
            this.nouisliderTo = 80;
            this.nouisliderRed = 35;
            this.nouisliderBlue = 90;
            this.nouisliderCyan = 20;
            this.nouisliderAmber = 60;
            this.nouisliderGreen = 75;

            //Color Picker
            this.color = '#03A9F4';
            this.color2 = '#8BC34A';
            this.color3 = '#F44336';
            this.color4 = '#FFC107';

            $scope.Vendors = [];
            $scope.VendorsTemp = [];
            $scope.categories = [];
            $scope.selectedA = [];
            $scope.selectedB = [];
            $scope.showCategoryDropdown = false;
            $scope.checkVendorPhoneUniqueResult = false;
            $scope.checkVendorEmailUniqueResult = false;
            $scope.checkVendorPanUniqueResult = false;
            $scope.checkVendorTinUniqueResult = false;
            $scope.checkVendorStnUniqueResult = false;
            $scope.showFreeCreditsMsg = false;
            $scope.showNoFreeCreditsMsg = false;
            $scope.formRequest = {
                isTabular: true,
                isRFP: false,
                auctionVendors: [],
                listRequirementItems: [],
                isQuotationPriceLimit: false,
                quotationPriceLimit: 0,
                quotationFreezTime: '',
                deleteQuotations: false,
                expStartTimeName: '',
                isDiscountQuotation: 0,
                isRevUnitDiscountEnable: 0,
                multipleAttachments: [],
                contractStartTime: '',
                contractEndTime: '',
                isContract: false,
                isRFQ: 1,
                biddingType: 'TENDER'
            };
            $scope.Vendors.city = "";
            $scope.Vendors.quotationUrl = "";
            $scope.vendorsLoaded = false;
            $scope.requirementAttachment = [];
            $scope.selectedProjectId = 0;
            $scope.selectedProject = {};
            $scope.branchProjects = [
                { PROJECT_ID: "RAW MATERIALS", PROJECT_CODE: "RAW MATERIALS"},
                { PROJECT_ID: "CAPEX", PROJECT_CODE: "CAPEX" },
                { PROJECT_ID: "SOLAR", PROJECT_CODE: "SOLAR" }
            ];

            $scope.changeProject = function () {
                console.log($scope.selectedProjectId);
                var filterdProjects = _.filter($scope.branchProjects, function (o) {
                    return o.PROJECT_ID == $scope.selectedProjectId;
                });
                if (filterdProjects && filterdProjects.length > 0) {
                    $scope.selectedProject = filterdProjects[0];
                }
            }

            $scope.sessionid = userService.getUserToken();

            $scope.selectedQuestionnaire == {}

            $scope.formRequest.indentID = 0;

            $scope.cijList = [];

            $scope.indentList = [];


            /*region start WORKFLOW*/
            $scope.workflowList = [];
            $scope.itemWorkflow = [];
            $scope.workflowObj = {};
            $scope.formRequest.workflowID = 0;
            $scope.currentStep = 0;
            $scope.orderInfo = 0;
            $scope.assignToShow = '';
            $scope.isWorkflowCompleted = false;
            $scope.WorkflowModule = 'QUOTATION';
            $scope.disableWFSelection = false;
            /*region end WORKFLOW*/


            
            $scope.compID = userService.getUserCompanyId();
            //prmCompanyService.getBranchProjects({ compid: $scope.compID, branchid: userService.getUserSelectedBranch(), projectid: 0, sessionid: userService.getUserToken() })
            //    .then(function (response) {
            //        $scope.branchProjects = response;
            //    });



            
            auctionsService.GetCompanyConfiguration($scope.compID, "ITEM_UNITS", userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.companyItemUnits = unitResponse;
                });

            $scope.getPreviousItemPrice = function (itemDetails, index) {
                $scope.itemPreviousPrice = {};
                $scope.itemPreviousPrice.lastPrice = -1;
                $scope.bestPriceEnable = index;
                $log.info(itemDetails);
                itemDetails.sessionID = userService.getUserToken();
                itemDetails.compID = userService.getUserCompanyId();
                auctionsService.getPreviousItemPrice(itemDetails)
                .then(function (response) {
                    if (response && response.errorMessage == '') {
                        $scope.itemPreviousPrice.lastPrice = Number(response.initialPrice);
                        $scope.itemPreviousPrice.lastPriceDate = userService.toLocalDate(response.currentTime);
                        $scope.itemPreviousPrice.lastPriceVendor = response.companyName;
                        $log.info($scope.itemPreviousPrice);
                    }
                });
            };

            $scope.showTable = true;

            $scope.dispalyLastprices = function (val) {
                $scope.showTable = val;
            }

            $scope.GetLastPrice = function (itemDetails, index) {
                $scope.itemLastPrice = {};
                $scope.itemLastPrice.lastPrice = -1;
                $scope.bestLastPriceEnable = index;
                $log.info(itemDetails);
                itemDetails.sessionID = userService.getUserToken();
                itemDetails.compID = userService.getUserCompanyId();
                auctionsService.GetLastPrice(itemDetails)
                    .then(function (response) {
                        $scope.itemLastPrice = response;

                        $scope.itemLastPrice.forEach(function (item, index) {

                            item.currentTime = userService.toLocalDate(item.currentTime);
                        })
                    });
            };




            $scope.budgetValidate = function () {
                if ($scope.formRequest.budget != "" && (isNaN($scope.formRequest.budget) || $scope.formRequest.budget.indexOf('.') > -1)) {
                    $scope.postRequestLoding = false;
                    swal({
                        title: "Error!",
                        text: "Please enter valid budget, budget should be greater than 1,00,000.",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {

                        });

                    $scope.formRequest.budget = "";
                }
            };

            $scope.clickToOpen = function () {
                ngDialog.open({ template: 'login/termsAddNewReq.html', width: 1000, height: 500 });
            };

            $scope.changeCategory = function () {

                $scope.selectedSubCategoriesList = [];
                $scope.formRequest.auctionVendors = [];
                $scope.loadSubCategories();
                //$scope.getvendors();
            }

            $scope.getCreditCount = function () {
                userService.getProfileDetails({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.userDetails = response;
                        //$scope.selectedCurrency = $filter('filter')($scope.currencies, { value: response.currency });
                        //$scope.selectedCurrency = $scope.selectedCurrency[0];
                        if (response.creditsLeft) {
                            $scope.showFreeCreditsMsg = true;
                        } else {
                            $scope.showNoFreeCreditsMsg = true;
                        }
                    });
            }



            $scope.VendorsList = [];
            $scope.VendorsTempList1 = [];

            $scope.getvendors = function (catObj) {
                $scope.ShowDuplicateVendorsNames = [];
                $scope.VendorsList = [];
                $scope.VendorsTempList1 = [];
                $scope.vendorsLoaded = false;
                var category = '';

                category = catObj.catCode;

                var params = { 'Categories': category, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId(), evalID: $scope.isTechEval ? $scope.selectedQuestionnaire.evalID : 0 };
                $http({
                    method: 'POST',
                    url: domain + 'getvendors',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            $scope.VendorsList = response.data;
                            $scope.VendorsTempList = $scope.VendorsList;



                            $scope.Vendors.forEach(function (item, index) {
                                $scope.VendorsTempList.forEach(function (item1, index1) {
                                    if (item.vendorID == item1.vendorID) {
                                        $scope.ShowDuplicateVendorsNames.push(item1);
                                        $scope.VendorsList.splice(index1, 1);
                                    }
                                })
                            });

                            if ($scope.formRequest.auctionVendors.length > 0) {
                                $scope.formRequest.auctionVendors.forEach(function (item1, index1) {
                                    $scope.VendorsTempList.forEach(function (item2, index2) {
                                        if (item1.vendorID == item2.vendorID) {
                                            $scope.ShowDuplicateVendorsNames.push(item2);
                                            $scope.VendorsList.splice(index2, 1);
                                        }
                                    });
                                });
                            }


                            $scope.VendorsTempList1 = $scope.VendorsList;

                            if ($scope.ShowDuplicateVendorsNames.length > 0) {
                                $scope.totalItems = $scope.ShowDuplicateVendorsNames.length;
                                //swal({
                                //    title: "Cancelled",
                                //    type: "error",
                                //    confirmButtonText: "Ok",
                                //    allowOutsideClick: true,
                                //    customClass: 'swal-wide'
                                //});
                                //$(".sweet-alert h2").html("oops...! Some vendors are already retrieved in products they are <h3 style='color:red'><div style='max-height: 400px;overflow-y: auto;overflow-x:scroll'>" + $scope.ShowDuplicateVendorsNames + "</div></h3> so vendors are not retrieved in the categories even though they are assigned.");
                            } else {
                                $scope.totalItems = $scope.ShowDuplicateVendorsNames.length;
                            }

                        } else {
                            $scope.VendorsList = [];
                        }

                        if ($scope.searchCategoryVendorstring != '') {
                            $scope.searchingCategoryVendors($scope.searchCategoryVendorstring);
                        } else {
                            $scope.searchingCategoryVendors('');
                        }


                    } else {

                    }
                }, function (result) {
                });
            };

            $scope.getReqQuestionnaire = function () {


            }

            $scope.getQuestionnaireList = function () {
                techevalService.getquestionnairelist(0)
                    .then(function (response) {
                        $scope.questionnaireList = $filter('filter')(response, { reqID: 0 });
                        if ($stateParams.Id && $stateParams.Id > 0) {
                            techevalService.getreqquestionnaire($stateParams.Id, 1)
                                .then(function (response) {
                                    $scope.selectedQuestionnaire = response;

                                    if ($scope.selectedQuestionnaire && $scope.selectedQuestionnaire.evalID > 0) {
                                        $scope.isTechEval = true;
                                    }

                                    $scope.questionnaireList.push($scope.selectedQuestionnaire);
                                })
                        }
                    })
            };



            // $scope.getQuestionnaireList();

            /*$scope.getvendorsbysubcat = function () {
                $scope.vendorsLoaded = false;
                var category = [];
    
                category.push($scope.sub.selectedSubcategories);
                //$scope.formRequest.category = category;
                if ($scope.formRequest.category != undefined) {
                    var params = { 'Categories': category, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId() };
                    $http({
                        method: 'POST',
                        url: domain + 'getvendorsbycatnsubcat',
                        encodeURI: true,
                        headers: { 'Content-Type': 'application/json' },
                        data: params
                    }).then(function (response) {
                        if (response && response.data) {
                            if (response.data.length > 0) {
                                $scope.Vendors = response.data;
                                $scope.vendorsLoaded = true;
                                for (var j in $scope.formRequest.auctionVendors) {
                                    for (var i in $scope.Vendors) {
                                        if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                            $scope.Vendors.splice(i, 1);
                                        }
                                    }
                                }
                            }
                            //$scope.formRequest.auctionVendors =[];
                        } else {
                        }
                    }, function (result) {
                    });
                }
    
            };*/

            $scope.isRequirementPosted = 0;


            $scope.getCategories = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getcategories?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {

                        if (response.data.length > 0) {
                            $scope.categories = _.uniq(_.map(response.data, 'category'));
                            $scope.categoriesdata = response.data;
                            $scope.showCategoryDropdown = true;
                        }
                    } else {
                    }
                }, function (result) {
                });
                
            };


            $scope.getCurrencies = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getkeyvaluepairs?parameter=CURRENCY',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            $scope.currencies = response.data;
                            if (!$scope.formRequest.currency) {
                                $scope.formRequest.currency = 'INR';
                            }

                            $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: $scope.formRequest.currency });
                            $scope.selectedCurrency = $scope.selectedCurrency[0];
                            
                            $scope.getCreditCount();
                        }
                    } else {
                    }
                }, function (result) {
                });
            };

            


            $scope.getData = function () {
                
                // $scope.getCategories();

                if ($stateParams.Id) {
                    var id = $stateParams.Id;
                    $scope.isEdit = true;

                    auctionsService.getrequirementdata({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), "userid": userService.getUserId() })
                        .then(function (response) {
                            $scope.selectedProjectId = response.projectId;
                            var category = response.category[0];
                            response.category = category;
                            response.taxes = parseInt(response.taxes);
                            //response.paymentTerms = parseInt(response.paymentTerms);
                            $scope.formRequest = response;
                            $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: $scope.formRequest.currency });
                            $scope.selectedCurrency = $scope.selectedCurrency[0];

                            //$scope.getPRNumber($scope.formRequest.PR_ID);
                            $scope.itemSNo = $scope.formRequest.itemSNoCount;

                            $scope.formRequest.checkBoxEmail = true;
                            $scope.formRequest.checkBoxSms = true;
                            $scope.loadSubCategories();

                            $scope.isRequirementPosted = $scope.formRequest.auctionVendors.length;



                            if (!$scope.formRequest.multipleAttachments) {
                                $scope.formRequest.multipleAttachments = [];
                            }
                            $scope.formRequest.attFile = response.attachmentName;
                            if ($scope.formRequest.attFile != '' && $scope.formRequest.attFile != null && $scope.formRequest.attFile != undefined) {


                                var attchArray = $scope.formRequest.attFile.split(',');

                                attchArray.forEach(function (att, index) {

                                    var fileUpload = {
                                        fileStream: [],
                                        fileName: '',
                                        fileID: att
                                    };

                                    $scope.formRequest.multipleAttachments.push(fileUpload);
                                })

                            }


                            $scope.selectedSubcategories = response.subcategories.split(",");
                            for (i = 0; i < $scope.selectedSubcategories.length; i++) {
                                for (j = 0; j < $scope.subcategories.length; j++) {
                                    if ($scope.selectedSubcategories[i] == $scope.subcategories[j].subcategory) {
                                        $scope.subcategories[j].ticked = true;
                                    }
                                }
                            }
                            //$scope.getvendors();
                            $scope.selectSubcat();
                            $scope.formRequest.attFile = response.attachmentName;
                            $scope.formRequest.quotationFreezTime = userService.toLocalDate($scope.formRequest.quotationFreezTime);
                            //$scope.formRequest.quotationFreezTimeNew = userService.toLocalDate($scope.formRequest.quotationFreezTime);
                            //$scope.formRequest.urgency.push(urgency);

                            $scope.formRequest.expStartTime = userService.toLocalDate($scope.formRequest.expStartTime);

                            $scope.SelectedVendors = $scope.formRequest.auctionVendors;

                            $scope.SelectedVendorsCount = $scope.formRequest.auctionVendors.length;
                            

                            $scope.formRequest.listRequirementItems.forEach(function (item, itemIndex) {
                                item.isNonCoreProductCategory = !item.isCoreProductCategory; //Try to clean up keep only one property
                                if (item.productQuotationTemplateJson && item.productQuotationTemplateJson != '' && item.productQuotationTemplateJson != null && item.productQuotationTemplateJson != undefined) {
                                    item.productQuotationTemplateArray = JSON.parse(item.productQuotationTemplateJson);
                                    item.productQuotationTemplate = JSON.parse(item.productQuotationTemplateJson);
                                }
                                else {
                                    item.productQuotationTemplateArray = [];
                                }

                                if (item.productQuotationTemplate && item.productQuotationTemplate.length > 0) {
                                    item.productQuotationTemplate.pop();
                                }

                                $scope.GetProductQuotationTemplate(item.catalogueItemID, itemIndex);
                            });


                            $scope.postRequestLoding = false;
                            $scope.selectRequirementPRS();
                        });
                }

            };

            $scope.showSimilarNegotiationsButton = function (value, searchstring) {
                $scope.showSimilarNegotiations = value;
                if (!value) {
                    $scope.CompanyLeads = {};
                }
                if (value) {
                    if (searchstring.length < 3) {
                        $scope.CompanyLeads = {};
                    }
                    if (searchstring.length > 2) {
                        $scope.searchstring = searchstring;
                        $scope.GetCompanyLeads(searchstring);
                    }
                }
                return $scope.showSimilarNegotiations;
            }

            $scope.showSimilarNegotiations = false;

            $scope.CompanyLeads = {};

            $scope.searchstring = '';

            $scope.GetCompanyLeads = function (searchstring) {
                if (searchstring.length < 3) {
                    $scope.CompanyLeads = {};
                }
                if ($scope.showSimilarNegotiations && searchstring.length > 2) {
                    $scope.searchstring = searchstring;
                    var params = { "userid": userService.getUserId(), "searchstring": $scope.searchstring, "searchtype": 'Title', "sessionid": userService.getUserToken() };
                    auctionsService.GetCompanyLeads(params)
                        .then(function (response) {
                            $scope.CompanyLeads = response;
                            $scope.CompanyLeads.forEach(function (item, index) {
                                item.postedOn = userService.toLocalDate(item.postedOn);
                            })
                        });
                }
            }


            $scope.changeScheduledAuctionsLimit = function () {
                $scope.scheduledLimit = 8;
                $scope.getMiniItems();
            }

            $scope.loadSubCategories = function () {
                $scope.subcategories = _.filter($scope.categoriesdata, { category: $scope.formRequest.category });
                /*$scope.subcategories = _.map($scope.subcategories, 'subcategory');*/
            }

            $scope.selectedSubCategoriesList = [];

            $scope.selectSubcat = function (subcat) {

                $scope.selectedSubCategoriesList = [];

                if (!$scope.isEdit) {
                    $scope.formRequest.auctionVendors = [];
                }
                $scope.vendorsLoaded = false;
                var category = [];
                var count = 0;
                var succategory = "";
                $scope.sub.selectedSubcategories = $filter('filter')($scope.subcategories, { ticked: true });
                selectedcount = $scope.sub.selectedSubcategories.length;
                if (selectedcount > 0) {
                    succategory = _.map($scope.sub.selectedSubcategories, 'id');
                    category.push(succategory);

                    $scope.selectedSubCategoriesList = succategory;

                    //$scope.formRequest.category = category;
                    if ($scope.formRequest.category != undefined) {
                        var params = { 'Categories': succategory, 'sessionID': userService.getUserToken(), 'count': selectedcount, 'uID': userService.getUserId(), evalID: $scope.selectedQuestionnaire ? $scope.selectedQuestionnaire.evalID : 0 };
                        $http({
                            method: 'POST',
                            url: domain + 'getvendorsbycatnsubcat',
                            encodeURI: true,
                            headers: { 'Content-Type': 'application/json' },
                            data: params
                        }).then(function (response) {
                            if (response && response.data) {
                                if (response.data.length > 0) {
                                    $scope.Vendors = response.data;
                                    $scope.vendorsLoaded = true;
                                    for (var j in $scope.formRequest.auctionVendors) {
                                        for (var i in $scope.Vendors) {
                                            if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                                $scope.Vendors.splice(i, 1);
                                            }
                                        }
                                    }

                                    $scope.VendorsTemp = $scope.Vendors;
                                    $scope.searchVendors('');

                                }
                                //$scope.formRequest.auctionVendors =[];
                            } else {
                            }
                        }, function (result) {
                        });
                    }
                } else {
                    //$scope.getvendors();
                }

            }

            $scope.getData();

            $scope.checkVendorUniqueResult = function (idtype, inputvalue) {


                if (idtype == "PHONE") {
                    $scope.checkVendorPhoneUniqueResult = false;
                } else if (idtype == "EMAIL") {
                    $scope.checkVendorEmailUniqueResult = false;
                }
                else if (idtype == "PAN") {
                    $scope.checkVendorPanUniqueResult = false;
                }
                else if (idtype == "TIN") {
                    $scope.checkVendorTinUniqueResult = false;
                }
                else if (idtype == "STN") {
                    $scope.checkVendorStnUniqueResult = false;
                }

                if (inputvalue == "" || inputvalue == undefined) {
                    return false;
                }

                userService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
                    if (idtype == "PHONE") {
                        $scope.checkVendorPhoneUniqueResult = !response;
                    } else if (idtype == "EMAIL") {
                        $scope.checkVendorEmailUniqueResult = !response;
                    }
                    else if (idtype == "PAN") {
                        $scope.checkVendorPanUniqueResult = !response;
                    }
                    else if (idtype == "TIN") {
                        $scope.checkVendorTinUniqueResult = !response;
                    }
                    else if (idtype == "STN") {
                        $scope.checkVendorStnUniqueResult = !response;
                    }
                });
            };
            $scope.selectForA = function (item) {
                var index = $scope.selectedA.indexOf(item);
                if (index > -1) {
                    $scope.selectedA.splice(index, 1);
                } else {
                    $scope.selectedA.splice($scope.selectedA.length, 0, item);
                }
                for (i = 0; i < $scope.selectedA.length; i++) {
                    $scope.formRequest.auctionVendors.push($scope.selectedA[i]);
                    $scope.Vendors.splice($scope.Vendors.indexOf($scope.selectedA[i]), 1);
                    $scope.VendorsTemp.splice($scope.VendorsTemp.indexOf($scope.selectedA[i]), 1);
                }
                $scope.reset();
            }

            $scope.selectForB = function (item) {
                var index = $scope.selectedB.indexOf(item);
                if (index > -1) {
                    $scope.selectedB.splice(index, 1);
                } else {
                    $scope.selectedB.splice($scope.selectedA.length, 0, item);
                }
                for (i = 0; i < $scope.selectedB.length; i++) {
                    $scope.Vendors.push($scope.selectedB[i]);
                    $scope.VendorsTemp.push($scope.selectedB[i]);
                    $scope.formRequest.auctionVendors.splice($scope.formRequest.auctionVendors.indexOf($scope.selectedB[i]), 1);
                }
                $scope.reset();
            }

            $scope.AtoB = function () {

            }

            $scope.BtoA = function () {

            }

            $scope.reset = function () {
                $scope.selectedA = [];
                $scope.selectedB = [];
            }

            // $scope.getFile = function () {
            //     $scope.progress = 0;
            //     fileReader.readAsDataUrl($scope.file, $scope)
            //     .then(function(result) {
            //         $scope.formRequest.attachment = result;
            //     });
            // };

            $scope.multipleAttachments = [];

            $scope.getFile = function () {
                $scope.progress = 0;

                //$scope.file = $("#attachement")[0].files[0];
                $scope.multipleAttachments = $("#attachement")[0].files;

                $scope.multipleAttachments = Object.values($scope.multipleAttachments)


                $scope.multipleAttachments.forEach(function (item, index) {

                    fileReader.readAsDataUrl(item, $scope)
                        .then(function (result) {

                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };

                            var bytearray = new Uint8Array(result);

                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = item.name;

                            if (!$scope.formRequest.multipleAttachments) {
                                $scope.formRequest.multipleAttachments = [];
                            }

                            $scope.formRequest.multipleAttachments.push(fileUpload);

                        });

                })

            };


            $scope.newVendor = {};
            $scope.Attaachmentparams = {};
            $scope.deleteAttachment = function (reqid) {
                $scope.Attaachmentparams = {
                    reqID: reqid,
                    userID: userService.getUserId()
                }
                auctionsService.deleteAttachment($scope.Attaachmentparams)
                    .then(function (response) {
                        if (response.errorMessage != "") {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            growlService.growl("Attachment deleted Successfully", "inverse");
                            $scope.getData();
                        }
                    });
            }

            $scope.newVendor.panno = "";
            $scope.newVendor.vatNum = "";
            $scope.newVendor.serviceTaxNo = "";

            $scope.addVendor = function () {
                $scope.emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                $scope.mobileRegx = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
                $scope.panregx = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
                //$scope.emailRegx = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
                var addVendorValidationStatus = false;
                $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                if ($scope.newVendor.companyName == "" || $scope.newVendor.companyName === undefined) {
                    $scope.companyvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.firstName == "" || $scope.newVendor.firstName === undefined) {
                    $scope.firstvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.lastName == "" || $scope.newVendor.lastName === undefined) {
                    $scope.lastvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.contactNum == "" || $scope.newVendor.contactNum === undefined || isNaN($scope.newVendor.contactNum)) {
                    $scope.contactvalidation = true;
                    addVendorValidationStatus = true;
                }
                else if ($scope.newVendor.contactNum.length != 10) {
                    $scope.contactvalidationlength = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.email == "" || $scope.newVendor.email === undefined) {
                    $scope.emailvalidation = true;
                    addVendorValidationStatus = true;
                }
                else if (!$scope.emailRegx.test($scope.newVendor.email)) {
                    $scope.emailregxvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.vendorcurrency == "" || $scope.newVendor.vendorcurrency === undefined) {
                    $scope.vendorcurrencyvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.panno != "" && $scope.newVendor.panno != undefined && !$scope.panregx.test($scope.newVendor.panno)) {
                    $scope.panregxvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.vatNum != "" && $scope.newVendor.vatNum != undefined && $scope.newVendor.vatNum.length != 11) {
                    $scope.tinvalidation = true;
                    addVendorValidationStatus = true;
                }

                if ($scope.newVendor.serviceTaxNo != "" && $scope.newVendor.serviceTaxNo != undefined && $scope.newVendor.serviceTaxNo.length != 15) {
                    $scope.stnvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.formRequest.category == "" || $scope.formRequest.category === undefined) {
                    $scope.categoryvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.checkVendorEmailUniqueResult || $scope.checkVendorEmailUniqueResult) {
                    addVendorValidationStatus = true;
                }
                if (addVendorValidationStatus) {
                    return false;
                }
                var vendCAtegories = [];
                $scope.newVendor.category = $scope.formRequest.category;
                vendCAtegories.push($scope.newVendor.category);
                var params = {
                    "register": {
                        "firstName": $scope.newVendor.firstName,
                        "lastName": $scope.newVendor.lastName,
                        "email": $scope.newVendor.email,
                        "phoneNum": $scope.newVendor.contactNum,
                        "username": $scope.newVendor.contactNum,
                        "password": $scope.newVendor.contactNum,
                        "companyName": $scope.newVendor.companyName ? $scope.newVendor.companyName : "",
                        "isOTPVerified": 0,
                        "category": $scope.newVendor.category,
                        "userType": "VENDOR",
                        "panNumber": ("panno" in $scope.newVendor) ? $scope.newVendor.panno : "",
                        "stnNumber": ("serviceTaxNo" in $scope.newVendor) ? $scope.newVendor.serviceTaxNo : "",
                        "vatNumber": ("vatNum" in $scope.newVendor) ? $scope.newVendor.vatNum : "",
                        "referringUserID": userService.getUserId(),
                        "knownSince": ("knownSince" in $scope.newVendor) ? $scope.newVendor.knownSince : "",
                        "errorMessage": "",
                        "sessionID": "",
                        "userID": 0,
                        "department": "",
                        "currency": $scope.newVendor.vendorcurrency.key,
                        "altPhoneNum": $scope.newVendor.altPhoneNum,
                        "altEmail": $scope.newVendor.altEmail,
                        "subcategories": $scope.selectedSubCategoriesList
                    }
                };
                $http({
                    method: 'POST',
                    url: domain + 'register',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if ((response && response.data && response.data.errorMessage == "") || response.data.errorMessage == 'User assigned to Company.') {
                        $scope.formRequest.auctionVendors.push({ vendorName: $scope.newVendor.firstName + " " + $scope.newVendor.lastName, companyName: $scope.newVendor.companyName, vendorID: response.data.objectID });
                        $scope.newVendor = null;
                        $scope.newVendor = {};
                        //$scope.addVendorForm.$setPristine();
                        $scope.addVendorShow = false;
                        $scope.selectVendorShow = true;
                        $scope.newVendor = {};
                        $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                        growlService.growl("Vendor Added Successfully.", 'inverse');
                    } else if (response && response.data && response.data.errorMessage) {
                        growlService.growl(response.data.errorMessage, 'inverse');
                    } else {
                        growlService.growl('Unexpected Error Occurred', 'inverse');
                    }
                });
            }

            //$scope.checkboxModel = {
            //    value1: true
            //};

            $scope.formRequest.checkBoxEmail = true;
            $scope.formRequest.checkBoxSms = true;
            //$scope.postRequestLoding = false;
            $scope.formRequest.urgency = 'High (Will be Closed in 2 Days)';
            $scope.formRequest.deliveryLocation = '';
            $scope.formRequest.paymentTerms = '';
            $scope.formRequest.isSubmit = 0;


            $scope.titleValidation = $scope.attachmentNameValidation = false;
            $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = false;
            $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.questionnaireValidation = false;

            $scope.postRequest = function (isSubmit, pageNo, navigateToView, stage) {

                $scope.titleValidation = $scope.attachmentNameValidation = false;
                $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = false;
                $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.questionnaireValidation = false;

                $scope.postRequestLoding = true;
                $scope.formRequest.isSubmit = isSubmit;

                if (isSubmit == 1 || pageNo == 1) {
                    if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                        $scope.titleValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if (isSubmit == 1) {
                        if ($scope.formRequest.isTabular) {
                            $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                                if (!item.attachmentName || item.attachmentName == '') {
                                    $scope.attachmentNameValidation = true;
                                    return false;
                                }
                            })

                        }
                        if (!$scope.formRequest.isTabular) {
                            if ($scope.formRequest.description == null || $scope.formRequest.description == '') {
                                $scope.descriptionValidation = true;
                                $scope.postRequestLoding = false;
                                return false;
                            }
                        }
                    }
                }

                if (isSubmit == 1) {
                    if ($scope.formRequest.deliveryTime == null || $scope.formRequest.deliveryTime == '') {
                        $scope.deliveryTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.deliveryLocation == null || $scope.formRequest.deliveryLocation == '') {
                        $scope.deliveryLocationValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.paymentTerms == null || $scope.formRequest.paymentTerms == '') {
                        $scope.paymentTermsValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    
                }

                if (isSubmit == 1) {
                    if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                        $scope.urgencyValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    
                    if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= '') && $scope.formRequest.isQuotationPriceLimit == true) {
                        $scope.quotationPriceLimitValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.isQuotationPriceLimit == false) {
                        $scope.formRequest.quotationPriceLimit = 0;
                    }
                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.noOfQuotationRemindersValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.remindersTimeIntervalValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.isTechEval == true && (!$scope.selectedQuestionnaire || $scope.selectedQuestionnaire.evalID <= 0)) {
                        $scope.questionnaireValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        $scope.quotationFreezTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }

                if (!$scope.postRequestLoding) {
                    return false;
                }


                if (pageNo != 4) {
                    $scope.textMessage = "Save as Draft.";
                }

                if ($scope.formRequest.checkBoxEmail == true && $scope.formRequest.checkBoxSms == true && pageNo == 4) {
                    $scope.textMessage = "This will send an email and sms invite to all the vendors selected above.";
                }
                else if ($scope.formRequest.checkBoxEmail == true && pageNo == 4) {
                    $scope.textMessage = "This will send an email invite to all the vendors selected above.";
                }
                else if ($scope.formRequest.checkBoxSms == true && pageNo == 4) {
                    $scope.textMessage = "This will send an SMS invite to all the vendors selected above.";
                }
                else if (pageNo == 4) {
                    $scope.textMessage = "This will not send an SMS or EMAIL the vendors selected above.";
                }

                if ($scope.stateParamsReqID && !$scope.formRequest.deleteQuotations && pageNo == 4 && !$scope.isClone) {
                    $scope.textMessage = "Please select \"Request New Quotations\" to vendors if you modify any details in Requirement item specifications / Quantity. Click \"Cancel\" to select the option.";
                }

                $scope.formRequest.currency = $scope.selectedCurrency.value;
                $scope.formRequest.timeZoneID = 190;
                $scope.postRequestLoding = false;
                swal({
                    title: "Are you sure?",
                    text: $scope.textMessage,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                    $scope.postRequestLoding = true;
                    var ts = userService.toUTCTicks($scope.formRequest.quotationFreezTime);
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    $scope.formRequest.quotationFreezTime = "/Date(" + milliseconds + "000++530)/";
                    // this post request
                    


                    var ts = userService.toUTCTicks($scope.formRequest.expStartTime);
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    $scope.formRequest.expStartTime = "/Date(" + milliseconds + "000+0530)/";

                    $scope.formRequest.requirementID = $stateParams.Id ? $stateParams.Id : -1;
                    $scope.formRequest.customerID = userService.getUserId();
                    $scope.formRequest.customerFirstname = userService.getFirstname();
                    $scope.formRequest.customerLastname = userService.getLastname();
                    $scope.formRequest.isClosed = "NOTSTARTED";
                    $scope.formRequest.endTime = "";
                    $scope.formRequest.sessionID = userService.getUserToken();
                    $scope.formRequest.subcategories = "";
                    $scope.formRequest.budget = 100000;
                    $scope.formRequest.custCompID = userService.getUserCompanyId();
                    $scope.formRequest.customerCompanyName = userService.getUserCompanyId();

                    //for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                    //    $scope.formRequest.subcategories += $scope.sub.selectedSubcategories[i].subcategory + ",";
                    //}
                    var category = [];
                    //category.push($scope.formRequest.category);
                    $scope.formRequest.category = category;

                    $scope.formRequest.listRequirementItems.forEach(function (item, itemIndexs) {
                        if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                            item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                subItem.dateCreated = "/Date(1561000200000+0530)/";
                                subItem.dateModified = "/Date(1561000200000+0530)/";
                            })
                        }
                    });

                    if (!$scope.formRequest.isForwardBidding) {
                        $scope.formRequest.cloneID = 0;
                        if ($stateParams.reqObj && $stateParams.reqObj.cloneId && $stateParams.reqObj.cloneId !== "") {
                            $scope.formRequest.cloneID = $stateParams.reqObj.cloneId;
                            $scope.formRequest.auctionVendors = _.filter($scope.formRequest.auctionVendors, function (x) { return x.companyName !== 'PRICE_CAP'; });
                        }

                        if ($scope.PRList && $scope.PRList.length > 0) {
                            var selectedPRs = _.filter($scope.PRList, function (x) { return x.isSelected; });
                            var selectedPRList = [];
                            if (selectedPRs && selectedPRs.length > 0) {
                                selectedPRs.forEach(function (selectedPR, itemIndexs) {
                                    selectedPRList.push(selectedPR.PR_ID);
                                });

                                $scope.formRequest.PR_ID = selectedPRList.join(',');
                            }                            
                        }

                        auctionsService.postrequirementdata($scope.formRequest)
                            .then(function (response) {
                                if (response.objectID != 0) {
                                    if ($scope.selectedProject) {
                                        $scope.selectedProject.sessionID = userService.getUserToken();
                                        $scope.selectedProject.requirement = {};
                                        $scope.selectedProject.requirement.requirementID = response.objectID;
                                        var param = { item: $scope.selectedProject };
                                        //prmCompanyService.SaveProjectRequirement(param).then(function (projecgtMappingResponse) {
                                        //    console.log("saving project mapping");
                                        //    console.log(projecgtMappingResponse);
                                        //    console.log("END saving project mapping");
                                        //});
                                    }
                                    
                                    $scope.SaveReqDepartments(response.objectID);
                                    $scope.saveRequirementCustomFields(response.objectID);
                                    //$scope.SaveReqDeptDesig(response.objectID);

                                    //$scope.DeleteRequirementTerms();
                                    //$scope.SaveRequirementTerms(response.objectID);

                                    if ($scope.selectedQuestionnaire != null && $scope.selectedQuestionnaire != undefined && $scope.selectedQuestionnaire.evalID != undefined) {
                                        techevalService.getquestionnaire($scope.selectedQuestionnaire.evalID, 1)
                                            .then(function (response1) {
                                                $scope.selectedQuestionnaire = response1;

                                                if ($scope.isTechEval) {
                                                    $scope.selectedQuestionnaire.reqID = response.objectID;
                                                }
                                                else {
                                                    $scope.selectedQuestionnaire.reqID = 0;
                                                }
                                                $scope.selectedQuestionnaire.sessionID = userService.getUserToken();
                                                var params = {
                                                    questionnaire: $scope.selectedQuestionnaire
                                                }
                                                techevalService.assignquestionnaire(params)
                                                    .then(function (response) {
                                                    })
                                            })
                                    }
                                    if (stage) {
                                        $state.go('save-tender', { Id: response.objectID });
                                    }
                                    swal({
                                        title: "Done!",
                                        text: "Requirement Saved Successfully",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },
                                        function () {
                                            //$scope.postRequestLoding = false;
                                            //$state.go('view-requirement');

                                            if (navigateToView && stage == undefined) {
                                                $state.go('view-tender', { 'Id': response.objectID });
                                            } else {
                                                if ($scope.stateParamsReqID > 0 && stage == undefined) {
                                                    location.reload();
                                                }
                                                else {
                                                    if (stage == undefined) {
                                                        //$state.go('form.addnewrequirement', { 'Id': response.objectID });
                                                        $state.go('save-tender', { Id: response.objectID });
                                                    }
                                                }
                                            }
                                        });
                                }
                            });
                    } else {
                        fwdauctionsService.postrequirementdata($scope.formRequest)
                            .then(function (response) {
                                if (response.objectID != 0) {

                                    //$scope.SaveReqDepartments(response.objectID);
                                    $scope.SaveReqDeptDesig(response.objectID);

                                    swal({
                                        title: "Done!",
                                        text: "Requirement Created Successfully",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },
                                        function () {
                                            //$scope.postRequestLoding = false;
                                            //$state.go('view-requirement');
                                            $state.go('fwd-view-req', { 'Id': response.objectID });
                                        });
                                }
                            });
                    }
                });
                //$scope.postRequestLoding = false;
            };


            $scope.ItemFile = '';
            $scope.itemSNo = 1;
            $scope.ItemFileName = '';

            $scope.itemnumber = $scope.formRequest.listRequirementItems.length;

            $scope.requirementItems =
                {
                    productSNo: $scope.itemSNo++,
                    ItemID: 0,
                    productIDorName: '',
                    productNo: '',
                    productDescription: '',
                    productQuantity: 0,
                    productBrand: '',
                    othersBrands: '',
                    isDeleted: 0,
                    itemAttachment: '',
                    attachmentName: '',
                    itemMinReduction: 0,
                    splitenabled: 0,
                    fromrange: 0,
                    torange: 0,
                    requiredQuantity: 0,
                    productCode: ''
                }

            $scope.formRequest.listRequirementItems.push($scope.requirementItems);
            $scope.AddItem = function () {
                $window.scrollBy(0, 50);
                $scope.itemnumber = $scope.formRequest.listRequirementItems.length;
                $scope.requirementItems =
                    {
                        productSNo: $scope.itemSNo++,
                        ItemID: 0,
                        productIDorName: '',
                        productNo: '',
                        productDescription: '',
                        productQuantity: 0,
                        productBrand: '',
                        othersBrands: '',
                        isDeleted: 0,
                        itemAttachment: '',
                        attachmentName: '',
                        itemMinReduction: 0,
                        splitenabled: 0,
                        fromrange: 0,
                        torange: 0,
                        requiredQuantity: 0,
                        productCode: ''
                    }

                $scope.formRequest.listRequirementItems.push($scope.requirementItems);

                //Widget Management
                $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                    if (item.productSNo !== $scope.requirementItems.productSNo && item.widgetState !== -1 && item.productIDorName !== '') { //&& item.productCode !== ''
                        item.widgetState = 0;
                    }
                });
                //^Widget Management
            };

            $scope.handleWindowState = function (product, state) {
                // if (product.productCode !== '') { //item.productIDorName !== ''
                if (product.productIDorName !== '') { //item.productIDorName !== ''
                    product.widgetState = state;
                } else {
                    swal("Error!", "Item details cannot be empty", "error");
                }
            }

            $scope.AddOtherRequirementItem = function (nonCoreProduct) {
                var item = {};
                if (nonCoreProduct) {
                    var output = $scope.productsList.filter(function (product) {
                        return (String(product.prodCode).toUpperCase().includes(nonCoreProduct.prodCode.toUpperCase()) == true);
                    });
                    if (output) {
                        nonCoreProduct.doHide = true;
                        item = output[0];
                    }
                }

                if (item) {
                    var index = 0;
                    if (!$scope.formRequest.listRequirementItems[$scope.formRequest.listRequirementItems.length - 1].productCode) {
                        $scope.AddItem();
                        index = $scope.formRequest.listRequirementItems.length - 1;

                    } else {
                        $scope.AddItem();
                        index = $scope.formRequest.listRequirementItems.length - 1;
                    }

                    item.isNonCoreProductCategory = 1;
                    $scope.fillTextbox(item, index);
                }
            };

            $scope.deleteItem = function (product) {
                //$scope.formRequest.listRequirementItems.splice(SNo, 1);
                
                if (!$scope.isEdit) {
                    if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 1) {
                        $scope.formRequest.listRequirementItems = _.filter($scope.formRequest.listRequirementItems, function (x) { return x.productSNo !== product.productSNo; });
                    };
                } else {
                    product.isDeleted = 1;
                }



                $scope.nonCoreproductsList.forEach(function (item, index) {
                    if(item.prodNo === product.productNo) {
                        item.doHide = false;
                    }
                });
                
                if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length === 1 
                    && $scope.formRequest.listRequirementItems[0].isNonCoreProductCategory === 1) {
                    location.reload();
                }
            };



            $scope.getFile1 = function (id, itemid, ext) {
                $scope.file = $("#" + id)[0].files[0];

                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        if (id == "itemsAttachment") {
                            if (ext != "xlsx") {
                                swal("Error!", "File type should be XSLX. Please download the template and  fill values accordingly.", "error");
                                return;
                            }
                            var bytearray = new Uint8Array(result);
                            $scope.formRequest.itemsAttachment = $.makeArray(bytearray);
                            if (!$scope.isEdit) {
                                $scope.formRequest.listRequirementItems = [];
                            }

                            $scope.postRequest(0, 1, false);
                            //$scope.formRequest.itemsAttachmentName = $scope.file.name;
                        }

                        if (id != "itemsAttachment") {
                            var bytearray = new Uint8Array(result);
                            var arrayByte = $.makeArray(bytearray);
                            var ItemFileName = $scope.file.name;
                            var index = _.indexOf($scope.formRequest.listRequirementItems, _.find($scope.formRequest.listRequirementItems, function (o) { return o.productSNo == id; }));
                            var obj = $scope.formRequest.listRequirementItems[index];
                            obj.itemAttachment = arrayByte;
                            obj.attachmentName = ItemFileName;
                            $scope.formRequest.listRequirementItems.splice(index, 1, obj);
                        }
                    });
            }

            $scope.pageNo = 1;

            $scope.nextpage = function (pageNo) {
                $scope.tableValidation = false;
                $scope.tableValidationMsg = '';
                $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = departmentsValidation = false;
                $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.remindersTimeIntervalValidation = $scope.questionnaireValidation = false;

                $scope.titleValidation = $scope.attachmentNameValidation = false;
                $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = false;
                $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.questionnaireValidation = $scope.expNegotiationTimeValidation = false;

                if (pageNo == 1 || pageNo == 4) {

                    if ($scope.reqDepartments == null || $scope.reqDepartments == undefined || $scope.reqDepartments.length == 0) {
                        $scope.GetReqDepartments();
                    }
                    
                    if ($scope.currencies == null || $scope.currencies == undefined || $scope.currencies.length == 0) {
                        //$scope.getCurrencies();
                    }

                    if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                        $scope.titleValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }

                    if ($scope.formRequest.isTabular) {
                        $scope.selectedProducts = [];
                        $scope.formRequest.listRequirementItems.forEach(function (item, index) {

                            if ($scope.tableValidation || item.isDeleted) {
                                return false;
                            }

                            var sno = parseInt(index) + 1;

                            if (!$scope.formRequest.isRFP) {
                                if (item.productCode == null || item.productCode == '') {
                                    $scope.tableValidation = true;
                                    $scope.tableValidationMsg = 'Please enter product Code for item: ' + sno;
                                    return;
                                }
                            }
                            
                            if (item.productIDorName == null || item.productIDorName == '') {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = 'Please enter product Code/Name for item: ' + sno;
                                return;
                            }
                            else {
                                $scope.selectedProducts.push(item.catalogueItemID);
                                var productQuotationTemplateFiltered = _.filter(item.productQuotationTemplate, function (o) {
                                    return o.IS_VALID == 1;
                                });
                                if (productQuotationTemplateFiltered != null && productQuotationTemplateFiltered != undefined && productQuotationTemplateFiltered.length > 0) {

                                    

                                    item.productQuotationTemplateJson = JSON.stringify(productQuotationTemplateFiltered);
                                } else {
                                    item.productQuotationTemplateJson = '';
                                }


                            }
                            //if (item.productNo == null || item.productNo == '') {
                            //    $scope.tableValidation = true;
                            //}
                            //if (item.productDescription == null || item.productDescription == '') {
                            //    $scope.tableValidation = true;
                            //}
                            if (item.productQuantity <= 0 || item.productQuantity == undefined || item.productQuantity == '') {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = 'Please enter product Quantity for item: ' + sno;
                                return;
                            }
                            if (item.productQuantityIn == null || item.productQuantityIn == '') {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = 'Please enter product Units for item: ' + sno;
                                return;
                            }
                            if ($scope.formRequest.isSplitEnabled == true) {
                                if (item.fromrange == null || item.fromrange == '' || item.fromrange <= 0) {
                                    $scope.tableValidation = true;
                                    $scope.tableValidationMsg = 'Please enter From Range for item: ' + sno;
                                    return;
                                }
                                if (item.torange == null || item.torange == '' || item.torange <= 0) {
                                    $scope.tableValidation = true;
                                    $scope.tableValidationMsg = 'Please enter To Range for item: ' + sno;
                                    return;
                                }

                                if (parseFloat(item.fromrange) > parseFloat(item.torange)) {
                                    $scope.tableValidation = true;
                                    $scope.tableValidationMsg = 'Please make sure that To Range is higher than From Range for item: ' + sno;
                                    return;
                                }

                                //if (item.requiredQuantity == null || item.requiredQuantity == '' || item.requiredQuantity <= 0) {
                                //    $scope.tableValidation = true;
                                //    $scope.tableValidationMsg = 'Please enter Required Qty for item: ' + sno;
                                //    return;
                                //}


                            }
                        });


                        if ($scope.tableValidation) {
                            return false;
                        }

                    }
                    if (!$scope.formRequest.isTabular) {
                        if ($scope.formRequest.description == null || $scope.formRequest.description == '') {
                            $scope.descriptionValidation = true;
                            $scope.postRequestLoding = false;
                            return false;
                        }
                    }
                     

                    var phn = '';
                    if ($scope.userObj && $scope.userObj.phoneNum)
                    {
                        phn = $scope.userObj.phoneNum;
                        $scope.formRequest.phoneNum = phn;
                        $scope.formRequest.contactDetails = phn;
                    }
                    else {
                        $scope.formRequest.phoneNum = '';
                        $scope.formRequest.contactDetails = '';
                    }

                                       

                }

                if (pageNo == 2 || pageNo == 4) {

                    

                    if ($scope.questionnaireList == null || $scope.questionnaireList == undefined || $scope.questionnaireList.length == 0) {
                        $scope.getQuestionnaireList();
                    }

                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        var d = new Date();
                        d.setHours(18, 00, 00);
                        d = new moment(d).format("DD-MM-YYYY HH:mm");
                        $scope.formRequest.quotationFreezTime = d;
                        
                    }

                    
                    if ($scope.formRequest.expStartTime == null || $scope.formRequest.expStartTime == '') {
                        var dt = new Date();
                        var tomorrowNoon = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate() + 1, 12, 0, 0);
                        tomorrowNoon = new moment(tomorrowNoon).format("DD-MM-YYYY HH:mm");
                        $scope.formRequest.expStartTime = tomorrowNoon;

                    }

                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.formRequest.noOfQuotationReminders = 3;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.formRequest.remindersTimeInterval = 2;
                    }
                    if ($scope.formRequest.deliveryTime == null || $scope.formRequest.deliveryTime == '') {
                        $scope.deliveryTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.deliveryLocation == null || $scope.formRequest.deliveryLocation == '') {
                        $scope.deliveryLocationValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.paymentTerms == null || $scope.formRequest.paymentTerms == '') {
                        $scope.paymentTermsValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    

                    if ($scope.reqDepartments.length > 0) {

                        $scope.departmentsValidation = true;

                        $scope.reqDepartments.forEach(function (item, index) {
                            if (item.isValid == true || $scope.noDepartments == true)
                                $scope.departmentsValidation = false;
                        });

                        if ($scope.departmentsValidation == true) {
                            return false;
                        };
                    }

                }

                if (pageNo == 3 || pageNo == 4) {

                    //if ($scope.categories == null || $scope.categories == undefined || $scope.categories.length == 0) {
                    //    $scope.getCategories();
                    //}
                    $scope.getproductvendors();

                    if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                        $scope.urgencyValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    

                    if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= 0) && $scope.formRequest.isQuotationPriceLimit == true) {
                        $scope.quotationPriceLimitValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.noOfQuotationRemindersValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.remindersTimeIntervalValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.isTechEval == true && (!$scope.selectedQuestionnaire || $scope.selectedQuestionnaire.evalID <= 0)) {
                        $scope.questionnaireValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        $scope.quotationFreezTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }

                    var ts = moment($scope.formRequest.quotationFreezTime, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var quotationFreezTime = new Date(m);

                    var ts = moment($scope.formRequest.expStartTime, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var expStartTime = new Date(m);

                    auctionsService.getdate()
                        .then(function (GetDateResponse) {
                            //var CurrentDate = moment(new Date(parseInt(GetDateResponse.substr(6))));

                            var CurrentDateToLocal = userService.toLocalDate(GetDateResponse);

                            var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                            var m = moment(ts);
                            var deliveryDate = new Date(m);
                            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                            var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";

                            var CurrentDate = moment(new Date(parseInt(CurrentDateToTicks.substr(6))));

                            if (quotationFreezTime < CurrentDate) {
                                $scope.quotationFreezTimeValidation = true;
                                $scope.postRequestLoding = false;
                                return false;
                            }
                            //else if (quotationFreezTime >= expStartTime) {
                            //        $scope.expNegotiationTimeValidation = true;
                            //        $scope.postRequestLoding = false;
                            //        return false;
                            //    }
                            $scope.pageNo = $scope.pageNo + 1;
                            //  console.log("page NO:::::::::::" + $scope.pageNo);

                        });

                }

                if (pageNo == 4) {
                    $scope.formRequest.subcategoriesView = '';
                    for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                        $scope.formRequest.subcategoriesView += $scope.sub.selectedSubcategories[i].subcategory + ",";
                    }
                    $scope.pageNo = $scope.pageNo + 1;
                }

                if (pageNo != 3) {
                    $scope.pageNo = $scope.pageNo + 1;
                }

                // return $scope.pageNo;
                // location.reload();
            }

            $scope.previouspage = function () {
                $scope.pageNo = $scope.pageNo - 1;
                // location.reload();
                //return $scope.pageNo;
            }

            $scope.gotopage = function (pageNo) {
                $scope.pageNo = pageNo;

                
                return $scope.pageNo;
            }


            $scope.reqDepartments = [];
            $scope.userDepartments = [];

            $scope.GetReqDepartments = function () {
                $scope.reqDepartments = [];
                auctionsService.GetReqDepartments(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.reqDepartments = response;

                            $scope.noDepartments = true;

                            $scope.reqDepartments.forEach(function (item, index) {
                                if (item.isValid == true) {
                                    $scope.noDepartments = false;
                                }
                            });

                        }
                    });
            }

            // $scope.GetReqDepartments();


            $scope.SaveReqDepartments = function (reqID) {

                $scope.reqDepartments.forEach(function (item, index) {
                    item.reqID = reqID;
                    item.userID = userService.getUserId();
                })

                var params = {
                    "listReqDepartments": $scope.reqDepartments,
                    sessionID: userService.getUserToken()
                };
                if ($scope.formRequest.isForwardBidding) {
                    fwdauctionsService.SaveReqDepartments(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                               
                            }
                        });
                } else {
                    auctionsService.SaveReqDepartments(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                               
                            }
                        });
                }


            };

            $scope.noDepartments = false;


            $scope.noDepartmentsFunction = function (value) {

                $scope.departmentsValidation = false;

                if (value == 'NA') {
                    $scope.reqDepartments.forEach(function (item, index) {
                        item.isValid = false;
                    });
                }
                else {
                    $scope.reqDepartments.forEach(function (item, index) {
                        if (item.isValid == true)
                            $scope.noDepartments = false;
                    });
                }
            };


            $scope.reqDeptDesig = [];

            $scope.GetReqDeptDesig = function () {
                $scope.reqDeptDesig = [];
                auctionsService.GetReqDeptDesig(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.reqDeptDesig = response;
                            $scope.noDepartments = true;
                            $scope.reqDeptDesig.forEach(function (item, index) {
                                if (item.isValid == true) {
                                    $scope.noDepartments = false;
                                }
                            });
                        }
                    });
            };

            // $scope.GetReqDeptDesig();


            $scope.SaveReqDeptDesig = function (reqID) {

                $scope.reqDeptDesig.forEach(function (item, index) {
                    item.reqID = reqID;
                    item.userID = userService.getUserId();
                })

                var params = {
                    "listReqDepartments": $scope.reqDeptDesig,
                    sessionID: userService.getUserToken()
                };

                if ($scope.formRequest.isForwardBidding) {
                    fwdauctionsService.SaveReqDeptDesig(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {

                            }
                        });
                } else {
                    auctionsService.SaveReqDeptDesig(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                //growlService.growl(".", "success");
                                //$scope.GetCompanyDepartments();
                                //$scope.addnewdeptView = false;
                                //$scope.department = {
                                //    userID: $scope.userID,
                                //    deptID: 0,
                                //    sessionID: $scope.sessionID
                                //};
                                ////$window.history.back();
                            }
                        });
                }


            };

            $scope.AssignVendorToCompany = function (vendorPhone, vendorEmail) {

                if ($scope.newVendor.altPhoneNum == undefined) {
                    $scope.newVendor.altPhoneNum = '';
                }
                if ($scope.newVendor.altEmail == undefined) {
                    $scope.newVendor.altEmail = '';
                }

                var params = {
                    userID: userService.getUserId(),
                    vendorPhone: vendorPhone,
                    vendorEmail: vendorEmail,
                    sessionID: userService.getUserToken(),
                    category: $scope.formRequest.category,
                    subCategory: $scope.selectedSubCategoriesList,
                    altPhoneNum: $scope.newVendor.altPhoneNum,
                    altEmail: $scope.newVendor.altEmail
                };

                auctionsService.AssignVendorToCompany(params)
                    .then(function (response) {

                        $scope.vendor = response;

                        if ($scope.vendor.errorMessage == '' || $scope.vendor.errorMessage == 'VENDOR ADDED SUCCESSFULLY') {
                            $scope.formRequest.auctionVendors.push({ vendorName: $scope.vendor.firstName + ' ' + $scope.vendor.lastName, companyName: $scope.vendor.email, vendorID: $scope.vendor.userID });
                            $scope.newVendor = {};
                            $scope.checkVendorPhoneUniqueResult = false;
                            $scope.checkVendorEmailUniqueResult = false;
                            $scope.checkVendorPanUniqueResult = false;
                            $scope.checkVendorTinUniqueResult = false;
                            $scope.checkVendorStnUniqueResult = false;
                            $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                            growlService.growl('Vendor Added Successfully', "success");
                        }
                        else {
                            $scope.newVendor = {};
                            $scope.checkVendorPhoneUniqueResult = false;
                            $scope.checkVendorEmailUniqueResult = false;
                            $scope.checkVendorPanUniqueResult = false;
                            $scope.checkVendorTinUniqueResult = false;
                            $scope.checkVendorStnUniqueResult = false;
                            $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                            growlService.growl($scope.vendor.errorMessage, "inverse");
                        }
                    });
            };



            $scope.searchvendorstring = '';

            $scope.searchVendors = function (value) {
                $scope.Vendors = _.filter($scope.VendorsTemp, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1; });
                // $scope.vendorsLoaded = true;
                // $scope.Vendors = _.filter($scope.allCompanyVendors, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1; });
                // var temp = $scope.formRequest.auctionVendors;
                // $scope.formRequest.auctionVendors.forEach(function (item, index) {
                // $scope.Vendors = _.filter($scope.Vendors, function (vendor) { return vendor.vendorID !== item.vendorID; });
                // });
            }


            $scope.exportItemsToExcel = function () {
                var mystyle = {
                    sheetid: 'RequirementDetails',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };

                alasql('SELECT itemID as [ItemID], productIDorName as [ProductName], productNo as [ProductNumber], productDescription as [Description], productQuantity as [Quantity], productQuantityIn as [Units], productBrand as PreferredBrand, othersBrands as OtherBrands INTO XLSX(?,{headers:true,sheetid: "RequirementDetails", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["RequirementDetails.xlsx", $scope.formRequest.listRequirementItems]);
            }


            $scope.cancelClick = function () {
                $window.history.back();
            }




            $scope.paymentRadio = false;
            $scope.paymentlist = [];

            $scope.addpaymentvalue = function () {
                var listpaymet =
                    {
                        reqTermsID: 0,
                        reqID: $scope.stateParamsReqID,
                        userID: userService.getUserId(),
                        reqTermsDays: 0,
                        reqTermsPercent: 0,
                        reqTermsType: 'PAYMENT',
                        paymentType: '+',
                        isRevised: 0
                    };
                $scope.paymentlist.push(listpaymet);
            };

            $scope.delpaymentvalue = function (val, id) {

                if (id > 0) {
                    $scope.listTerms.push(id);
                }

                $scope.paymentlist.splice(val, 1);
            };

            $scope.resetpayment = function () {
                $scope.paymentlist = [];
            };




            $scope.deliveryRadio = false;
            $scope.deliveryList = [];

            $scope.adddeliveryvalue = function () {
                var deliveryObj =
                    {
                        reqTermsID: 0,
                        reqID: $scope.stateParamsReqID,
                        userID: userService.getUserId(),
                        reqTermsDays: 0,
                        reqTermsPercent: 0,
                        reqTermsType: 'DELIVERY',
                        isRevised: 0
                    };
                $scope.deliveryList.push(deliveryObj);
            };

            $scope.deldeliveryvalue = function (val, id) {

                if (id > 0) {
                    $scope.listTerms.push(id);
                }
                $scope.deliveryList.splice(val, 1);
            };

            $scope.resetdelivery = function () {
                $scope.deliveryList = [];
            };










            $scope.listRequirementTerms = [];

            $scope.SaveRequirementTerms = function (reqID) {

                $scope.listRequirementTerms = [];

                $scope.deliveryList.forEach(function (item, index) {
                    item.reqID = reqID;
                    $scope.listRequirementTerms.push(item);
                });

                $scope.paymentlist.forEach(function (item, index) {

                    item.reqID = reqID;

                    if (item.paymentType == '-') {
                        item.reqTermsDays = -(item.reqTermsDays);
                    }

                    $scope.listRequirementTerms.push(item);
                });

                var params = {
                    "listRequirementTerms": $scope.listRequirementTerms,
                    sessionID: userService.getUserToken()
                };
                if ($scope.formRequest.isForwardBidding) {
                    fwdauctionsService.SaveReqDepartments(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                //growlService.growl(".", "success");
                                //$scope.GetCompanyDepartments();
                                //$scope.addnewdeptView = false;
                                //$scope.department = {
                                //    userID: $scope.userID,
                                //    deptID: 0,
                                //    sessionID: $scope.sessionID
                                //};
                                ////$window.history.back();
                            }
                        });
                } else {
                    auctionsService.SaveRequirementTerms(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                //growlService.growl(".", "success");
                                //$scope.GetCompanyDepartments();
                                //$scope.addnewdeptView = false;
                                //$scope.department = {
                                //    userID: $scope.userID,
                                //    deptID: 0,
                                //    sessionID: $scope.sessionID
                                //};
                                ////$window.history.back();
                            }
                        });
                }


            };




            $scope.GetRequirementTerms = function () {
                auctionsService.GetRequirementTerms(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.listRequirementTerms = response;

                            $scope.listRequirementTerms.forEach(function (item, index) {
                                if (item.reqTermsType == 'DELIVERY') {
                                    $scope.deliveryList.push(item);
                                    $scope.deliveryRadio = true;
                                }
                                else if (item.reqTermsType == 'PAYMENT') {

                                    if (item.reqTermsDays > 0) {
                                        item.paymentType = '+';
                                    }
                                    else if (item.reqTermsDays < 0) {
                                        item.paymentType = '-';
                                        item.reqTermsDays = -(item.reqTermsDays);
                                    }

                                    $scope.paymentlist.push(item);
                                    $scope.paymentRadio = true;
                                }
                            });



                        }
                    });
            }

           // $scope.GetRequirementTerms();




            $scope.listTerms = [];

            $scope.DeleteRequirementTerms = function () {

                var params = {
                    "listTerms": $scope.listTerms,
                    sessionID: userService.getUserToken()
                };

                if ($scope.formRequest.isForwardBidding) {
                    fwdauctionsService.SaveReqDepartments(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {

                            }
                        });
                } else {
                    auctionsService.DeleteRequirementTerms(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {

                            }
                        });
                }
            };




            $scope.paymentTypeFunction = function (type) {
                if (type == 'ADVANCE') {

                }
                if (type == 'POST') {

                }
            }







            $scope.GetCIJList = function () {
                auctionsService.GetCIJList($scope.compID, $scope.sessionid)
                    .then(function (response) {
                        $scope.cijList = response;

                        $scope.cijList.forEach(function (item, index) {
                            var cijObj = $.parseJSON(item.cij);
                            item.cij = cijObj;
                        });


                    })
            }


           // $scope.GetCIJList();



            $scope.GetIndentList = function () {
                auctionsService.GetIndentList(userService.getUserCompanyId(), userService.getUserToken())
                    .then(function (response) {
                        $scope.indentList = response;
                    })
            }

           // $scope.GetIndentList();


            $scope.changeIndent = function (val) {
            };

            $scope.formRequest.category = '';

            $scope.goToStage = function (stage, currentStage) {
                $scope.postRequestLoding = true;
                $scope.tableValidation = $scope.validationFailed = false;
                if (stage == 2 && currentStage == 1) {

                }
                $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                    if (item.productQuantity <= 0 || item.productQuantity == undefined || item.productQuantity == '') {
                        $scope.tableValidation = true;
                        $scope.validationFailed = true;
                    }
                });
                if ($scope.tableValidation) {
                    $scope.postRequestLoding = false;
                    return false;
                }

                if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                    $scope.titleValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }

                if (currentStage == 2 && stage > currentStage) {
                    if ($scope.formRequest.deliveryLocation == null || $scope.formRequest.deliveryLocation == '') {
                        $scope.deliveryLocationValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.paymentTerms == null || $scope.formRequest.paymentTerms == '') {
                        $scope.paymentTermsValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.deliveryTime == null || $scope.formRequest.deliveryTime == '') {
                        $scope.deliveryTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }
                if (currentStage == 3 && stage > currentStage) {
                    if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                        $scope.urgencyValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    
                    if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= '') && $scope.formRequest.isQuotationPriceLimit == true) {
                        $scope.quotationPriceLimitValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.isQuotationPriceLimit == false) {
                        $scope.formRequest.quotationPriceLimit = 0;
                    }

                    if ($scope.formRequest.noOfQuotationReminders > 0)
                    {
                        $scope.formRequest.noOfQuotationReminders = parseInt($scope.formRequest.noOfQuotationReminders);
                    }
                    if ($scope.formRequest.remindersTimeInterval > 0) {
                        $scope.formRequest.remindersTimeInterval = parseInt($scope.formRequest.remindersTimeInterval);
                    }


                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.noOfQuotationRemindersValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.remindersTimeIntervalValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.isTechEval == true && (!$scope.selectedQuestionnaire || $scope.selectedQuestionnaire.evalID <= 0)) {
                        $scope.questionnaireValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        $scope.quotationFreezTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }
                if (!$scope.postRequestLoding) {
                    return false;
                } else {
                    $scope.postRequestLoding = true;
                    var ts = userService.toUTCTicks($scope.formRequest.quotationFreezTime);
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    $scope.formRequest.quotationFreezTime = "/Date(" + milliseconds + "000+0530)/";
                    var ts = userService.toUTCTicks($scope.formRequest.expStartTime);
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    $scope.formRequest.expStartTime = "/Date(" + milliseconds + "000+0530)/";

                    $scope.formRequest.requirementID = $stateParams.Id ? $stateParams.Id : -1;
                    $scope.formRequest.customerID = userService.getUserId();
                    $scope.formRequest.customerFirstname = userService.getFirstname();
                    $scope.formRequest.customerLastname = userService.getLastname();
                    $scope.formRequest.isClosed = "NOTSTARTED";
                    $scope.formRequest.endTime = "";
                    $scope.formRequest.sessionID = userService.getUserToken();
                    $scope.formRequest.subcategories = "";
                    $scope.formRequest.budget = 100000;
                    $scope.formRequest.custCompID = userService.getUserCompanyId();
                    $scope.formRequest.customerCompanyName = userService.getUserCompanyId();

                    for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                        $scope.formRequest.subcategories += $scope.sub.selectedSubcategories[i].subcategory + ",";
                    }

                    var category = [];
                    if ($scope.formRequest.category) {
                        category.push($scope.formRequest.category);
                    }
                    $scope.formRequest.category = category;
                    if (!$scope.formRequest.isForwardBidding) {
                        $scope.formRequest.cloneID = 0;
                        if ($stateParams.reqObj && $stateParams.reqObj.cloneId && $stateParams.reqObj.cloneId !== "") {
                            $scope.formRequest.cloneID = $stateParams.reqObj.cloneId;
                        }
                        auctionsService.postrequirementdata($scope.formRequest)
                            .then(function (response) {
                                if (response.objectID != 0) {
                                    if ($scope.selectedProject) {
                                        $scope.selectedProject.sessionID = userService.getUserToken();
                                        $scope.selectedProject.requirement = {};
                                        $scope.selectedProject.requirement.requirementID = response.objectID;
                                        var param = { item: $scope.selectedProject };
                                        //prmCompanyService.SaveProjectRequirement(param).then(function (projecgtMappingResponse) {
                                        //    console.log("saving project mapping");
                                        //    console.log(projecgtMappingResponse);
                                        //    console.log("END saving project mapping");
                                        //});
                                    }
                                    $scope.SaveReqDepartments(response.objectID);
                                    //$scope.SaveReqDeptDesig(response.objectID);

                                    //$scope.DeleteRequirementTerms();
                                    // $scope.SaveRequirementTerms(response.objectID);

                                    if ($scope.selectedQuestionnaire != null && $scope.selectedQuestionnaire != undefined && $scope.selectedQuestionnaire.evalID != undefined) {
                                        techevalService.getquestionnaire($scope.selectedQuestionnaire.evalID, 1)
                                            .then(function (response1) {
                                                $scope.selectedQuestionnaire = response1;

                                                if ($scope.isTechEval) {
                                                    $scope.selectedQuestionnaire.reqID = response.objectID;
                                                }
                                                else {
                                                    $scope.selectedQuestionnaire.reqID = 0;
                                                }
                                                $scope.selectedQuestionnaire.sessionID = userService.getUserToken();
                                                var params = {
                                                    questionnaire: $scope.selectedQuestionnaire
                                                }
                                                techevalService.assignquestionnaire(params)
                                                    .then(function (response) {
                                                    })
                                            })
                                    }
                                    if (stage) {
                                        $state.go('save-requirement', { Id: response.objectID });
                                    }
                                }
                            });
                    } else {
                        fwdauctionsService.postrequirementdata($scope.formRequest)
                            .then(function (response) {
                                if (response.objectID != 0) {

                                    //$scope.SaveReqDepartments(response.objectID);
                                    $scope.SaveReqDeptDesig(response.objectID);

                                    swal({
                                        title: "Done!",
                                        text: "Requirement Created Successfully",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },
                                        function () {
                                            $scope.postRequestLoding = false;
                                            //$state.go('view-requirement');
                                            $state.go('fwd-view-req', { 'Id': response.objectID });
                                        });
                                }
                            });
                    }
                }
                //$scope.postRequest(0, stage - 1, false, stage);                
            }

            $scope.goBack = function (stage, reqID) {
                $state.go('save-requirement', { Id: reqID });
            }

            $scope.removeAttach = function (index) {
                $scope.formRequest.multipleAttachments.splice(index, 1);
            }

            $scope.productsList = [];
            $scope.nonCoreproductsList = [];
            catalogService.getUserProducts().then(function (response) {
                        $scope.productsList = response;
                        $scope.nonCoreproductsList = $scope.productsList.filter(function (product) {
                            return product.isCoreProductCategory == 0;
                        });
                        
                        // re-set values to back;
                        if ($scope.nonCoreproductsList && $scope.nonCoreproductsList.length > 0) {
                            $scope.nonCoreproductsList.forEach(function (product, index) {
                                if (!$scope.isEdit) {
                                    product.doHide = true;
                                    $scope.AddOtherRequirementItem(product);
                                }
                            });
                        }
                    });
            //$scope.productsList = catalogService.getUserProducts();
            //$scope.nonCoreproductsList = $scope.productsList.filter(function (product) {
            //    return product.isCoreProductCategory == 0;
            //});
            //$scope.getProducts = function () {
            //    catalogService.getUserProducts($scope.compID, userService.getUserId())
            //        .then(function (response) {
            //            $scope.productsList = response;
            //            $scope.nonCoreproductsList = $scope.productsList.filter(function (product) {
            //                return product.isCoreProductCategory == 0;
            //            });
            //            console.log($scope.nonCoreproductsList);
            //        });
            //};

            //$scope.getProducts();

            // in form catalogue ctrl
            $scope.fillTextbox = function (selProd, index) {
                $scope['ItemSelected_' + index] = true;
                $scope.formRequest.listRequirementItems[index].productIDorName = selProd.prodName;
                $scope.formRequest.listRequirementItems[index].catalogueItemID = selProd.prodId;
                $scope.formRequest.listRequirementItems[index].productNo = selProd.prodNo
                $scope.formRequest.listRequirementItems[index].hsnCode = selProd.prodHSNCode; 
                $scope.formRequest.listRequirementItems[index].productDescription = selProd.prodDesc;
                $scope.formRequest.listRequirementItems[index].productCode = selProd.prodCode;
                //$scope.formRequest.listRequirementItems[index].productQuantityInList = []; //;
                //$scope.formRequest.listRequirementItems[index].productQuantityInList.push(selProd.prodQty);
                $scope.formRequest.listRequirementItems[index].productQuantityIn = selProd.prodQty;
                if (selProd.isNonCoreProductCategory) {
                    $scope.formRequest.listRequirementItems[index].productQuantity = 1;
                }
                $scope.formRequest.listRequirementItems[index].isNonCoreProductCategory = selProd.isNonCoreProductCategory;
                $scope['filterProducts_' + index] = null;
                $scope["filterProducts_Code_" + index] = null;
                $scope["filterProducts_Code1_" + index] = null;

                if (!selProd.isNonCoreProductCategory || selProd.isCoreProductCategory) {
                    $scope.GetProductQuotationTemplate(selProd.prodId, index, true);
                } else {
                    $scope.formRequest.listRequirementItems = _.sortBy($scope.formRequest.listRequirementItems, function (o) {
                        if (o.isNonCoreProductCategory) { return o.isNonCoreProductCategory; } else { return 0; }
                    });
                }
            }

            $scope.autofillProduct = function (prodName, index, fieldType) {
                $scope['ItemSelected_' + index] = false;
                var output = [];
                if (prodName && prodName != '' && prodName.length > 0) {
                    prodName = String(prodName).toUpperCase();
                    output = $scope.productsList.filter(function (product) {
                        if(product.isCoreProductCategory === 1) {
                            if (fieldType == 'NAME') {
                                return (String(product.prodName).toUpperCase().includes(prodName) == true);
                            }
                            else if (fieldType == 'CODE') {
                                return (String(product.prodNo).toUpperCase().includes(prodName) == true);
                            } 
                            else if (fieldType == 'CODEMAIN') {
                                return (String(product.prodCode).toUpperCase().includes(prodName) == true);
                            }
                        }
                    });
                }
                if (fieldType == 'NAME') {
                    $scope["filterProducts_" + index] = output;
                }
                else if (fieldType == 'CODE') {
                    $scope["filterProducts_Code_" + index] = output;
                }
                else if (fieldType == 'CODEMAIN') {
                    $scope["filterProducts_Code1_" + index] = output;
                }
            }

            $scope.onBlurProduct = function (index) {
                if ($scope['ItemSelected_' + index] == false) {
                    if (!$scope.formRequest.isRFP)
                    {
                        $scope.formRequest.listRequirementItems[index].productIDorName = "";
                        $scope.formRequest.listRequirementItems[index].productNo = "";
                        $scope.formRequest.listRequirementItems[index].productCode = "";
                    }

                }
                //$scope['filterProducts_' + index] = null;
            }

            $scope.formSplitChange = function () {
                var splitenabled = $scope.formRequest.isSplitEnabled;
                $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                    item.splitenabled = splitenabled;
                    if (splitenabled) {
                        item.productQuantity = 1;
                        $scope.formRequest.isDiscountQuotation = 0;
                    }
                });
            }

            $scope.onSplitChange = function (index) {


                if ($scope.formRequest.listRequirementItems[index].splitenabled == true) {
                    $scope.formRequest.listRequirementItems[index].productQuantity = 1;
                }
            }

            $scope.getCatalogCategories = function () {
                auctionsService.getcatalogCategories(userService.getUserId())
                    .then(function (response) {
                        $scope.companyCatalogueList = response;

                        $scope.catName = $scope.companyCatalogueList[0];
                        $scope.searchCategoryVendors($scope.catName);

                    });
            };

            $scope.searchCategoryVendors = function (str) {
                $scope.getvendors(str);
            };

            $scope.getproductvendors = function () {
                $scope.vendorsLoaded = false;
                $scope.Vendors = [];

                //products.push($scope.formRequest.category);
                //$scope.formRequest.category = category;
                //if ($scope.selectedProducts.length > 0) {
                    var params = { 'products': $scope.selectedProducts, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId(), evalID: $scope.isTechEval ? $scope.selectedQuestionnaire.evalID : 0 };
                    $http({
                        method: 'POST',
                        url: domain + 'getvendorsbyproducts',
                        encodeURI: true,
                        headers: { 'Content-Type': 'application/json' },
                        data: params
                    }).then(function (response) {
                        if (response && response.data) {
                            if (response.data.length > 0) {
                                $scope.Vendors = response.data;
                                // if (allVendors === 1) {
                                // $scope.allCompanyVendors = response.data;
                                // allVendors = 0;
                                // }  
                                $scope.vendorsLoaded = true;
                                for (var j in $scope.formRequest.auctionVendors) {
                                    for (var i in $scope.Vendors) {
                                        if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                            $scope.Vendors.splice(i, 1);
                                        }
                                    }
                                }

                                $scope.VendorsTemp = $scope.Vendors;
                                if ($scope.searchvendorstring != '') {
                                    $scope.searchVendors($scope.searchvendorstring);
                                } else {
                                    $scope.searchVendors('');
                                }


                            }
                            $scope.getCatalogCategories();
                            //$scope.formRequest.auctionVendors =[];
                        } else {
                        }
                    }, function (result) {
                    });
                //}

                //$scope.getCatalogCategories();

            };

            $scope.getCurrencies();



            /*region start WORKFLOW*/

            $scope.getWorkflows = function () {
                workflowService.getWorkflowList()
                    .then(function (response) {
                        $scope.workflowList = [];
                        $scope.workflowListTemp = response;
                        $scope.workflowListTemp.forEach(function (item, index) {
                            if (item.WorkflowModule == $scope.WorkflowModule) {
                                $scope.workflowList.push(item);
                            }
                        });

                        //if (userService.getUserObj().isSuperUser) {
                        //    $scope.workflowList = $scope.workflowList;
                        //}
                        //else {
                        //    $scope.workflowList = $scope.workflowList.filter(function (item) {
                        //        return item.deptID == userService.getSelectedUserDeptID();

                        //    });
                        //}



                    });
            };
            //form Ctrl
           // $scope.getWorkflows();

            $scope.getItemWorkflow = function () {
                workflowService.getItemWorkflow(0, $stateParams.Id, $scope.WorkflowModule)
                    .then(function (response) {
                        $scope.itemWorkflow = response;
                        if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                            $scope.currentStep = 0;

                            var count = 0;

                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {

                                if (track.status == 'APPROVED' || track.status == 'HOLD') {
                                    $scope.isFormdisabled = true;
                                }

                                if (track.status == 'APPROVED') {
                                    $scope.isWorkflowCompleted = true;
                                    $scope.orderInfo = track.order;
                                    $scope.assignToShow = track.status;

                                }
                                else {
                                    $scope.isWorkflowCompleted = false;
                                }



                                if (track.status == 'REJECTED' && count == 0) {
                                    count = count + 1;
                                }

                                if ((track.status == 'PENDING' || track.status == 'HOLD') && count == 0) {
                                    count = count + 1;
                                    $scope.IsUserApproverForStage(track.approverID);
                                    $scope.currentAccess = track.order;
                                }

                                if ((track.status == 'PENDING' || track.status == 'HOLD' || track.status == 'REJECTED') && $scope.currentStep == 0) {
                                    $scope.currentStep = track.order;
                                    return false;
                                }
                            });
                        }
                    });

            };

            $scope.updateTrack = function (step, status) {

                $scope.commentsError = '';

                if ($scope.isReject) {
                    $scope.commentsError = 'Please Save Rejected Items/Qty';
                    return false;
                }

                if (status == 'REJECTED' && (step.comments == null || step.comments == "")) {
                    $scope.commentsError = 'Please enter comments';
                    return false;
                }

                step.status = status;
                step.sessionID = $scope.sessionID;
                step.modifiedBy = userService.getUserId();

                step.moduleName = $scope.WorkflowModule;

                workflowService.SaveWorkflowTrack(step)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            $scope.getItemWorkflow();
                            //location.reload();
                            //     $state.go('list-pr');
                        }
                    })
            };

            $scope.assignWorkflow = function (moduleID) {
                workflowService.assignWorkflow(({ wID: $scope.formRequest.workflowID, moduleID: moduleID, user: userService.getUserId(), sessionID: $scope.sessionID }))
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                            $scope.isSaveDisable = false;
                        }
                        else {
                            //  $state.go('list-pr');
                        }
                    })
            };

            $scope.IsUserApprover = false;

            $scope.functionResponse = false;

            $scope.IsUserApproverForStage = function (approverID) {
                workflowService.IsUserApproverForStage(approverID, userService.getUserId())
                    .then(function (response) {
                        $scope.IsUserApprover = response;
                    });
            };

            $scope.isApproverDisable = function (index) {

                var disable = true;

                var previousStep = {};

                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                    if (index == stepIndex) {
                        if (stepIndex == 0) {
                            if (userService.getLocalDeptDesigt().deptId == step.department.deptID && userService.getLocalDeptDesigt().desigId == step.approver.desigID &&
                                (step.status == 'PENDING' || step.status == 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                        else if (stepIndex > 0) {
                            if (previousStep.status == 'PENDING' || previousStep.status == 'HOLD' || previousStep.status == 'REJECTED') {
                                disable = true;
                            }
                            else if (userService.getLocalDeptDesigt().deptId == step.department.deptID && userService.getLocalDeptDesigt().desigId == step.approver.desigID &&
                                (step.status == 'PENDING' || step.status == 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                    }
                    previousStep = step;
                })

                return disable;
            };

            /*region end WORKFLOW*/

            $scope.PRList1 = [];

            $scope.GetReqPRList = function () {
                var params = {
                    "userid": userService.getUserId(),
                    "deptid": $scope.deptID,
                    "desigid": $scope.desigID,
                    "depttypeid": $scope.deptTypeID,
                    "onlyopen": 1,
                    "sessionid": userService.getUserToken(),
                }
                PRMPRServices.getreqprlist(params)
                    .then(function (response) {
                        $scope.PRList = response;
                        $scope.PRList1 = response;
                        $scope.selectRequirementPRS();
                        $scope.PRList.forEach(function (item, index) {
                            item.isSelected = false;
                        })

                        // console.log("main array count>>>" + $scope.PRList.length);

                        $scope.PRList = $scope.PRList.filter(function (prObj) {
                            prObj.CREATED_DATE = userService.toLocalDate(prObj.CREATED_DATE);
                            prObj.DELIVERY_DATE = userService.toLocalDate(prObj.DELIVERY_DATE);
                            return prObj.PR_STATUS !== 'COMPLETED';
                        });
                        //  console.log("filtered array count>>>" + $scope.PRList.length);
                        if ($stateParams.prDetail && $stateParams.prDetail.PR_ID) {
                            $scope.formRequest.PR_ID = $stateParams.prDetail.PR_ID;
                            $scope.formRequest.PR_NUMBER = $stateParams.prDetail.PR_NUMBER;
                            $stateParams.prDetail.isSelected = true;
                            $scope.fillReqItems($stateParams.prDetail);
                            //$scope.GetPRItemsList({ PR_ID: $stateParams.prDetail.PR_ID });
                        }
                    })
            };

            $scope.GetReqPRList();

            $scope.getPRNumber = function (pr_ID)
            {
                var params = {
                    "prid": pr_ID,
                    "sessionid": userService.getUserToken()
                };
                PRMPRServices.getprdetails(params)
                    .then(function (response) {
                        $scope.PRDetails = response;
                        if ($scope.PRDetails.PR_ID == pr_ID) {
                            //$scope.formRequest.PR_ID = $scope.PRDetails.PR_ID;
                            $scope.formRequest.PR_NUMBER = $scope.PRDetails.PR_NUMBER;
                        }
                    })
            }

            $scope.fillReqItems = function (pr) {
                $scope.formRequest.PR_ID = pr.PR_ID;
                if (pr.isSelected) {
                    $scope.GetPRItemsList(pr);
                } else if (!pr.isSelected) {
                    $scope.GetPRItemsList(pr);
                    

                }

                $scope.PRList = _.orderBy($scope.PRList, ['isSelected'], ['desc']);
            }


            $scope.prDeleteItemsArr = [];

            $scope.GetPRItemsList = function (pr) {
                if (pr && pr.PR_ID > 0) {
                    var params = {
                        "prid": pr.PR_ID,
                        "sessionid": userService.getUserToken()
                    }
                    PRMPRServices.getpritemslist(params)
                        .then(function (response) {
                            $scope.PRItemsList = response;


                            // First check whether this PR exists in requirement items or not if exists then reduce the quantity and return the item//

                            $scope.prDeleteItemsArr = [];

                            if (!pr.isSelected) {
                                if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                    //var success = $scope.PRItemsList.every(function (val) {//pritemlist array
                                    //    if ($scope.formRequest.listRequirementItems.some(productIds => productIds.productId === val.PRODUCT_ID)) {// requirement items array 
                                    //        var index = $scope.formRequest.listRequirementItems.findIndex(c => c.productId === val.PRODUCT_ID);
                                    //        var deductQuantity = $scope.formRequest.listRequirementItems[index].productQuantity - val.REQUIRED_QUANTITY;
                                    //        $scope.formRequest.listRequirementItems[index].productQuantity = deductQuantity;
                                    //    }
                                    //});

                                    //$scope.deleteItem(pr);
                                    //return;
                                    $scope.PRItemsList.forEach(function (prItem, prIndex) {
                                        $scope.formRequest.listRequirementItems.forEach(function (rfqItem, rfqIndex) {
                                            if (prItem.PRODUCT_ID == rfqItem.productId) {
                                                var deductQuantity = rfqItem.productQuantity - prItem.REQUIRED_QUANTITY;
                                                rfqItem.productQuantity = deductQuantity;
                                                if (rfqItem.productQuantity <= 0) {
                                                    $scope.prDeleteItemsArr.push(rfqItem);
                                                }
                                            } else {
                                                if (prItem.PR_ID == rfqItem.PR_ID)
                                                {
                                                    $scope.prDeleteItemsArr.push(rfqItem);
                                                }
                                            }
                                        });
                                    });

                                    if ($scope.prDeleteItemsArr && $scope.prDeleteItemsArr.length >0) {
                                        $scope.prDeleteItemsArr.forEach(function (item, index) {
                                            $scope.deleteItem(item);
                                        });
                                    }
                                    return;
                                }
                            }
                            
                            // First check whether this PR exists in requirement items or not if exists then reduce the quantity and return the item//


                            var selectedPrs = $scope.PRList.filter(function (pr) {
                                return pr.isSelected;
                            });

                            var validPRItems = $scope.PRItemsList.filter(function (prItem) {
                                return (!prItem.REQ_ID || prItem.REQ_ID <= 0);
                            });

                            //To check it is first PR selected in List
                            if (selectedPrs && selectedPrs.length <= 1 && validPRItems && validPRItems.length > 0) {
                                $scope.formRequest.listRequirementItems = [];
                            }

                            var currentPR = $scope.PRList.filter(function (prDetails) {
                                return prDetails.PR_ID === +pr.PR_ID;
                            });

                            if (currentPR && currentPR.length > 0) {
                                currentPR[0].isSelected = true;
                            }                            

                            if (!validPRItems || validPRItems.length <= 0) {
                                swal("Warning!", "All items in PR already linked to existing Requirement.", "error");   
                                if (currentPR && currentPR.length > 0) {
                                    currentPR[0].isSelected = false;
                                }
                            }

                            $scope.PRItemsList.forEach(function (item, index) {
                                if (!item.REQ_ID || item.REQ_ID <= 0) {
                                    $scope.requirementItems =
                                        {
                                            productSNo: index + 1,
                                            ItemID: 0,
                                            productIDorName: item.ITEM_NAME,
                                            productNo: item.ITEM_NUM,
                                            hsnCode: item.HSN_CODE,
                                            productDescription: item.ITEM_DESCRIPTION,
                                            productQuantity: item.REQUIRED_QUANTITY,
                                            productQuantityIn: item.UNITS,
                                            productBrand: item.BRAND,
                                            othersBrands: '',
                                            isDeleted: 0,
                                            productImageID: item.ATTACHMENTS,
                                            attachmentName: '',
                                            //budgetID: 0,
                                            //bcInfo: '',
                                            productCode: item.ITEM_CODE,
                                            splitenabled: 0,
                                            fromrange: 0,
                                            torange: 0,
                                            requiredQuantity: 0,
                                            I_LLP_DETAILS: "",
                                            itemAttachment: "",
                                            itemMinReduction: 0,
                                            productId: item.PRODUCT_ID,
                                            PR_ID:item.PR_ID
                                        };

                                    if ($scope.requirementItems.productImageID == null || $scope.requirementItems.productImageID == '') {
                                        $scope.requirementItems.productImageID = 0;
                                    } else {
                                        $scope.requirementItems.productImageID == item.ATTACHMENTS;
                                    }



                                    if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                        var existingItem = $scope.formRequest.listRequirementItems.filter(function (currentItem) {
                                            return currentItem.productId == $scope.requirementItems.productId;
                                        });

                                        if (existingItem && existingItem.length > 0) {
                                            existingItem[0].productQuantity += $scope.requirementItems.productQuantity;
                                        } else {
                                            $scope.formRequest.listRequirementItems.push($scope.requirementItems);
                                        }
                                    } else {
                                        $scope.formRequest.listRequirementItems.push($scope.requirementItems);
                                    }

                                    //Correcting SNO (because Each and Every Time It is Filling with PR Items List)//

                                    if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                        $scope.formRequest.listRequirementItems.forEach(function (snoItem, snoIndex) {
                                            snoItem.productSNo = snoIndex + 1;
                                        })
                                    }

                                    //Correcting SNO (because Each and Every Time It is Filling with PR Items List)//

                                    $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                                        var selProd = $scope.productsList.filter(function (prod) {
                                            //return prod.prodCode == item.productIDorName;//prodName
                                            return prod.prodCode == item.productCode;
                                        });

                                        if (selProd && selProd != null && selProd.length > 0) {
                                            $scope.autofillProduct(item.productIDorName, index)
                                            $scope.fillTextbox(selProd[0], index);
                                        }
                                    });
                                }
                            });
                        });
                }
            };





            $scope.GetProductQuotationTemplate = function (productId, index, productChange) {

                var params = {
                    "catitemid": productId,
                    "sessionid": userService.getUserToken()
                };

                catalogService.GetProductQuotationTemplate(params)
                    .then(function (response) {
                        if (response) {
                            var currentItem = $scope.formRequest.listRequirementItems[index];
                            currentItem.productQuotationTemplateMaster = response;
                            if (!currentItem.productQuotationTemplate || currentItem.productQuotationTemplate.length <= 0 ||
                                !$scope.isEdit) {
                                currentItem.productQuotationTemplate = response;
                                if ($scope.isEdit) {
                                    currentItem.productQuotationTemplate.forEach(function (templateItem, itemIndexs) {
                                        templateItem.IS_VALID = 0;
                                    });
                                }
                            }
                            else if (currentItem.productQuotationTemplateMaster && currentItem.productQuotationTemplateMaster.length > 0) {
                                if (productChange) {
                                    currentItem.productQuotationTemplate = currentItem.productQuotationTemplateMaster;
                                } else {
                                    currentItem.productQuotationTemplateMaster.forEach(function (templateItem, itemIndexs) {
                                        if (templateItem && templateItem.NAME) {
                                            var filterResult = currentItem.productQuotationTemplateArray.filter(function (template) {
                                                return (template.NAME === templateItem.NAME);
                                            });

                                            if (!filterResult || filterResult.length <= 0) {
                                                templateItem.IS_VALID = 0;
                                                currentItem.productQuotationTemplate.unshift(templateItem);
                                            }
                                        }
                                    });
                                }
                            }

                            $scope.formRequest.listRequirementItems = _.sortBy($scope.formRequest.listRequirementItems, function (o) {
                                if (o.isNonCoreProductCategory) { return o.isNonCoreProductCategory; } else { return 0; }
                            });
                        }
                    });
            };

            //Below product sub item
            $scope.SaveProductQuotationTemplate = function (obj) {
                var sameNameError = false;
                obj.currentProductQuotationTemplate.forEach(function (item, itemIndex) {
                    if (obj.NAME.toUpperCase() == item.NAME.toUpperCase() && obj.T_ID == 0) {
                        sameNameError = true;
                    }
                });

                if (sameNameError) { growlService.growl("Same name Error.", "inverse"); return false; }

                var params = {
                    "productquotationtemplate": obj,
                    "sessionid": userService.getUserToken()
                };

                catalogService.SaveProductQuotationTemplate(params)
                    .then(function (response) {
                        if (response) {
                            growlService.growl("Saved Successfully.", "success");
                            $scope.resetTemplateObj();
                            if (obj.PRODUCT_ID > 0 && obj.index >= 0) {
                                $scope.GetProductQuotationTemplate(obj.PRODUCT_ID, obj.index, true);
                            }
                        }
                    });
            };

            $scope.resetTemplateObj = function (product, index) {
                if (!product) {
                    product = { catalogueItemID: 0, productQuotationTemplate: []};
                }
                if (!index) {
                    index = 0;
                }

                $scope.QuotationTemplateObj = {
                    currentProductQuotationTemplate: product.productQuotationTemplate,
                    index: index,
                    T_ID: 0,
                    PRODUCT_ID: product.catalogueItemID,
                    NAME: '',
                    DESCRIPTION: '',
                    HAS_SPECIFICATION: 0,
                    HAS_PRICE: 0,
                    HAS_QUANTITY: 0,
                    CONSUMPTION: 1,
                    UOM: '',
                    HAS_TAX: 0,
                    IS_VALID: 1,
                    U_ID: userService.getUserId()
                };
            };

            $scope.resetTemplateObj();
            //^Above product sub item

            $scope.loadCustomFields = function () {
                var params = {
                    "compid": userService.getUserCompanyId(),
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.getCustomFieldList(params)
                    .then(function (response) {
                        $scope.customFieldList = response;
                            if ($scope.isEdit) {
                                params = {
                                    "compid": userService.getUserCompanyId(),
                                    "fieldmodule": 'REQUIREMENT',
                                    "moduleid": $stateParams.Id,
                                    "sessionid": userService.getUserToken()
                                };

                                PRMCustomFieldService.GetCustomFieldsByModuleId(params)
                                .then(function (response) {
                                    $scope.selectedcustomFieldList = response;
                                    $scope.selectedcustomFieldList.forEach(function (cfItem, index) {
                                        cfItem.IS_SELECTED == 1;

                                        var cfTempField = _.filter($scope.customFieldList, function (field) {
                                                return field.CUST_FIELD_ID == cfItem.CUST_FIELD_ID;
                                            });
                                        if(cfTempField && cfTempField.length > 0){
                                            cfTempField[0].IS_SELECTED = 1;
                                            cfTempField[0].FIELD_VALUE = cfItem.FIELD_VALUE;
                                            cfTempField[0].MODULE_ID = cfItem.MODULE_ID;
                                        }
                                    });
                                });
                             }
                        });
                };

            $scope.loadCustomFields();

            $scope.addCustomFieldToFrom = function (field) {
                $scope.selectedcustomFieldList = _.filter($scope.customFieldList, function (field) {
                    return field.IS_SELECTED == 1;
                });
            };

            $scope.validateSubItemQuantity = function (product, productQuotationTemplate){
                if (!productQuotationTemplate.CONSUMPTION) {
                    productQuotationTemplate.CONSUMPTION = 1;
                } else {
                    product.productQuantity = 1;
                }
            };

            
            $scope.saveRequirementCustomFields = function(requirementId) {
                if (requirementId) {
                     $scope.selectedcustomFieldList.forEach(function (item, index) {
                        item.MODULE_ID = requirementId;
                        item.ModifiedBy = $scope.userID;
                    });

                    var params = {
                        "details": $scope.selectedcustomFieldList,
                        "sessionid": userService.getUserToken()
                    };

                    PRMCustomFieldService.saveCustomFieldValue(params)
                        .then(function (response) {
                        
                        });
                }
            };

            $scope.GetPRMFieldMappingDetails = function () {
                $scope.prmFieldMappingDetails = {};
                var template = $scope.selectedTemplate ? $scope.selectedTemplate : 'DEFAULT';
                userService.GetPRMFieldMappingDetails(template).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        $scope.prmFieldMappingDetails[item.FIELD_NAME] = item.FIELD_ALIAS_NAME;
                    });
                });
            };

            $scope.GetPRMFieldMappingDetails();

            $scope.GetPRMFieldMappingTemplates = function () {
                PRMCustomFieldService.GetPRMFieldMappingTemplates().then(function (response) {
                    $scope.prmFieldMappingTemplates = response;
                });
            };

            $scope.GetPRMFieldMappingTemplates();


            $scope.searchingCategoryVendors = function (value) {
                if (value) {
                    $scope.VendorsList = _.filter($scope.VendorsTempList1, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1; });
                }
            };

            $scope.selectFormCategorySearch = function (vendor) {
                var error = false;

                $scope.formRequest.auctionVendors.forEach(function (SV, SVIndex) {
                    if (SV.vendorID == vendor.vendorID) {
                        error = true;
                        growlService.growl("Vendor already selected", "inverse");

                    }
                })

                if (error == true) {
                    return;
                }

                $scope.Vendors.forEach(function (UV, UVIndex) {
                    if (UV.vendorID == vendor.vendorID) {
                        error = true;
                    }
                })

                if (error == false) {
                    vendor.isFromGlobalSearch = 1;
                    $scope.formRequest.auctionVendors.push(vendor);
                    $scope.VendorsList = $scope.VendorsList.filter(function (obj) {
                        return obj != vendor;
                    });
                    $scope.ShowDuplicateVendorsNames.push(vendor);
                }
                else {
                    $scope.selectForA(vendor);
                    vendor.isChecked = false;
                }
            };

            // Category Based Vendors //
            $scope.VendorsList = [];
            $scope.VendorsTempList1 = [];

            $scope.getCategoryVendors = function (str) {
                $scope.ShowDuplicateVendorsNames = [];
                $scope.VendorsList = [];
                $scope.VendorsTempList1 = [];
                $scope.vendorsLoaded = false;
                var category = '';

                category = catObj.catCode;

                var params = { 'Categories': category, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId(), evalID: $scope.isTechEval ? $scope.selectedQuestionnaire.evalID : 0 };
                $http({
                    method: 'POST',
                    url: domain + 'getVendorsbyCategories',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            $scope.VendorsList = response.data;
                            $scope.VendorsTempList = $scope.VendorsList;



                            $scope.Vendors.forEach(function (item, index) {
                                $scope.VendorsTempList.forEach(function (item1, index1) {
                                    if (item.vendorID == item1.vendorID) {
                                        $scope.ShowDuplicateVendorsNames.push(item1);
                                        $scope.VendorsList.splice(index1, 1);
                                    }
                                })
                            });

                            if ($scope.formRequest.auctionVendors.length > 0) {
                                $scope.formRequest.auctionVendors.forEach(function (item1, index1) {
                                    $scope.VendorsTempList.forEach(function (item2, index2) {
                                        if (item1.vendorID == item2.vendorID) {
                                            $scope.ShowDuplicateVendorsNames.push(item2);
                                            $scope.VendorsList.splice(index2, 1);
                                        }
                                    });
                                });
                            }


                            $scope.VendorsTempList1 = $scope.VendorsList;

                            if ($scope.ShowDuplicateVendorsNames.length > 0) {
                                $scope.totalItems = $scope.ShowDuplicateVendorsNames.length;
                            } else {
                                $scope.totalItems = $scope.ShowDuplicateVendorsNames.length;
                            }

                        } else {
                            $scope.VendorsList = [];
                        }

                        if ($scope.searchCategoryVendorstring != '') {
                            $scope.searchingCategoryVendors($scope.searchCategoryVendorstring);
                        } else {
                            $scope.searchingCategoryVendors('');
                        }


                    } else {

                    }
                }, function (result) {
                });

            }
            // Category Based Vendors //

            $scope.validateLineItemsList = function () {
                if ($scope.nonCoreproductsList && $scope.nonCoreproductsList.length > 0 && $scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                    $scope.nonCoreproductsList.forEach(function (nonCoreProduct, index) {
                        var filterItems = $scope.formRequest.listRequirementItems.filter(function (product) {
                            return product.catalogueItemID === nonCoreProduct.prodId;
                        });

                        if (filterItems && filterItems.length > 0) {
                            nonCoreProduct.doHide = true;
                        }
                    });
                }
            };

            $scope.selectRequirementPRS = function () {
                if ($stateParams.Id) {
                    if ($scope.PRList && $scope.PRList.length > 0 && $scope.formRequest.PR_ID && $scope.formRequest.PR_ID !== '') {
                        var requirementPRs = $scope.formRequest.PR_ID.split(',');
                        requirementPRs.forEach(function (requirementPRs, index) {
                            var filterPR = $scope.PRList.filter(function (pr) {
                                return pr.PR_ID === +requirementPRs;
                            });

                            if (filterPR && filterPR.length > 0) {
                                filterPR[0].isSelected = true;
                            }
                        });

                        $scope.PRList = _.orderBy($scope.PRList, ['isSelected'], ['desc']);
                    }
                }
            };


            $('.selected-items-box').bind('click', function(e) {
                $('.multiple-selection-dropdown .list').slideToggle('fast');
            });




            $scope.totalPRItems = 0;
            $scope.currentPRPage = 1;
            $scope.currentPRPage2 = 1;
            $scope.itemsPerPRPage = 10;
            $scope.itemsPerPRPage2 = 10;
            $scope.maxPRSize = 10;

            $scope.setPRPage = function (pageNo) {
                $scope.currentPRPage = pageNo;
            };

            $scope.pagePRChanged = function () {
            };


            $scope.searchTable = function (str) {
                var filterText = angular.lowercase(str);
                if (!str || str == '' || str == undefined || str == null) {
                    $scope.PRList = $scope.PRList1;
                }
                else {
                    $scope.PRList = $scope.PRList1.filter(function (products) {
                        return (String(angular.lowercase(products.PR_NUMBER)).includes(filterText) == true);
                    });
                }


                $scope.totalPRItems = $scope.PRList.length;
            }



            $scope.createProduct = function (product,index) {
                $scope.itemProductNameErrorMessage = '';
                $scope.itemProductUnitsErrorMessage = '';

                if (!product.productIDorName || !product.productQuantityIn) {

                    if (!product.productIDorName) {
                        $scope.itemProductNameErrorMessage = 'Please Enter Product Name.';
                    } else if (!product.productQuantityIn) {
                        $scope.itemProductUnitsErrorMessage = 'Please Enter Units.';
                    }
                    return 
                }


                $scope.productObj = {
                    prodId: 0,
                    prodCode: product.productCode,
                    compId : userService.getUserCompanyId(),
                    prodName: product.productIDorName,
                    prodNo:   product.productNo,
                    prodQty:  product.productQuantityIn,
                    isValid: 1,
                    ModifiedBy: userService.getUserId()
                };

                var params = {
                    reqProduct: $scope.productObj,
                    sessionID: userService.getUserToken()
                }

                catalogService.addproduct(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            //growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            //growlService.growl("product Added Successfully.", "success");
                            $scope.itemProductNameErrorMessage = '';
                            $scope.itemProductUnitsErrorMessage = '';
                            var productID = response.repsonseId;
                            $scope.formRequest.listRequirementItems[index].catalogueItemID = productID;
                            $scope.formRequest.listRequirementItems[index].isNewItem = true;
                            $scope.GetProductQuotationTemplate(productID, index, true);
                        }
                    });
                

            }

        }]);