﻿using PRM.Data;
using PRMServices.Infrastructure;
using PRMServices.Infrastructure.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRMServices.App_Code
{
    public class Startup
    {
        public static void AppInitialize()
        {
            DbExtension.Invoke();

            AutofacConfiguration.Initialize();

            AutoMapperConfiguration.Init();
        }
    }
}